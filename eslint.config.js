// eslint.config.mjs
import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';
import deprecation from 'eslint-plugin-deprecation';

/** @type {import('@typescript-eslint/utils').TSESLint.FlatConfig.ConfigFile} */
export default tseslint.config({
    extends: [
        eslint.configs.recommended,
        ...tseslint.configs.recommendedTypeChecked,
        ...tseslint.configs.stylisticTypeChecked,
        // deprecation.configs.recommended
    ],
    languageOptions: {
        parser: tseslint.parser,
        parserOptions: {
            project: true,
            tsconfigRootDir: import.meta.dirname,
        },
    },
    files: ['lib/**/*.ts', 'test/spec/**/*.ts'],
    rules: {
        "@typescript-eslint/require-await": "error",
        "deprecation/deprecation": "error",
        "@typescript-eslint/no-unsafe-argument": "warn"
    },
    plugins: {
        deprecation
    }
})

// export default tseslint.config({
//     extends: [
//         eslint.configs.recommended,
//         ...tseslint.configs.recommendedTypeChecked,
//         ...tseslint.configs.stylisticTypeChecked,
//     ],
//     languageOptions: {
//         parserOptions: {
//           project: true,
//           tsconfigRootDir: import.meta.dirname,
//         },
//     },
//     files: ['./lib/**/*.ts'],
//     rules: {
//         "no-unused-vars": "error",
//         "no-undef": "error",
//         "require-await": "error",
//         "require-yield": "error",
//         "@typescript-eslint/await-thenable": "error",
//         "@typescript-eslint/no-floating-promises": "error"
//     }
// });
