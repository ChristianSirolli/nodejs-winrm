import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as WinRM from '../../lib/winrm/internal.ts';
import { MochaLatte } from './spec_helper.ts';

describe('Exceptions', function() {
    const $ = new MochaLatte<void, {
        error: WinRM.WinRMHTTPTransportError & WinRM.WinRMWSManFault & WinRM.WinRMWMIError
    }>(this);
    $.context('WinRM.WinRMHTTPTransportError', $$ => {
        $$.let('error', () => new WinRM.WinRMHTTPTransportError('Foo happened', 500));
        it('adds the response code to the message', () => {
            expect($$.get('error').message).to.eq('Foo happened (500).');
        });
        it('exposes the response code as an attribute', () => {
            expect($$.get('error').status_code).to.eq(500);
        });
        it('is a winrm error', () => {
            expect($$.get('error')).to.be.instanceOf(WinRM.WinRMError);
            // expect($$.get('error')).to.be.a('WinRMError');
        });
    });
    $.context('WinRM.WinRMWSManFault', $$ => {
        $$.let('error', () => new WinRM.WinRMWSManFault('fault text', '42'));
        it('exposes the fault text as an attribute', () => {
            expect($$.get('error').fault_description).to.eq('fault text');
        });
        it('exposes the fault code as an attribute', () => {
            expect($$.get('error').fault_code).to.eq('42');
        });
        it('is a winrm error', () => {
            expect($$.get('error')).to.be.instanceOf(WinRM.WinRMError);
        });
    });
    $.context('WinRM.WinRMWMIError', $$ => {
        $$.let('error', () =>  new WinRM.WinRMWMIError('message text', 77777));
        it('exposes the error text as an attribute', () => {
            expect($$.get('error').error).to.eq('message text');
        });
        it('exposes the error code as an attribute', () => {
            expect($$.get('error').error_code).to.eq(77777);
        });
        it('is a winrm error', () => {
            expect($$.get('error')).to.be.instanceOf(WinRM.WinRMError);
        });
    });
});