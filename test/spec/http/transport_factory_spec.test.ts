import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as WinRM from '../../../lib/winrm/internal.ts';
import { ConnectionOpts } from '../../../lib/winrm/connection_opts.ts';
import { TransportString } from '../../../lib/winrm/types.ts';
import { MochaLatte } from '../spec_helper.ts';
// import sinon from 'sinon';

// class HttpGSSAPI extends WinRM.HTTP.HttpTransport {
//     constructor(endpoint: string | URL, _realm: string, opts: ConnectionOpts, _service = 'HTTP') {
//         super(endpoint, opts);
//     }
// };

// WinRM.HTTP.HttpGSSAPI = Object.assign(WinRM.HTTP.HttpGSSAPI, HttpGSSAPI);

describe('WinRM.HTTP.TransportFactory', function () {
    const $ = new MochaLatte<WinRM.HTTP.TransportFactory, {
        transport: TransportString,
        options: ConnectionOpts
    }>(this);
    $.subject(() => new WinRM.HTTP.TransportFactory());

    describe('.create_transport', () => {
        $.let('transport', () => 'negotiate');
        $.let('options', () => new ConnectionOpts({
            transport: $.get('transport'),
            endpoint: 'http://endpoint',
            user: 'user'
        }));
        it('creates a negotiate transport', () => {
            $.get('options').set('transport', 'negotiate');
            expect($.subject().create_transport($.get('options'))).to.be.instanceOf(WinRM.HTTP.HttpNegotiate);
        });
        it('creates a plaintext transport', () => {
            $.get('options').set('transport', 'plaintext');
            expect($.subject().create_transport($.get('options'))).to.be.instanceOf(WinRM.HTTP.HttpPlainText);
        });
        it('creates a basic auth ssl transport', () => {
            $.get('options').set('transport', 'ssl');
            $.get('options').set('basic_auth_only', true);
            expect($.subject().create_transport($.get('options'))).to.be.instanceOf(WinRM.HTTP.BasicAuthSSL);
        });
        it('creates a client cert ssl transport', () => {
            $.get('options').set('basic_auth_only', false);
            $.get('options').set('transport', 'ssl');
            $.get('options').set('client_cert', 'cert');
            expect($.subject().create_transport($.get('options'))).to.be.instanceOf(WinRM.HTTP.ClientCertAuthSSL);
        });
        it('creates a negotiate over ssl transport', () => {
            $.get('options').delete('basic_auth_only');
            $.get('options').delete('client_cert');
            $.get('options').set('transport', 'ssl');
            expect($.subject().create_transport($.get('options'))).to.be.instanceOf(WinRM.HTTP.HttpNegotiate);
        });
        it('creates a kerberos transport', () => {
            $.get('options').set('transport', 'kerberos');
            expect($.subject().create_transport($.get('options'))).to.be.instanceOf(WinRM.HTTP.HttpGSSAPI);
        });
        // it('creates a transport from a stringified transport', () => {
        //     $.get('options').set('transport', 'negotiate');
        //     expect(subject.create_transport($.get('options'))).toBeInstanceOf(WinRM.HTTP.HttpNegotiate);
        // });
        it('raises when transport type does not exist', () => {
            $.get('options').set('transport', 'fancy' as TransportString); // attempt to bypass TypeScript's type checking
            expect(() => $.subject().create_transport($.get('options'))).to.throw(WinRM.InvalidTransportError);
        });
    });
});