import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as WinRM from '../../../lib/winrm/internal.ts';
import { ConnectionOpts } from '../../../lib/winrm/connection_opts.ts';
// import { NTLMClient } from '../../../lib/winrm/helpers.ts';
// import axios from 'feaxios';
import { MochaLatte } from '../spec_helper.ts';

describe('WinRM.HTTP.HttpNegotiate', function () {
    const $ = new MochaLatte<unknown, {
        endpoint: string,
        user: string,
        domain: string,
        password: string,
        options: ConnectionOpts
    }>(this);
    describe('.init', () => {
        $.let('endpoint', () => 'http://some_endpoint/wsman');
        $.let('domain', () => 'some_domain');
        $.let('user', () => 'some_user');
        $.let('password', () => 'some_password');
        $.let('options', () => ConnectionOpts.create_with_defaults({
            endpoint: $.get('endpoint'),
            user: $.get('user'),
            password: $.get('password')
        }));
        $.context('user is not domain prefixed', () => {
            it('does not pass a domain to the NTLM client', () => {
                expect(() => new WinRM.HTTP.HttpNegotiate($.get('endpoint'), $.get('user'), $.get('password'), $.get('options'))).to.not.throw();
            });
        });
        $.context('user is domain prefixed', () => {
            it('passes prefixed domain to the NTLM client', () => {
                const { ntlmcli } = new WinRM.HTTP.HttpNegotiate($.get('endpoint'), `${$.get('domain')}\\${$.get('user')}`, $.get('password'), $.get('options'));
                const { username: passed_username, password: passed_password, opts: passed_opts } = ntlmcli;
                expect(passed_username).to.eq($.get('user'));
                expect(passed_password).to.eq($.get('password'));
                expect(passed_opts.get('domain')).to.eq($.get('domain'));
                
            });
        });
        $.context('option is passed with a domain', $$ => {
            $$.get('options').set('domain', $.get('domain'));
            it('passes domain option to the NTLM client', () => {
                // console.log([...$$.get('options')])
                const { ntlmcli } = new WinRM.HTTP.HttpNegotiate($.get('endpoint'), $.get('user'), $.get('password'), $$.get('options'));
                const { username: passed_username, password: passed_password, domain: passed_domain } = ntlmcli;
                expect(passed_username).to.eq($.get('user'));
                expect(passed_password).to.eq($.get('password'));
                expect(passed_domain).to.eq($.get('domain'));
            });
        });
    });
});