import { randomUUID } from "crypto";
import { describe } from "mocha";
import { MochaLatte, default_connection_opts } from "../spec_helper.ts";
import { InitRunspacePool } from "../../../lib/winrm/wsmv/init_runspace_pool.ts";
import { expect } from "chai";
import { pack } from "../../../lib/winrm/helpers.ts";

describe('WinRM.WSMV.InitRunspacePool', function() {
    const $ = new MochaLatte<InitRunspacePool, {
        shell_id: Uppercase<`${string}-${string}-${string}-${string}-${string}`>
        payload: number[]
    }>(this);
    $.context('default session options', $$ => {
        $$.let('shell_id', () => randomUUID().toUpperCase());
        $$.let('payload', () => Buffer.from('blah').toJSON().data);
        
        $$.subject(() => new $.described_class(default_connection_opts(false), $$.get('shell_id'), $$.get('payload')));
        
        it('creates a well formed message', () => {
            const xml = $$.subject().build();
            expect(xml).to.include('<w:OperationTimeout>PT60S</w:OperationTimeout>');
            expect(xml).to.include('<w:Locale mustUnderstand="false" xml:lang="en-US"/>');
            expect(xml).to.include('<p:DataLocale mustUnderstand="false" xml:lang="en-US"/>');
            expect(xml).to.include(
                '<p:SessionId mustUnderstand="false">' +
                'uuid:05A2622B-B842-4EB8-8A78-0225C8A993DF</p:SessionId>'
            );
            expect(xml).to.include('<w:MaxEnvelopeSize mustUnderstand="true">153600</w:MaxEnvelopeSize>');
            expect(xml).to.include('<a:To>http://localhost:5985/wsman</a:To>');
            expect(xml).to.include(
                '<w:OptionSet env:mustUnderstand="true">' +
                '<w:Option Name="protocolversion" MustComply="true">2.3</w:Option></w:OptionSet>'
            );
            expect(xml).to.include('<rsp:InputStreams>stdin pr</rsp:InputStreams>');
            expect(xml).to.include('<rsp:OutputStreams>stdout</rsp:OutputStreams>');
            expect(xml).to.include(`<rsp:Shell ShellId="${$$.subject().shell_id}" Name="Runspace">`);
            expect(xml).to.include(
                '<w:ResourceURI mustUnderstand="true">' +
                'http://schemas.microsoft.com/powershell/Microsoft.PowerShell'
            );
            expect(xml).to.include(
                '<creationXml xmlns="http://schemas.microsoft.com/powershell">' +
                `${Buffer.from(pack($$.get('payload'), 'C*')).toString('base64')}</creationXml>`
            );
        });
    });
});