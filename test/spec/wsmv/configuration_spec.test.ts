import type { Configuration } from "../../../lib/winrm/wsmv/configuration.ts";
import { MochaLatte, default_connection_opts } from "../spec_helper.ts";
import { expect } from "chai";
describe('WinRM.WSMV.Configuration', function () {
    const $ = new MochaLatte<Configuration, {
        xml: string
    }>(this);
    $.subject(() => new $.described_class(default_connection_opts(false)));
    $.let('xml', () => $.subject().build());
    it('creates a well formed message', () => {
        expect($.get('xml')).to.include('<w:OperationTimeout>PT60S</w:OperationTimeout>');
        expect($.get('xml')).to.include('<a:Action mustUnderstand="true">' +
            'http://schemas.xmlsoap.org/ws/2004/09/transfer/Get</a:Action>');
        expect($.get('xml')).to.include('w:ResourceURI mustUnderstand="true">' +
            'http://schemas.microsoft.com/wbem/wsman/1/config</w:ResourceURI>');
    });
});