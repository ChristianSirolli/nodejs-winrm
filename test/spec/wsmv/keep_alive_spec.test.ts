import { describe } from "mocha";
import { MochaLatte, default_connection_opts } from "../spec_helper.ts";
import { KeepAlive } from "../../../lib/winrm/wsmv/keep_alive.ts";
import { expect } from "chai";

describe('WinRM.WSMV.KeepAlive', function () {
    const $ = new MochaLatte<KeepAlive, {
        shell_id: Uppercase<`${string}-${string}-${string}-${string}-${string}`>
        xml: string
    }>(this);
    $.context('default session options', $$ => {
        $$.let('shell_id', () => 'F4A2622B-B842-4EB8-8A78-0225C8A993DF');
        $$.subject(() => new $.described_class(default_connection_opts(false), $$.get('shell_id')));
        $$.let('xml', $$$ => $$$.subject().build());

        it('creates a well formed message', () => {
            expect($$.get('xml')).to.include('<w:OperationTimeout>PT60S</w:OperationTimeout>');
            expect($$.get('xml')).to.include(
                '<w:OptionSet><w:Option Name="WSMAN_CMDSHELL_OPTION_KEEPALIVE">' + 
                'TRUE</w:Option></w:OptionSet>'
            );
            expect($$.get('xml')).to.include(
                '<w:SelectorSet><w:Selector Name="ShellId">' +
                `${$$.get('shell_id')}</w:Selector></w:SelectorSet>`
            );
            expect($$.get('xml')).to.include('<rsp:DesiredStream>stdout</rsp:DesiredStream>');
        });
    });
});