/* eslint-disable @typescript-eslint/no-this-alias */
import { describe, type Suite } from "mocha";
import type { Connection } from "../../lib/winrm/connection.ts";
import * as WinRM from '../../lib/winrm/internal.ts';
import type { Constructor, InitialConnectionOptions, WinRMOptions } from "lib/winrm/types.ts";
import { ConnectionOpts } from "lib/winrm/connection_opts";
import { Type } from "typescript";

export async function stubbed_response(file: string): BindingFn {
    return (await import(`./stubs/responses/${file}.ts`) as { default: (binding?: Record<string, string>) => string | Promise<string> }).default;
}

export async function stubbed_clixml(file: string): BindingFn {
    return (await import(`./stubs/clixml/${file}.ts`) as { default: (binding?: Record<string, string>) => string }).default;
}

export type BindingFn = Promise<(binding?: Record<string, string>) => string | Promise<string>>;

export function default_connection_opts(basic: false): ConnectionOpts
export function default_connection_opts(basic?: true): InitialConnectionOptions & Partial<WinRMOptions>
export function default_connection_opts(basic = true): InitialConnectionOptions & Partial<WinRMOptions> | ConnectionOpts {
    if (basic) {
        return {
            user: 'Administrator',
            password: 'password',
            endpoint: 'http://localhost:5985/wsman',
            max_envelope_size: 153600,
            session_id: '05A2622B-B842-4EB8-8A78-0225C8A993DF',
            operation_timeout: 60,
            locale: 'en-US'
        };
    } else {
        return ConnectionOpts.create_with_defaults(default_connection_opts());
    }
}

// Strip leading whitespace from each line that is the same as the
// amount of whitespace on the first line of the string.
// Leaves _additional_ indentation on later lines intact.
// and remove newlines.
export class _String extends String {
    unindent() {
        return this.replace(new RegExp(`^${this.match(/^\s*/)?.at(0)}`, 'gm'), '').replace('\n', '');
    }
    toByteString() {
        return Buffer.from(this).toString('ascii');
    }
}

export function turnOffLoggerErrorHandling(connection: Connection) {
    connection.logger.transports.forEach(transport => {
        transport.handleExceptions = false;
        transport.handleRejections = false;
    });
}

const bindingChecks: Record<symbol, Record<string, boolean>> = {};
const bindings: Record<symbol, Record<string, (ctx: MochaLatte) => unknown>> = {};


type ImportType = {
    [key: string]: ImportType
    // eslint-disable-next-line @typescript-eslint/ban-types
} | Constructor | Type | Function | string;
export class MochaLatte<
    Class = void,
    Variables extends Record<string, unknown> = Record<string, unknown>,
    Subject = Class
> {
    #binding: Partial<Variables & Record<symbol, (ctx: MochaLatte) => unknown>> = {};
    #subject: unknown;
    #subjectFunc: ($$: MochaLatte) => Subject = ($$, ...args) => new $$.described_class(...args) as Subject;
    #shared_examples: Record<string, ($$: MochaLatte, ...args: never[]) => void> = {}
    #sym = Symbol(crypto.randomUUID());
    suite: Suite;
    constructor(suite: Suite) {
        this.suite = suite;
        bindings[this.#sym] = {};
        bindingChecks[this.#sym] = {};
    }
    /**
     * Returns unique symbol for this instance of MochaLatte.
     * Can be used to compare contexts
     */
    get _sym() {
        return this.#sym;
    }
    /**
    * The constructor of the class named in Mocha.Suite.title
    * The type can be set on the MochaLatte instance
    * ```ts
    * import { describe } from 'mocha';
    * import { MochaLatte } from 'mocha-latte';
    * import { MyClass } from '../lib/index.ts';
    * 
    * describe('MyClass', function () {
    *   const $ = new MochaLatte<MyClass>(this);
    *   console.log(new $.described_class() instanceof MyClass) // true
    * });
    * ```
    */
    get described_class() {
        const $ = this;
        // return (function described_class() {
        const classStrArr = $.suite.title.split('.');
        const modules: ImportType[] = [WinRM];
        classStrArr.splice(0, 1);
        for (const str of classStrArr) {
            const module: ImportType = (modules.at(-1)! as Record<string, ImportType>)[str];
            modules.push(module);
        }
        return modules.at(-1) as new (...args: ConstructorParameters<Constructor<Class>>) => Class;
        // })();
    }
    get binding() {
        return Object.fromEntries(
            (Reflect.ownKeys(this.#binding)
                .filter(k => typeof k == 'string' && bindingChecks[this.#sym][k]) as string[])
                .map(k => [k, (this.#binding as Variables)[k] as string])
        );
    }

    getBinding<K extends readonly (keyof Variables)[]>(keys: K) {
        const binding = {} as Record<K[number], string & Promise<string>>;
        keys.forEach(key => {
            Reflect.set(binding, key, this.#binding[key as K[number]]);
        });
        return binding;
    }

    get<K extends keyof Variables>(prop: K, receiver: MochaLatte<Class, Variables, Subject> = this) {
        return Reflect.get(this.#binding, prop, receiver) as Variables[K];
    }
    let<K extends keyof Variables>(key: K, val: (ctx: MochaLatte<Class, Variables, Subject>) => unknown, addToBinding = true): boolean {
        return this.set(key, val, addToBinding);
    }
    set<K extends keyof Variables>(key: K, val: (ctx: MochaLatte<Class, Variables, Subject>) => unknown, addToBinding = true): boolean {
        const sym = Symbol(key as string);
        const $ = this;
        if (bindings[this.#sym][key as string]) {
            bindingChecks[this.#sym][`_${key as string}`] = true;
        }
        bindings[this.#sym][key as string] = val as (ctx: MochaLatte<void, Record<string, unknown>, void>) => unknown;
        bindingChecks[this.#sym][key as string] = addToBinding;
        const result = Reflect.defineProperty($.#binding, key, {
            get() {
                return (($.#binding as Record<string | symbol, ReturnType<typeof val>>)[sym] ??= Reflect.apply(val, $, [$])) as Variables[typeof key];
            },
            set(val2: (ctx: MochaLatte) => unknown) {
                ($.#binding as Record<string | symbol, ReturnType<typeof val>>)[sym] = Reflect.apply(val2, $, [$]);
            },
            enumerable: true,
            configurable: true
        });
        if (!result && !bindingChecks[this.#sym][`_${key as string}`]) {
            Reflect.deleteProperty($.#binding, key);
            return this.set(key, val);
        } else {
            return result;
        }
    }

    subject(): Subject
    subject(func: MochaLatte<Class, Variables, Subject>): Subject
    subject(func: ($$: MochaLatte<Class, Variables, Subject>) => Subject): undefined
    // subject<T2>(): T2
    // subject<T2>(func: MochaLatte<Class, Variables, Subject | T2>): T2
    // subject<T2>(func: ($$: MochaLatte<Class, Variables, Subject | T2>) => T2): undefined
    subject(func?: 
        MochaLatte<Class, Variables, Subject> |
        (($$: MochaLatte<Class, Variables, Subject>) => Subject)
    ): Subject | undefined {
        if (typeof func == 'function' && !(func instanceof MochaLatte)) {
            this.#subjectFunc = func as ($$: MochaLatte) => Subject;
        } else {
            // eslint-disable-next-line prefer-rest-params
            return (this.#subject ??= Reflect.apply(this.#subjectFunc, func as MochaLatte<Class, Variables, Subject> || this, [func as MochaLatte<Class, Variables, Subject> || this, ...arguments])) as Subject;
        }
    }
    context(title: string, fun: ($$: MochaLatte<Class, Variables, Subject>) => void | Promise<void>): Suite;
    context<T>(title: string, fun: ($$: MochaLatte<T, Variables, T>) => void | Promise<void>): Suite;
    context<T, S>(title: string, fun: ($$: MochaLatte<T, Variables, S>) => void | Promise<void>): Suite;
    context<T = Class, S = T>(title: string, fun: ($$: MochaLatte<T, Variables, S | T>) => void | Promise<void>) {
        const $$ = new MochaLatte<T, Variables, S>(this.suite);
        $$.subject(this.#subjectFunc as unknown as ($$: MochaLatte<T, Variables, S>) => S);
        Object.entries(bindings[this.#sym]).forEach(([key, val]) => {
            $$.set(key as keyof Variables, val as (ctx: MochaLatte<T, Variables, S>) => unknown);
        });
        return describe(title, function _context() {
            void fun($$);
        });
    }
    shared_examples(title: string, fun: ($$: this, ...args: never[]) => void) {
        this.#shared_examples[title] = fun as ($$: MochaLatte, ...args: never[]) => void;
    }
    it_behaves_like($$: MochaLatte, shared_example: string, ...args: unknown[]) {
        const $ = this;
        return describe(`it behaves like ${shared_example}`, function _it_behaves_like() {
            Reflect.apply($.#shared_examples[shared_example], $$ || $, args);
        });
    }
}
