import { describe, it } from 'mocha';
import { expect } from 'chai';
import { ConnectionOpts } from '../../lib/winrm/connection_opts.ts';
import type { InitialCertConnectionOptions, InitialConnectionOptions } from '../../lib/winrm/types.ts';
import { MochaLatte } from './spec_helper.ts';

describe('WinRM.ConnectionOpts', function () {
    const $ = new MochaLatte<void, {
        overrides: InitialConnectionOptions & InitialCertConnectionOptions & Record<string, unknown>
    }>(this);
    // eslint-disable-next-line @typescript-eslint/ban-types
    $.shared_examples('invalid options', ($$, error_type: Error | Function | string) => {
        it('it throws a validation error', () => {
            const check = () => ConnectionOpts.create_with_defaults($$.get('overrides') as InitialConnectionOptions);
            if (error_type instanceof String) {
                expect(check).to.throw(error_type as string);
            } else {
                // eslint-disable-next-line @typescript-eslint/ban-types
                expect(check).to.throw(error_type as Error | Function);
            }
        });
    });
    $.context('when there are no overrides', $$ => {
        $.it_behaves_like($$, 'invalid options', TypeError);
    });
    $.context('when there are only username and password', $$ => {
        $$.let('overrides', () => {
            return {
                user: 'Administrator',
                password: 'password'
            }
        });
        $.it_behaves_like($$, 'invalid options', 'NameError');
    });
    $.context('when there are only username and endpoint', $$ => {
        $$.let('overrides', () => {
            return {
                user: 'Administrator',
                endpoint: 'http://localhost:5985/wsman'
            }
        });
        $.it_behaves_like($$, 'invalid options', 'NameError');
    });
    $.context('when there are only password and endpoint', $$ => {
        $$.let('overrides', () => {
            return {
                password: 'password',
                endpoint: 'http://localhost:5985/wsman'
            }
        });
        $.it_behaves_like($$, 'invalid options', 'NameError');
    });
    $.context('when there are only certifcate and key', $$ => {
        $$.let('overrides', () => {
            return {
                client_cert: 'path/to/cert',
                key: 'path/to/key'
            }
        });
        $.it_behaves_like($$, 'invalid options', 'NameError');
    });
    $.context('when there are only key and endpoint', $$ => {
        $$.let('overrides', () => {
            return {
                key: 'path/to/key',
                endpoint: 'http://localhost:5985/wsman'
            }
        });
        $.it_behaves_like($$, 'invalid options', 'NameError');
    });
    $.context('when username, password, and endpoint are given', $$ => {
        $$.let('overrides', () => {
            return {
                user: 'Administrator',
                password: 'password',
                endpoint: 'http://localhost:5985/wsman'
            }
        });
        describe('.create_with_defaults', () => {
            it('creates a ConnectionOpts object', () => {
                const config = ConnectionOpts.create_with_defaults($$.get('overrides'));
                expect(config.get('user')).to.eq($$.get('overrides').user);
                expect(config.get('password')).to.eq($$.get('overrides').password);
                expect(config.get('endpoint')).to.eq($$.get('overrides').endpoint);
            });
        });
    });
    $.context('when certificate, key and endpoint are given', $$ => {
        $$.let('overrides', () => {
            return {
                client_cert: 'path/to/cert',
                client_key: 'path/to/key',
                endpoint: 'http://localhost:5985/wsman',
            }
        });
        describe('.create_with_defaults', () => {
            it('creates a ConnectionOpts object', () => {
                const config = ConnectionOpts.create_with_defaults($$.get('overrides'));
                expect(config.get('client_cert')).to.eq($$.get('overrides').client_cert);
                expect(config.get('client_key')).to.eq($$.get('overrides').client_key);
                expect(config.get('endpoint')).to.eq($$.get('overrides').endpoint);
            });
        });
    });
    $.context('when overrides are provided', $$ => {
        $$.let('overrides', () => {
            return {
                user: 'Administrator',
                password: 'password',
                endpoint: 'http://localhost:5985/wsman',
                transport: 'ssl'
            };
        });
        describe('.create_with_defaults', () => {
            it('creates a ConnectionOpts object', () => {
                const config = ConnectionOpts.create_with_defaults($$.get('overrides'));
                expect(config.get('user')).to.eq($$.get('overrides').user);
                expect(config.get('password')).to.eq($$.get('overrides').password);
                expect(config.get('endpoint')).to.eq($$.get('overrides').endpoint);
            });
        });
    });
    $.context('when receive_timeout is specified', $$ => {
        $$.let('overrides', () => {
            return {
                user: 'Administrator',
                password: 'password',
                endpoint: 'http://localhost:5985/wsman',
                receive_timeout: 120
            };
        });
        describe('.create_with_defaults', () => {
            it('creates a ConnectionOpts object', () => {
                const config = ConnectionOpts.create_with_defaults($$.get('overrides'));
                expect(config.get('receive_timeout')).to.eq($$.get('overrides').receive_timeout);
            });
        });
    });
    $.context('when operation_timeout is specified', $$ => {
        $$.let('overrides', () => {
            return {
                user: 'Administrator',
                password: 'password',
                endpoint: 'http://localhost:5985/wsman',
                operation_timeout: 120
            };
        });
        describe('.create_with_defaults', () => {
            it('creates a ConnectionOpts object', () => {
                const config = ConnectionOpts.create_with_defaults($$.get('overrides'));
                expect(config.get('operation_timeout')).to.eq($$.get('overrides').operation_timeout);
                expect(config.get('receive_timeout')).to.eq(($$.get('overrides').operation_timeout as number) + 10);
            });
        });
    });
    $.context('when invalid data types are given', $$ => {
        $$.let('overrides', () => {
            return {
                user: 'Administrator',
                password: 'password',
                endpoint: 'http://localhost:5985/wsman',
                operation_timeout: 'PT60S'
            };
        });
        describe('.create_with_defaults', () => {
            it('raises an error', () => {
                expect(() => ConnectionOpts.create_with_defaults($$.get('overrides'))).to.throw('operation_timeout must be an Integer');
            });
        });
    });
});