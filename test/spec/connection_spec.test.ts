import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as WinRM from '../../lib/winrm/internal.ts';
import { MochaLatte, default_connection_opts, turnOffLoggerErrorHandling } from './spec_helper.ts';

describe('WinRM.Connection', function () {
    const $ = new MochaLatte(this);
    $.context('new', () => {
        it('creates a new winrm session', () => {
            const connection = new WinRM.Connection(default_connection_opts());
            expect(connection).not.to.eq(undefined);
        });
    });
    $.context('shell("cmd")', () => {
        it('creates a new winrm session', () => {
            const connection = new WinRM.Connection(default_connection_opts()),
                cmd_shell = connection.shell('cmd');
            expect(cmd_shell).not.to.eq(undefined);
            expect(cmd_shell).to.be.instanceOf(WinRM.Shells.Cmd);
        });
    });
    $.context('shell("powershell")', () => {
        it('creates a new winrm session', () => {
            const connection = new WinRM.Connection(default_connection_opts()),
                cmd_shell = connection.shell('powershell');
            expect(cmd_shell).not.to.eq(undefined);
            expect(cmd_shell).to.be.instanceOf(WinRM.Shells.Powershell);
        });
    });
    $.context('shell("not_a_real_shell_type")', () => {
        it('creates a new winrm session', () => {
            const connection = new WinRM.Connection(default_connection_opts());
            turnOffLoggerErrorHandling(connection);
            expect(() => connection.shell('not_a_real_shell_type')).to.throw(WinRM.InvalidShellError);
        });
    });
});