import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as WinRM from '../../lib/winrm/internal.ts';
import { BindingFn, MochaLatte, stubbed_response } from './spec_helper.ts';
import { Node, DOMParser, XMLSerializer } from 'slimdom';

describe('response handler', function () {
    ['v1', 'v2', 'omi'].forEach(winrm_version => {
        const $ = new MochaLatte<void, {
            soap_fault: BindingFn
            open_shell: BindingFn
        }>(this);
        $.context(`winrm_version ${winrm_version}`, $$ => {
            $$.let('soap_fault', () => stubbed_response(`soap_fault_${winrm_version}.xml`));
            $$.let('open_shell', () => stubbed_response(`open_shell_${winrm_version}.xml`));

            describe(`successful 200 ${winrm_version} response`, () => {
                it('returns an xml doc', async function () {
                    const handler = new WinRM.ResponseHandler(await (await $$.get('open_shell'))(), 200),
                        xml_doc = handler.parse_to_xml();
                    expect(xml_doc instanceof Node && xml_doc.nodeType == 9).to.be.true;
                    expect(new XMLSerializer().serializeToString(xml_doc)).to.eq(new XMLSerializer().serializeToString(new DOMParser().parseFromString(await (await $$.get('open_shell'))(), 'application/xml')));
                });
            });
            describe(`failed 500 ${winrm_version} response`, () => {
                it('throws a WinRMHTTPTransportError', () => {
                    const handler = new WinRM.ResponseHandler('', 500);
                    expect(() => handler.parse_to_xml()).to.throw(WinRM.WinRMHTTPTransportError);
                });
            });
            describe(`failed 401 ${winrm_version} response`, () => {
                it('throws a WinRMAuthorizationError', () => {
                    const handler = new WinRM.ResponseHandler('', 401);
                    expect(() => handler.parse_to_xml()).to.throw(WinRM.WinRMAuthorizationError);
                });
            });
            describe(`failed 400 ${winrm_version} response`, () => {
                it('throws a WinRMWSManFault', async function () {
                    const handler = new WinRM.ResponseHandler(await (await $$.get('soap_fault'))(), 400);
                    try {
                        handler.parse_to_xml();
                    } catch (e) {
                        if (e instanceof WinRM.WinRMWSManFault) {
                            // console.log(e);
                            expect(e.fault_code).to.eq('2150858778');
                            expect(e.fault_description).to.include('The specified class does not exist in the given namespace');
                        } else if (e instanceof WinRM.WinRMSoapFault) {
                            expect(e.code).to.eq('SOAP-ENV:Receiver');
                            expect(e.subcode).to.eq('wsman:InternalError');
                            expect(e.reason).to.eq('get-instance: instance name parameter is missing');
                        } else {
                            throw e;
                        }
                    }
                });
            });
        });
    });
    describe('failed 500 WMI error response', () => {
        const wmi_error = stubbed_response('wmi_error_v2.xml');
        it('raises a WinRMWMIError', async function () {
            const handler = new WinRM.ResponseHandler(await (await wmi_error)(), 500);
            try {
                handler.parse_to_xml();
            } catch (e) {
                if (e instanceof WinRM.WinRMWMIError) {
                    expect(e.error_code).to.eq('2150859173');
                    expect(e.error).to.include('The WS-Management service cannot process the request.');
                } else {
                    throw e;
                }
            }
        });
    });
});