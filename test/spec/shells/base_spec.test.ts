import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as WinRM from '../../../lib/winrm/internal.ts';
import { ConnectionOpts } from 'lib/winrm/connection_opts';
import { Transport } from 'lib/winrm/types';
import winston from 'winston';
import { createStubInstance, mock, type SinonSpy, spy, type SinonMock } from 'sinon';
import { MochaLatte } from "../spec_helper.ts";

class DummyShell extends WinRM.Shells.Base {
    static finalize: (heldValue: { connection_opts: ConnectionOpts; transport: Transport; shell_id: string; }) => () => void
    static close_shell: (connection_opts: ConnectionOpts, transport: Transport, shell_id: string) => Promise<null>
    static #closed = false;
    static get closed() {
        return this.#closed;
    }
    static set closed(value: boolean) {
        this.#closed = value;
    }
    static {
        this.finalize = function(heldValue) {
            return () => {
                void this.close_shell(heldValue.connection_opts, heldValue.transport, heldValue.shell_id);
            }
        }
        this.close_shell = function() {
            this.closed = true;
            return new Promise(r => r(null));
        }
    }
    open_shell(): Promise<string> {
        (this.constructor as typeof DummyShell).closed = false;
        return new Promise(r => r('shell_id'));
    }
    send_command(): Promise<string> {
        return new Promise(r => r('command_id'));
    }
    out_streams(): string[] {
        return ['std'];
    }
}

describe('DummyShell', function () {
    const $ = new MochaLatte<DummyShell, {
        retry_limit: number,
        shell_id: string,
        output: string,
        command_id: string,
        payload: string,
        command: string,
        arguments: string[],
        output_message: WinRM.PSRP.Message,
        connection_options: ConnectionOpts,
        transport: Transport,
        reader: unknown,
        mock: SinonMock,
        spy: SinonSpy
    }>(this);
    $.let('retry_limit', () => 1);
    $.let('shell_id', () => 'shell_id');
    $.let('output', () => 'output');
    $.let('command_id', () => 'command_id');
    $.let('payload', () => 'message_payload');
    $.let('command', () => 'command');
    $.let('arguments', () => ['args']);
    $.let('output_message', () => {
        const stub = createStubInstance(WinRM.WSMV.Base);
        stub.build.returns('<obj><S>output_message</S></obj>');
        return stub;
    });
    $.let('spy', () => spy());
    $.subject($$ => {
        const dummyShell = new DummyShell($$.get('connection_options'), $$.get('transport'), winston.loggers.get('test'));
        $$.let('mock', () => mock(dummyShell))
        return dummyShell;
    });
    before(() => {
        $.subject();
        $.get('mock')
    });
    $.shared_examples('retry shell command', ($$, fault: string) => {
        it('only closes the shell if there are too many', async () => {
            if (fault == WinRM.Shells.Base.TOO_MANY_COMMANDS) {
                $.get('mock').expects('close_shell').called;
            } else {
                $.get('mock').expects('close_shell').notCalled;
            }
            await $$.subject().run($$.get('command'), $$.get('arguments'));
            expect(() => $.get('mock').verify()).to.not.throw();
        });

        it('opens a new shell', async () => {
            $.get('mock').expects('open').calledTwice;
            await $$.subject().run($$.get('command'), $$.get('arguments'));
            expect(() => $.get('mock').verify()).to.not.throw();
        });

        it('retries the command once', async () => {
            $.get('mock').expects('send_command').calledTwice;
            await $$.subject().run($$.get('command'), $$.get('arguments'));
            expect(() => $.get('mock').verify()).to.not.throw();
        });
    });
});