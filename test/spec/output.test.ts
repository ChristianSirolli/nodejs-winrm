import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as WinRM from '../../lib/winrm/internal.ts';
import { MochaLatte } from './spec_helper.ts';

describe('WinRM.Output', function() {
    const $ = new MochaLatte<WinRM.Output, {
        exitcode: number
    }>(this);
    $.subject(() => new WinRM.Output());
    $.context('when there is no output', $$ => {
        describe('.stdout', () => {
            it('is empty', () => {
                expect($$.subject().stdout()).to.be.empty;
            });
        });
        describe('.stderr', () => {
            it('is empty', () => {
                expect($$.subject().stderr()).to.be.empty;
            });
        });
        describe('.output', () => {
            it('is empty', () => {
                expect($$.subject().output()).to.be.empty;
            });
        });
    });
    $.context('when there is only one line', $$ => {
        // $$.subject().data = [];
        describe('.stdout', () => {
            it('is equal to that line', () => {
                $$.subject().push({ stdout: 'foo' });
                expect($$.subject().stdout()).to.eq('foo');
            });
        });
        describe('.stderr', () => {
            it('is equal to that line', () => {
                $$.subject().push({ stderr: 'foo' });
                expect($$.subject().stderr()).to.eq('foo');
            });
        });
        describe('.output', () => {
            it('is equal to stdout', () => {
                $$.subject().pop();
                $$.subject().pop();
                $$.subject().push({ stdout: 'foo' });
                expect($$.subject().output()).to.eq('foo');
            });
            it('is equal to stderr', () => {
                $$.subject().pop();
                $$.subject().push({ stderr: 'foo' });
                expect($$.subject().output()).to.eq('foo');
            });
        });
    });
    $.context('when there is one line of each type', $$ => {
        // $$.subject().data = [];
        $$.subject().push({ stdout: 'foo' });
        $$.subject().push({ stderr: 'bar' });
        describe('.stdout', () => {
            it('is equal to that line', () => {
                expect($$.subject().stdout()).to.eq('foo');
            });
        });
        describe('.stderr', () => {
            it('is equal to that line', () => {
                expect($$.subject().stderr()).to.eq('bar');
            });
        });
        describe('.output', () => {
            it('is equal to stdout + stderr', () => {
                expect($$.subject().output()).to.eq('foobar');
            });
        });
    });
    $.context('when there are multiple lines', $$ => {
        // $$.subject().data = [];
        $$.subject().push({ stdout: 'I can have a newline\nanywhere, ' });
        $$.subject().push({ stderr: 'I can also have stderr' });
        $$.subject().push({ stdout: 'or stdout', stderr: ' and stderr' });
        $$.subject().push({});
        $$.subject().push({ stdout: ' or nothing! (above)' });
        describe('.stdout', () => {
            it('is equal to that line', () => {
                expect($$.subject().stdout()).to.eq('I can have a newline\nanywhere, or stdout or nothing! (above)');
            });
        });
        describe('.stderr', () => {
            it('is equal to that line', () => {
                expect($$.subject().stderr()).to.eq('I can also have stderr and stderr');
            });
        });
        describe('.output', () => {
            it('is equal to that line', () => {
                expect($$.subject().output()).to.eq('I can have a newline\nanywhere, I can also have stderror stdout and stderr or nothing! (above)');
            });
        });
    });
    describe('.exitcode', () => {
        $.context('when a valid exit code is set', $$ => {
            $$.let('exitcode', () => 0);
            it('sets the exit code', () => {
                $$.subject().exitcode = $$.get('exitcode');
                expect($$.subject().exitcode).to.eq($$.get('exitcode'));
            });
        });
        $.context('when an invalid exit code is set', $$ => {
            $$.let('exitcode', () => 'bad');
            it('sets the exit code', () => {
                expect(() => $$.subject().exitcode = $$.get('exitcode')).to.throw(WinRM.InvalidExitCode);
            });
        });
    });
});