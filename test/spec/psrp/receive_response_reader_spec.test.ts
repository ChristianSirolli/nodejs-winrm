import { describe, it } from "mocha";
import { type BindingFn, MochaLatte, stubbed_clixml, stubbed_response } from "../spec_helper.ts";
import * as WinRM from '../../../lib/winrm/internal.ts';
import { expect, use } from "chai";
import type { ReceiveResponseReader } from "../../../lib/winrm/psrp/receive_response_reader.ts";
import type { Message } from "../../../lib/winrm/psrp/message.ts";
import { pack } from "../../../lib/winrm/helpers.ts";
import { createLogger, transports } from "winston";
// import { double } from '../../../../mochaLatte/lib/index.ts';
import type { MessageType, Transport } from "../../../lib/winrm/types.ts";
import type { Fragment } from "../../../lib/winrm/psrp/fragment.ts";
// import type { CommandOutput } from "lib/winrm/wsmv/command_output.ts";
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { createStubInstance } from 'sinon';
import { DOMParser } from "slimdom";
import chaiString from '@achingbrain/chai-string';
import { Buffer } from "node:buffer";

use(chaiString);

describe('WinRM.PSRP.ReceiveResponseReader', function () {
    const $ = new MochaLatte<ReceiveResponseReader, {
        shell_id: string
        command_id: string
        output_message: WinRM.WSMV.Base
        test_data_xml_template: BindingFn
        test_data_xml_template_not_done: BindingFn
        test_data_text: string
        test_data: Promise<string> | string
        message: Promise<Message>
        message_type: MessageType
        fragment: Promise<Fragment>
        test_data_stdout: Promise<string>
        transport: Transport
        test_data_error_xml_template: BindingFn
        error_message: string
        script_root: string
        category_message: string
        stack_trace: string
        error_id: string
        pipeline_state: number
        test_data_xml_not_done: Promise<string>
        test_data_xml_done: Promise<string>
    }>(this);
    // console.log('Parent Context:', $._sym);
    $.let("shell_id", () => "F4A2622B-B842-4EB8-8A78-0225C8A993DF");
    $.let("command_id", () => "A2A2622B-B842-4EB8-8A78-0225C8A993DF");
    $.let('output_message', () => {
        const stub = createStubInstance(WinRM.WSMV.Base);
        stub.build.returns('<obj><S>output_message</S></obj>');
        return stub;
    }, false);
    // $.let('output_message', $$ => new WinRM.WSMV.CommandOutput(default_connection_opts(false), $$.binding as { shell_id: string, command_id: string }), false)
    // $.let('output_message', () => double(WinRM.WSMV.CommandOutput, { build: () => '<?xml version="1.0"?><s:Envelope>output_message</s:Envelope>' }));
    $.let("test_data_xml_template", () => stubbed_response("get_powershell_output_response.xml"), false);
    $.let("test_data_xml_template_not_done", () => stubbed_response("get_powershell_output_response_not_done.xml"), false);
    $.let("test_data_text", () => "some data", false);
    $.let("test_data", $$ => `<obj><S>${$$.get('test_data_text')}</S></obj>`, false);
    $.let("message", async $$ => {
        // console.log('in let message, message_type is', $$.get('message_type'), 'context:', $$._sym);
        return new WinRM.PSRP.Message($$.get('shell_id'), $$.get('message_type'), $$.get('test_data') instanceof Promise ? await $$.get('test_data') : $$.get('test_data') as string, $$.get('command_id'));
    }, false);
    $.let("fragment", async $$ => new WinRM.PSRP.Fragment(1, (await $$.get('message')).bytes()), false);
    $.let("test_data_stdout", async $$ => Buffer.from(pack((await $$.get('fragment')).bytes(), "C*"), 'ascii').toString('base64'));
    $.let('transport', $$ => {
        const stub = createStubInstance(WinRM.HTTP.HttpTransport);
        stub.send_request.returns((async () => {
            const parser = new DOMParser(),
                message = await (await $$.get('test_data_xml_template'))($$.getBinding(['command_id', 'test_data_stdout']));
            // console.log(message);
            return message ? parser.parseFromString(message, "application/xml") : null;
        })());
        return stub;
    }, false)

    // beforeEach(async function () (
    //     allow($.get('transport'), "send_request", (await $.get('test_data_xml_template'))($.binding))
    // ));

    $.subject($$ => new $.described_class($$.get('transport'), createLogger({ level: 'debug', transports: [new transports.Console()] })));

    describe(".read_output", () => {
        $.context("response doc stdout with pipeline output", $$ => {
            $$.let("message_type", () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_output);

            it("outputs to stdout", async function () {
                expect((await $$.subject().read_output($$.get('output_message'))).stdout()).to.eq(`${$$.get('test_data_text')}\r\n`);
            });
        });

        $.context("response doc stdout error record", $$ => {
            $$.let("message_type", () => WinRM.PSRP.Message.MESSAGE_TYPES.error_record);
            $$.let("test_data_error_xml_template", () => stubbed_clixml("error_record.xml"), false);
            $$.let("error_message", () => "an error");
            $$.let("script_root", () => "script_root");
            $$.let("category_message", () => "category message");
            $$.let("stack_trace", () => "stack trace");
            $$.let("error_id", () => "Microsoft.PowerShell.Commands.WriteErrorException");
            $$.let("test_data", async () => (await $$.get('test_data_error_xml_template'))($$.getBinding(['category_message', 'error_message', 'error_id', 'script_root', 'stack_trace'])), false);

            it("outputs to stderr", async function () {
                expect((await $$.subject().read_output($$.get('output_message'))).stderr()).to.match(new RegExp($$.get('error_message')));
            });
        });

        $.context("response doc failed pipeline state", $$ => {
            $$.let("message_type", () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_state);
            $$.let("test_data_error_xml_template", () => stubbed_clixml("pipeline_state.xml"), false);
            $$.let("pipeline_state", () => WinRM.PSRP.MessageData.PipelineState.FAILED);
            $$.let("error_message", () => "an error");
            $$.let("category_message", () => "category message");
            $$.let("error_id", () => "Microsoft.PowerShell.Commands.WriteErrorException");
            $$.let("test_data", async () => (await $$.get('test_data_error_xml_template'))($$.getBinding(['pipeline_state', 'error_message', 'error_id', 'category_message'])), false);

            it("outputs to stderr", async function () {
                expect((await $$.subject().read_output($$.get('output_message'))).stderr()).to.match(new RegExp($$.get('error_message')));
            });
        });

        $.context("response doc writing error to host", $$ => {
            $$.let("message_type", () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_host_call);
            $$.let("test_data", () => `<Obj RefId='0'><MS><I64 N='ci'>-100</I64><Obj N='mi' RefId='1'><TN RefId='0'><T>System.Management.Automation.Remoting.RemoteHostMethodId</T><T>System.Enum</T><T>System.ValueType</T><T>System.Object</T></TN><ToString>WriteErrorLine</ToString><I32>18</I32></Obj><Obj N='mp' RefId='2'><TN RefId='1'><T>System.Collections.ArrayList</T><T>System.Object</T></TN><LST><S>errors</S></LST></Obj></MS></Obj>`);

            it("outputs to stderr", async function () {
                expect((await $$.subject().read_output($$.get('output_message'))).stderr()).to.eq("errors\r\n");

            });
        });

        $.context("response doc writing output to host", $$ => {
            $$.let("message_type", () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_host_call);
            $$.let("test_data", () => `<Obj RefId='0'><MS><I64 N='ci'>-100</I64><Obj N='mi' RefId='1'><TN RefId='0'><T>System.Management.Automation.Remoting.RemoteHostMethodId</T><T>System.Enum</T><T>System.ValueType</T><T>System.Object</T></TN><ToString>WriteLine</ToString><I32>18</I32></Obj><Obj N='mp' RefId='2'><TN RefId='1'><T>System.Collections.ArrayList</T><T>System.Object</T></TN><LST><S>output</S></LST></Obj></MS></Obj>`);

            it("outputs to stderr", async function () {
                expect((await $$.subject().read_output($$.get('output_message'))).stdout()).to.eq("output\r\n");

            });
        })
    });

    describe(".read_message", () => {
        $.context("do not wait for done state", $$ => {
            // console.log('Child Context:', $$._sym);
            $$.let("message_type", () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_output);

            it("outputs the message in an array", async function () {
                expect(await $$.subject().read_message($$.get('output_message'))).to.have.lengthOf(1);
                expect((await $$.subject().read_message($$.get('output_message')))[0].data).to.endWith((await $$.get('message')).data);

            });
        });

        $.context("read in a block", $$ => {
            // console.log('Child Context:', $$._sym);
            $$.let("message_type", () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_output);

            it("outputs the message in an array", async function () {
                (await $$.subject().read_message($$.get('output_message'), async msg => {
                    // console.log('in test, message_type is', $$.get('message_type'), 'context:', $$._sym);
                    expect(msg.data).to.have.string((await $$.get('message')).data);
                }));
                // !.forEach(msg => {
                //     expect(msg.data).to.have.string($$.get('message').data);
                // });

            });
        });

        $.context("wait for done state", $$ => {
            // console.log('Child Context:', $$._sym);
            $$.let("message_type", () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_output);
            $$.let("test_data_xml_not_done", async () => (await $$.get('test_data_xml_template_not_done'))($$.getBinding(['command_id', 'test_data_stdout'])), false);
            $$.let("test_data_xml_done", async () => (await $$.get('test_data_xml_template'))($$.getBinding(['command_id', 'test_data_stdout'])), false);
            $$.let("transport", $$$ => ({
                send_request: async () => [await $$$.get('test_data_xml_not_done'), await $$$.get('test_data_xml_done')]
            }), false);

            it("outputs two messages", async function () {
                expect(await $$.subject().read_message($$.get('output_message'), true)).to.have.lengthOf(2);

            });

            it("outputs the first message", async function () {
                expect((await $$.subject().read_message($$.get('output_message'), true))[0].data).to.endWith((await $$.get('message', $$)).data);

            });

            it("outputs the second message", async function () {
                expect((await $$.subject().read_message($$.get('output_message'), true))[1].data).to.endWith((await $$.get('message', $$)).data);

            });
        });
    });
});