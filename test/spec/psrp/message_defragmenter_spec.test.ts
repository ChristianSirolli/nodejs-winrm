import { describe } from "mocha";
import { MochaLatte } from "../spec_helper.ts";
import * as WinRM from '../../../lib/winrm/internal.ts';
import { expect } from "chai";
import type { MessageDefragmenter } from "../../../lib/winrm/psrp/message_defragmenter.ts";
import type { Message } from "../../../lib/winrm/psrp/message.ts";
import type { Fragment } from "../../../lib/winrm/psrp/fragment.ts";
import { pack } from "../../../lib/winrm/helpers.ts";
import { Buffer } from "node:buffer";

describe('WinRM.PSRP.MessageDefragmenter', function () {
    const $ = new MochaLatte<MessageDefragmenter, {
        bytes: string,
        blob: Message,
        fragment1: Fragment,
        fragment2: Fragment,
        fragment3: Fragment,
    }, Promise<Message | undefined>>(this);
    $.context('a real life fragment', $$ => {
        $$.let('bytes', () => Buffer.from("\x00\x00\x00\x00\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00\x01\x03\x00\x00\x00I\x01" +
            "\x00\x00\x00\x04\x10\x04\x00Kk/=Z\xD3-E\x81v\xA0+6\xB1\xD3\x88\n\xED\x90\x9Cj\xE7PG" +
            "\x9F\xA2\xB2\xC99to9\xEF\xBB\xBF<S>some data_x000D__x000A_</S>", 'ascii').toString('ascii'))
        $$.subject(() => new $.described_class().defragment(Buffer.from($$.get('bytes'), 'ascii').toString('base64')));
        it('parses the data', async () => {
            expect((await $$.subject())?.data).to.eq(Buffer.from("\xEF\xBB\xBF<S>some data_x000D__x000A_</S>", 'ascii').toString('ascii'));
        });
        it('parses the destination', async () => {
            expect((await $$.subject())?.destination).to.eq(1);
        });
        it('parses the message type', async () => {
            expect((await $$.subject())?.type).to.eq(WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_output);
        });
    });

    $.context<MessageDefragmenter>('multiple fragments', $$ => {
        $$.let('blob', () => new WinRM.PSRP.Message(
            'bc1bfbba-8215-4a04-b2df-7a3ac0310e16',
            WinRM.PSRP.Message.MESSAGE_TYPES.session_capability,
            'This is a fragmented message'
        ));
        $$.let('fragment1', () => new WinRM.PSRP.Fragment(1, $$.get('blob').bytes().slice(0,6), 0, true, false));
        $$.let('fragment2', () => new WinRM.PSRP.Fragment(1, $$.get('blob').bytes().slice(6,11), 1, false, false));
        $$.let('fragment3', () => new WinRM.PSRP.Fragment(1, $$.get('blob').bytes().slice(11), 2, false, true));

        it('pieces the message together', async () => {
            await $$.subject()?.defragment(Buffer.from(pack($$.get('fragment1').bytes(), 'C*'), 'ascii').toString('base64'));
            await $$.subject()?.defragment(Buffer.from(pack($$.get('fragment2').bytes(), 'C*'), 'ascii').toString('base64'));
            const message = await $$.subject().defragment(Buffer.from(pack($$.get('fragment3').bytes(), 'C*'), 'ascii').toString('base64'));

            expect(message?.data?.slice(3)).to.eq($$.get('blob').data);
        });
    });
});

