import { describe, it } from 'mocha';
import { expect } from 'chai';
import * as WinRM from '../../../../lib/winrm/internal.ts';
import { MochaLatte } from '../../spec_helper.ts';

describe('WinRM.PSRP.MessageData.Base', function() {
    const $ = new MochaLatte<WinRM.PSRP.MessageData.Base, { raw_data: string }>(this);
    $.let('raw_data', () => 'raw_data');
    
    $.subject(() => new WinRM.PSRP.MessageData.Base($.get('raw_data')));
    
    it('holds raw message data', () => {
        expect($.subject().raw).to.eq($.get('raw_data'));
    });
});