import { expect } from "chai";
import type { PipelineOutput } from "lib/winrm/psrp/message_data/pipeline_output";
import { describe } from "mocha";
import { MochaLatte } from "test/spec/spec_helper";

describe('WinRM.PSRP.MessageData.PipelineOutput', function () {
    const $ = new MochaLatte<PipelineOutput, {
        raw_data: string
    }>(this);
    $.context('receiving output with BOM and no new line', $$ => {
        $$.let('raw_data', () => "\xEF\xBB\xBF<obj><S>some data</S></obj>");
        $$.subject(() => new $.described_class($$.get('raw_data')));
        it('output removes BOM and adds newline', () => {
            expect($$.subject().output()).to.eq("some data\r\n");
        });
    });
    $.context('receiving output with encoded new line', $$ => {
        $$.let('raw_data', () => "<obj><S>some data_x000D__x000A_</S></obj>");
        $$.subject(() => new $.described_class($$.get('raw_data')));
        it('decodes without double newline', () => {
            expect($$.subject().output()).to.eq("some data\r\n");
        });
    });
    $.context('receiving output with new line in middle', $$ => {
        $$.let('raw_data', () => `<obj><S>some_x000D__x000A_data</S></obj>`);
        $$.subject(() => new $.described_class($$.get('raw_data')));
        it('decodes and replaces newline', () => {
            expect($$.subject().output()).to.eq("some\r\ndata\r\n");
        });
    });
});