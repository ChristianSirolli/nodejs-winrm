import { describe, it } from 'mocha';
import { expect } from 'chai';
import { MochaLatte, stubbed_clixml, type BindingFn } from '../../spec_helper.ts';
import { ErrorRecord } from 'lib/winrm/psrp/message_data/error_record.ts';

describe('WinRM.PSRP.MessageData.ErrorRecord', function () {
    const $ = new MochaLatte<ErrorRecord, {
        test_data_xml_template: BindingFn,
        error_message: string,
        script_root: string
        category_message: string
        stack_trace: string
        error_id: string
        raw_data: Promise<string>
    }, Promise<ErrorRecord>>(this);
    $.let('test_data_xml_template', () => stubbed_clixml('error_record.xml'), false);
    $.let('error_message', () => 'an error');
    $.let('script_root', () => 'script_root');
    $.let('category_message', () => 'category message');
    $.let('stack_trace', () => 'stack trace');
    $.let('error_id', () => 'Microsoft.PowerShell.Commands.WriteErrorException');
    $.let('raw_data', async () => (await $.get('test_data_xml_template'))($.binding), false);
    $.subject(async () => new $.described_class(await $.get('raw_data')));

    // let test_data_xml_template: (...args: unknown[]) => string,
    //     raw_data: string,
    //     subject: ErrorRecord;
    // const described_class = _describedClass.call(this) as typeof ErrorRecord,
    //     error_message = 'an error',
    //     script_root = 'script_root',
    //     category_message = 'category message',
    //     stack_trace = 'stack trace',
    //     error_id = 'Microsoft.PowerShell.Commands.WriteErrorException';
    // before(async function() {
    //     test_data_xml_template = await stubbed_clixml('error_record.xml');
    //     raw_data = test_data_xml_template(category_message, error_message, error_id, script_root, stack_trace);
    //     $.subject(() => new $.described_class(raw_data));
    // });
    it('returns the exception', async () => {
        expect(await $.subject()).to.have.nested.property('exception.message', $.get('error_message'));
    });
    it('returns the FullyQualifiedErrorId', async function () {
        expect(await $.subject()).to.have.property('fully_qualified_error_id', $.get('error_id'));
    });
    it('returns the invocation info', async () => {
        expect(await $.subject()).to.have.nested.property('invocation_info.line', `write-error '${$.get('error_message')}'`);
    });
    it('converts camel case properties to underscore', async () => {
        expect(await $.subject()).to.have.nested.property('invocation_info.ps_script_root', $.get('script_root'));
    });
    it('returns the error category message', async () => {
        expect(await $.subject()).to.have.property('error_category_message', $.get('category_message'));
    });
    it('returns the script stack trace', async () => {
        expect(await $.subject()).to.have.property('error_details_script_stack_trace', $.get('stack_trace'));
    });
});