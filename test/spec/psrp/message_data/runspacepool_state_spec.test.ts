import { expect } from "chai";
import * as WinRM from '../../../../lib/winrm/internal.ts';
import type { RunspacepoolState } from "lib/winrm/psrp/message_data/runspacepool_state";
import { describe, it } from "mocha";
import { MochaLatte } from "test/spec/spec_helper";

describe('WinRM.PSRP.MessageData.RunspacepoolState', function () {
    const $ = new MochaLatte<RunspacepoolState, {
        raw_data: string
    }>(this);
    $.let('raw_data', () => "\xEF\xBB\xBF<Obj RefId=\"0\"><MS><I32 N=\"RunspaceState\">2</I32></MS></Obj>");

    $.subject(() => new $.described_class($.get('raw_data')));

    it('parses runspace state', () => {
        expect($.subject().runspace_state()).to.eq(WinRM.PSRP.MessageData.RunspacepoolState.OPENED);
    });
});