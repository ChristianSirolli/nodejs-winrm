import { describe } from "mocha";
import { BindingFn, MochaLatte, stubbed_clixml } from "test/spec/spec_helper";
import * as WinRM from '../../../../lib/winrm/internal.ts';
import { PipelineState } from "lib/winrm/psrp/message_data/pipeline_state.ts";
import { expect } from "chai";

describe('WinRM.PSRP.MessageData.PipelineState', function() {
    const $ = new MochaLatte<Promise<PipelineState>, {
        test_data_xml_template: BindingFn,
        pipeline_state: number,
        error_message: string,
        category_message: string,
        error_id: string,
        raw_data: Promise<string>
    }>(this);
    $.let('test_data_xml_template', () => stubbed_clixml('pipeline_state.xml'));
    $.let('pipeline_state', () => WinRM.PSRP.MessageData.PipelineState.FAILED);
    $.let('error_message', () => 'an error occurred');
    $.let('category_message', () => 'category message');
    $.let('error_id', () => 'an error occurred');
    $.let('raw_data', async $$ => (await $$.get('test_data_xml_template'))($$.binding))
    $.subject(async $$ => new $.described_class(await $$.get('raw_data')));

    it('returns the state', async function () {
        expect((await $.subject()).pipeline_state()).to.eq($.get('pipeline_state'));
    });

    it('returns the exception', async () => {
        return expect(await $.subject()).to.have.nested.property('exception_as_error_record.exception.message', $.get('error_message'));
    });

    it('returns the FullyQualifiedErrorId', async () => {
        return expect(await $.subject()).to.have.nested.property('exception_as_error_record.fully_qualified_error_id', $.get('error_id'));
    });

    it('returns the error category message', async () => {
        return expect(await $.subject()).to.have.nested.property('exception_as_error_record.error_category_message', $.get('category_message'));
    });

    $.context('state is not failed', $$ => {
        $$.let('pipeline_state', () => WinRM.PSRP.MessageData.PipelineState.COMPLETED);
        it('has an undefined exception', async function () {
            expect(await $$.subject($$)).to.have.property('exception_as_error_record', undefined)
            // console.log($.get('pipeline_state'), $$.get('pipeline_state'));
        });
    });
});