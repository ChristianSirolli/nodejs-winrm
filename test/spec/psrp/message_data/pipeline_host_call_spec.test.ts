import { expect } from "chai";
import type { PipelineHostCall } from "lib/winrm/psrp/message_data/pipeline_host_call";
import { describe } from "mocha";
import { MochaLatte } from "test/spec/spec_helper";

describe('WinRM.PSRP.MessageData.PipelineHostCall', function () {
    const $ = new MochaLatte<PipelineHostCall, {
        raw_data: string
    }>(this);
    $.let('raw_data', () =>
        "\xEF\xBB\xBF<Obj RefId=\"0\"><MS><I64 N=\"ci\">-100</I64><Obj N=\"mi\" RefId=\"1\">" +
        '<TN RefId="0"><T>System.Management.Automation.Remoting.RemoteHostMethodId</T>' +
        '<T>System.Enum</T><T>System.ValueType</T><T>System.Object</T></TN>' +
        '<ToString>WriteLine3</ToString><I32>17</I32></Obj><Obj N="mp" RefId="2">' +
        '<TN RefId="1"><T>System.Collections.ArrayList</T><T>System.Object</T></TN><LST>' +
        '<I32>7</I32><I32>0</I32><S>hello</S></LST></Obj></MS></Obj>'
    );
    
    $.subject(() => new $.described_class($.get('raw_data')));
    
    it('parses method identifier', () => {
        expect($.subject().method_identifier()).to.eq('WriteLine3');
    });

    it('parses method parameters', () => {
        expect($.subject().method_parameters()[0].s[0]).to.eq('hello');
    });
});