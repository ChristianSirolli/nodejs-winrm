import { expect } from "chai";
import type { SessionCapability } from "lib/winrm/psrp/message_data/session_capability";
import { describe, it } from "mocha";
import { MochaLatte } from "test/spec/spec_helper";

describe('WinRM.PSRP.MessageData.SessionCapability', function () {
    const $ = new MochaLatte<SessionCapability, {
        protocol_version: string,
        ps_version: string,
        serialization_version: string
        raw_data: string
    }>(this);
    $.let('protocol_version', () => '2.2');
    $.let('ps_version', () => '2.0');
    $.let('serialization_version', () => '1.1.0.1');
    $.let('raw_data', () => "\xEF\xBB\xBF<Obj RefId=\"0\"><MS>"+
      `<Version N="protocolversion">${$.get('protocol_version')}</Version>`+
      `<Version N="PSVersion">${$.get('ps_version')}</Version>`+
      `<Version N="SerializationVersion">${$.get('serialization_version')}</Version></MS></Obj>`);

    $.subject(() => new $.described_class($.get('raw_data')));

    it('parses protocol version', () => {
        expect($.subject().protocol_version()).to.eq($.get('protocol_version'));
    });

    it('parses ps version', () => {
        expect($.subject().ps_version()).to.eq($.get('ps_version'));
    });

    it('parses serialization version', () => {
        expect($.subject().serialization_version()).to.eq($.get('serialization_version'));
    });
});