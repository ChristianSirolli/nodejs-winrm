import { describe, it } from "mocha";
import { MochaLatte } from "../spec_helper.ts";
import * as WinRM from '../../../lib/winrm/internal.ts';
import { expect } from "chai";

describe('WinRM.PSRP.UUID', function () {
    const $ = new MochaLatte<WinRM.PSRP.UUID>(this);
    $.subject(() => Reflect.construct(WinRM.PSRP.UUID, []));
    const uuid_helper = $.subject();
    $.context('uuid is null', () => {
        const uuid = undefined;
        it('should return an empty byte array', () => {
            const bytes = uuid_helper.uuid_to_windows_guid_bytes(uuid);
            expect(bytes).to.deep.eq([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);
        });
    });
    $.context('uuid 08785e96-eb1b-4a74-a767-7b56e8f13ea9 with lower case letters', () => {
        const uuid = '08785e96-eb1b-4a74-a767-7b56e8f13ea9';
        it('should return a Windows GUID struct compatible little endian byte array', () => {
            const bytes = uuid_helper.uuid_to_windows_guid_bytes(uuid);
            expect(bytes).to.deep.eq([150, 94, 120, 8, 27, 235, 116, 74, 167, 103, 123, 86, 232, 241, 62, 169]);
        });
    });
    $.context('uuid 045F9E19D-8B77-4394-AB0C-197497661668 with upper case letters', () => {
        const uuid = '45F9E19D-8B77-4394-AB0C-197497661668';
        it('should return a Windows GUID struct compatible little endian byte array', () => {
            const bytes = uuid_helper.uuid_to_windows_guid_bytes(uuid);
            expect(bytes).to.deep.eq([157, 225, 249, 69, 119, 139, 148, 67, 171, 12, 25, 116, 151, 102, 22, 104]);
        });
    });
});