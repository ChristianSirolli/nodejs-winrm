import { describe } from "mocha";
import { MochaLatte } from "../spec_helper.ts";
import * as WinRM from '../../../lib/winrm/internal.ts';
import { expect } from "chai";
import type { Message } from "../../../lib/winrm/psrp/message.ts";
import type { MessageType } from "lib/winrm/types.ts";

describe('WinRM.PSRP.Message', function () {
    const $ = new MochaLatte<Message, { payload: string }>(this);
    $.context('all fields provided', $$ => {
        $$.let('payload', () => 'this is my payload');
        $$.subject($$$ => new $.described_class(
            'bc1bfbba-8215-4a04-b2df-7a3ac0310e16',
            WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_output,
            $$$.get('payload'),
            '4218a578-0f18-4b19-82c3-46b433319126'
        ));

        it('sets the destination to server LE', () => {
            expect($$.subject().bytes().slice(0, 4)).to.deep.eq([2, 0, 0, 0]);
        });
        it('sets the message type LE', () => {
            expect($$.subject().bytes().slice(4, 8)).to.deep.eq([4, 16, 4, 0]);
        });
        it('sets the runspace pool id', () => {
            expect($$.subject().bytes().slice(8, 24)).to.deep.eq(
                [186, 251, 27, 188, 21, 130, 4, 74, 178, 223, 122, 58, 192, 49, 14, 22]
            );
        });
        it('sets the pipeline id', () => {
            expect($$.subject().bytes().slice(24, 40)).to.deep.eq(
                [120, 165, 24, 66, 24, 15, 25, 75, 130, 195, 70, 180, 51, 49, 145, 38]
            );
        });
        it('prefixes the blob with BOM', () => {
            expect($$.subject().bytes().slice(40, 43)).to.deep.eq([239, 187, 191]);
        });
        it('contains at least the first 8 bytes of the XML payload', () => {
            expect($$.subject().bytes().slice(43)).to.deep.eq([...Buffer.from($$.get('payload'), 'utf-8')]);
        });
        it('parses the data', () => {
            expect($$.subject().parsed_data).to.be.instanceOf(WinRM.PSRP.MessageData.PipelineOutput)
        });
    });

    $.context('create', () => {
        it('raises error when message type is not valid', () => {
            expect(() => new WinRM.PSRP.Message(
                'bc1bfbba-8215-4a04-b2df-7a3ac0310e16',
                0x00000000 as MessageType,
                `<Obj RefId="0"/>`,
                '4218a578-0f18-4b19-82c3-46b433319126'
            )).to.throw(Error);
        });
    });

    $.context('no command id', $$ => {
        $$.subject(() => {
            const payload = `<Obj RefId="0"><MS><Version N="protocolversion">2.3</Version>
<Version N="PSVersion">2.0</Version><Version N="SerializationVersion">1.1.0.1</Version></MS>
</Obj>`
            return new WinRM.PSRP.Message(
                'bc1bfbba-8215-4a04-b2df-7a3ac0310e16',
                WinRM.PSRP.Message.MESSAGE_TYPES.session_capability,
                payload
            );
        });

        it('sets the pipeline id to empty', () => {
            expect($$.subject().bytes().slice(24, 40)).to.deep.eq([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        });
    });
});