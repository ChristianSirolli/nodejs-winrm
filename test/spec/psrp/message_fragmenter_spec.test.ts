import { describe } from "mocha";
import { MochaLatte } from "../spec_helper.ts";
import * as WinRM from '../../../lib/winrm/internal.ts';
import { expect } from "chai";
import type { MessageFragmenter } from "../../../lib/winrm/psrp/message_fragmenter.ts";
import type { Message } from "../../../lib/winrm/psrp/message.ts";
import type { Fragment } from "../../../lib/winrm/psrp/fragment.ts";

type PFs = Promise<Fragment[]>;

describe('WinRM.PSRP.MessageFragmenter', function () {
    const $ = new MochaLatte<MessageFragmenter, {
        message: Message,
        data: string
    }, PFs>(this);
    $.let('message', $$ => new WinRM.PSRP.Message(
        'bc1bfbba-8215-4a04-b2df-7a3ac0310e16',
        WinRM.PSRP.Message.MESSAGE_TYPES.session_capability,
        $$.get('data')
    )
    );

    $.subject(async $$ => {
        // console.log('Subject data:', $$.get('message'));
        const fragmenter = new $.described_class(45),
            fragments: Fragment[] = [];
        await fragmenter.fragment($$.get('message'), fragment => {
            fragments.push(fragment);
            return new Promise(() => undefined);
        });
        return fragments;
    });

    $.context('one fragment', async $$ => {
        $$.let('data', () => 'th');
        await $$.subject($$).then(subject => {
            it('returns 1 fragment', function () {
                expect(subject).to.have.lengthOf(1);
            });

            it('has blob data equal to the message bytes', function () {
                expect(subject[0].blob.length).to.eq($$.get('message').bytes().length);
            });

            it('identifies the fragment as start and end', function () {
                expect(subject).to.have.property('[0].start_fragment', true);
                expect(subject).to.have.property('[0].end_fragment', true);
            });

            it('identifies the fragment as start and end', function () {
                expect(subject).to.have.property('[0].fragment_id', 0);
            });
        });
    });

    $.context('two fragment', async $$ => {
        $$.let('data', () => 'This is a fragmented message');
        await $$.subject($$).then(subject => {
            it('splits the message', function () {
                expect(subject).to.have.lengthOf(2);
            });

            it('has a sum of blob data equal to the message bytes', function () {
                expect(subject[0].blob.length + subject[1].blob.length).to.eq($$.get('message').bytes().length);
            });

            it('identifies the first fragment as start and not end', function () {
                expect(subject).to.have.property('[0].start_fragment', true);
                expect(subject).to.have.property('[0].end_fragment', false);
            });

            it('identifies the second fragment as end and not start', function () {
                expect(subject).to.have.property('[1].start_fragment', false);
                expect(subject).to.have.property('[1].end_fragment', true);
            });

            it('assigns incrementing fragment ids', function () {
                expect(subject).to.have.property('[0].fragment_id', 0);
                expect(subject).to.have.property('[1].fragment_id', 1);
            });
        });
    });

    $.context('three fragment', async $$ => {
        $$.let('data', () => 'This is a fragmented message because framents are lovely');
        await $$.subject($$).then(subject => {
            it('splits the message', function () {
                expect(subject).to.have.lengthOf(3);
            });

            it('has a sum of blob data equal to the message bytes', function () {
                expect(subject[0].blob.length + subject[1].blob.length + subject[2].blob.length)
                    .to.eq($$.get('message').bytes().length);
            });

            it('identifies the first fragment as start and not end', function () {
                expect(subject).to.have.property('[0].start_fragment', true);
                expect(subject).to.have.property('[0].end_fragment', false);
            });

            it('identifies the second fragment as not start and not end', function () {
                expect(subject).to.have.property('[1].start_fragment', false);
                expect(subject).to.have.property('[1].end_fragment', false);
            });

            it('identifies the third fragment as end and not start', function () {
                expect(subject).to.have.property('[2].start_fragment', false);
                expect(subject).to.have.property('[2].end_fragment', true);
            });

            it('assigns incrementing fragment ids', function () {
                expect(subject).to.have.property('[0].fragment_id', 0);
                expect(subject).to.have.property('[1].fragment_id', 1);
                expect(subject).to.have.property('[2].fragment_id', 2);
            });
        });
    });
});