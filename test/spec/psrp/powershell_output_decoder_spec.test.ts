import { describe } from "mocha";
import { BindingFn, MochaLatte, stubbed_clixml } from "../spec_helper.ts";
import * as WinRM from '../../../lib/winrm/internal.ts';
import { expect } from "chai";
import type { PowershellOutputDecoder } from "../../../lib/winrm/psrp/powershell_output_decoder.ts";
import type { MessageType } from "lib/winrm/types.ts";
import type { Message } from "lib/winrm/psrp/message.ts";

describe('WinRM.PSRP.PowershellOutputDecoder', function () {
    const $ = new MochaLatte<PowershellOutputDecoder, {
        message_type: MessageType,
        data: Promise<string> | string,
        message: Promise<Message>,
        test_data_error_xml_template: BindingFn,
        error_message: string,
        script_root: string,
        category_message: string,
        stack_trace: string,
        error_id: string,
        pipeline_state: string
    }, Promise<string | undefined>>(this);
    $.let('message_type', () => WinRM.PSRP.Message.MESSAGE_TYPES.error_record);
    $.let('data', () => 'blah');
    $.let('message', async $$ => new WinRM.PSRP.Message(
        'bc1bfbba-8215-4a04-b2df-7a3ac0310e16',
        $$.get('message_type'),
        await $$.get('data'),
        '4218a578-0f18-4b19-82c3-46b433319126'
    ));

    $.subject(async $$ => {
        const message = await $$.get('message');
        // console.log(message);
        return new $.described_class().decode(message);
    });

    $.context('undecodable message type', $$ => {
        $$.let('message_type', () => WinRM.PSRP.Message.MESSAGE_TYPES.public_key);

        it('ignores message', async function () {
            $$.get('message_type');
            expect(await $$.subject($$)).to.be.undefined;
        });
    });

    $.context('undecodable method identifier', $$ => {
        $$.let('message_type', () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_host_call);
        $$.let('data', () => "\xEF\xBB\xBF<Obj RefId=\"0\"><MS><I64 N=\"ci\">-100</I64><Obj N=\"mi\" RefId=\"1\">" +
            '<TN RefId="0"><T>System.Management.Automation.Remoting.RemoteHostMethodId</T>' +
            '<T>System.Enum</T><T>System.ValueType</T><T>System.Object</T></TN>' +
            '<ToString>WriteProgress</ToString><I32>17</I32></Obj><Obj N="mp" RefId="2">' +
            '<TN RefId="1"><T>System.Collections.ArrayList</T><T>System.Object</T></TN><LST>' +
            '<I32>7</I32><I32>0</I32><S>some_x000D__x000A_data</S></LST></Obj></MS></Obj>');

        it('ignores message', async function () {
            expect(await $$.subject($$)).to.be.undefined;
        });
    });

    $.context('receiving pipeline output', $$ => {
        $$.let('message_type', () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_output);
        $$.let('data', () => '<obj><S>some data</S></obj>');

        it('decodes output', async function () {
            expect(await $$.subject($$)).to.eq("some data\r\n");
        });
    });

    $.context('writeline with new line in middle', $$ => {
        $$.let('message_type', () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_host_call);
        $$.let('data', () => "\xEF\xBB\xBF<Obj RefId=\"0\"><MS><I64 N=\"ci\">-100</I64><Obj N=\"mi\" RefId=\"1\">" +
            '<TN RefId="0"><T>System.Management.Automation.Remoting.RemoteHostMethodId</T>' +
            '<T>System.Enum</T><T>System.ValueType</T><T>System.Object</T></TN>' +
            '<ToString>WriteLine3</ToString><I32>17</I32></Obj><Obj N="mp" RefId="2">' +
            '<TN RefId="1"><T>System.Collections.ArrayList</T><T>System.Object</T></TN><LST>' +
            '<I32>7</I32><I32>0</I32><S>some_x000D__x000A_data</S></LST></Obj></MS></Obj>'
        );

        it('decodes and replaces newline', async function () {
            expect(await $$.subject($$)).to.eq("some\r\ndata\r\n");
        });
    });

    $.context('receiving error record', $$ => {
        $$.let('message_type', () => WinRM.PSRP.Message.MESSAGE_TYPES.error_record);
        $$.let('test_data_error_xml_template', () => stubbed_clixml('error_record.xml'), false);
        $$.let('error_message', () => 'an error');
        $$.let('script_root', () => 'script_root');
        $$.let('category_message', () => 'category message');
        $$.let('stack_trace', () => 'stack trace');
        $$.let('error_id', () => 'Microsoft.PowerShell.Commands.WriteErrorException');
        $$.let('data', async $$$ => (await $$$.get('test_data_error_xml_template'))($$$.binding), false);

        it('decodes error record', async function () {
            expect(await $$.subject($$)).to.match(new RegExp($$.get('error_message')));
        });
    });

    $.context('receiving error record in pipeline state', $$ => {
        $$.let('message_type', () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_state);
        $$.let('test_data_error_xml_template', () => stubbed_clixml('pipeline_state.xml'), false);
        $$.let('pipeline_state', () => WinRM.PSRP.MessageData.PipelineState.FAILED);
        $$.let('error_message', () => 'an error');
        $$.let('category_message', () => 'category message');
        $$.let('error_id', () => 'an error');
        $$.let('data', async () => (await $$.get('test_data_error_xml_template'))($$.binding), false);

        it('decodes error record', async function () {
            expect(await $$.subject($$)).to.match(new RegExp($$.get('error_message')));
        });
    });
});