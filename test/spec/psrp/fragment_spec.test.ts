import { MochaLatte } from "../spec_helper.ts";
import { expect } from "chai";
import { Fragment } from "lib/winrm/psrp";
describe('WinRM.PSRP.Fragment', function () {
    const $ = new MochaLatte<Fragment, {
        id: number,
        message: string,
        fragment_id: number
    }>(this);
    $.let('id', () => 1);
    $.let('message', () => 'blah blah blah');
    $.context('called with just id and blob', $$ => {
        $$.subject(() => new $.described_class($.get('id'), Buffer.from($.get('message'))));

        it('sets the message id to 1', () => {
            expect($$.subject().bytes().slice(0, 8)).to.eql([0, 0, 0, 0, 0, 0, 0, $.get('id')]);
        });
        it('sets the fragment id to 0', () => {
            expect($$.subject().bytes().slice(8, 16)).to.eql([0, 0, 0, 0, 0, 0, 0, 0]);
        });
        it('sets the last 2 bits of the end/start fragment', () => {
            expect($$.subject().bytes()[16]).to.eq(3);
        });
        it('sets message blob length to 3640', () => {
            expect($$.subject().bytes().slice(17, 21)).to.eql([0, 0, 0, Buffer.from($.get('message')).byteLength]);
        });
        it('sets message blob', () => {
            expect($$.subject().bytes().slice(21)).to.eql(Buffer.from($.get('message')).toJSON().data);
        });
    });

    $.context('specifying a fragment id', $$ => {
        $$.let('fragment_id', () => 1);

        $$.subject(() => new $.described_class($.get('id'), Buffer.from($.get('message')), $$.get('fragment_id')))

        it('sets the fragment id', () => {
            expect($$.subject().bytes().slice(8, 16)).to.eql([0, 0, 0, 0, 0, 0, 0, $$.get('fragment_id')]);
        });
    });

    $.context('middle fragment', $$ => {
        $$.subject(() => new $.described_class($.get('id'), Buffer.from($.get('message')), 1, false, false));

        it('sets the last 2 bits of the end/start fragment to 0', () => {
            expect($$.subject().bytes()[16]).to.eq(0);
        });
    });

    $.context('end fragment', $$ => {
        $$.subject(() => new $.described_class($.get('id'), Buffer.from($.get('message')), 1, true, false));

        it('sets the end fragment bit', () => {
            expect($$.subject().bytes()[16]).to.eq(1);
        });
    });

    $.context('end fragment', $$ => {
        $$.subject(() => new $.described_class($.get('id'), Buffer.from($.get('message')), 1, false, true));

        it('sets the end fragment bit', () => {
            expect($$.subject().bytes()[16]).to.eq(2);
        });
    });
});