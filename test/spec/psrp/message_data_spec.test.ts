import { describe } from "mocha";
import { MochaLatte } from "../spec_helper.ts";
import * as WinRM from '../../../lib/winrm/internal.ts';
import { expect } from "chai";
import { MessageType } from "lib/winrm/types.ts";

describe('WinRM.PSRP.MessageData', function () {
    const $ = new MochaLatte<WinRM.PSRP.MessageData.Base, {
        raw_data: string,
        message_type: MessageType
    }>(this);
    describe('.parse', function () {
        $.let('raw_data', () => 'raw_data');
        // $.let('message', function _message ($$) {
        //     return new WinRM.PSRP.Message('00000000-0000-0000-0000-000000000000', $$.get('message_type'), $.get('raw_data'))
        // });
        $.subject(function _subject($$) {
            const message = new WinRM.PSRP.Message('00000000-0000-0000-0000-000000000000', $$.get('message_type'), $.get('raw_data'));
            return WinRM.PSRP.MessageData.parse(message)!;
        });
        $.context('defined message type', $$ => {
            $$.let('message_type', () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_output);
            it('creates correct message data type', function () {
                // console.log(typeof $$.subject(), $$.subject() instanceof WinRM.PSRP.MessageData.PipelineOutput);
                expect($$.subject($$)).to.be.an.instanceOf(WinRM.PSRP.MessageData.PipelineOutput)
            });
        });
        $.context('undefined message type', $$ => {
            $$.let('message_type', () => WinRM.PSRP.Message.MESSAGE_TYPES.pipeline_input);

            it('returns undefined', function () {
                // console.log(typeof $$.subject());
                expect($$.subject()).to.be.undefined;
            });
        });
    });
});