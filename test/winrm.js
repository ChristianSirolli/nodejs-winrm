// @ts-check
import { WinRM } from '../dist/index.js';
let conn = new WinRM.Connection({
        endpoint: 'http://townley-it.local.townley.net:5985/wsman',
        user: process.env['WINRM_USER'] ?? '',
        domain: process.env['WINRM_DOMAIN'],
        password: process.env['WINRM_PASS'] ?? '',
        transport: 'plaintext',
        realm: process.env['WINRM_DOMAIN'],
        receive_timeout: 10000
    }),
    ps = conn.shell('powershell'),
    output = await ps.run('$PSVersionTable', [], ([stdout, stderr]) => console.log(stdout, stderr));
    // cmd = conn.shell('cmd'),
    // output = await cmd.run('ipconfig', [], ([stdout, stderr]) => console.log(stdout, stderr));
    // output = await conn.run_wql('SELECT * FROM Win32_LogicalDisk');
console.log(output);