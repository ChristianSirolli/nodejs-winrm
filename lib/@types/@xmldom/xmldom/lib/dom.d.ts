import { Node as DOMNode, NodeArray, NodeFlags, SourceFile, SourceFileLike, SyntaxKind } from 'typescript';
declare module "@xmldom/xmldom/lib/dom.ts" {
    export const DocumentType: DocumentType;
	export const DOMException: DOMException;
	export const DOMImplementation: DOMImplementation;
	export const Element: Element;
	export class Node implements DOMNode {
		readonly pos: number;
        readonly end: number;
		readonly kind: SyntaxKind;
        readonly flags: NodeFlags;
        readonly parent: DOMNode;
		getSourceFile(): SourceFile;
        getChildCount(sourceFile?: SourceFile): number;
        getChildAt(index: number, sourceFile?: SourceFile): DOMNode;
        getChildren(sourceFile?: SourceFile): DOMNode[];
        getStart(sourceFile?: SourceFile, includeJsDocComment?: boolean): number;
        getFullStart(): number;
        getEnd(): number;
        getWidth(sourceFile?: SourceFileLike): number;
        getFullWidth(): number;
        getLeadingTriviaWidth(sourceFile?: SourceFile): number;
        getFullText(sourceFile?: SourceFile): string;
        getText(sourceFile?: SourceFile): string;
        getFirstToken(sourceFile?: SourceFile): DOMNode | undefined;
        getLastToken(sourceFile?: SourceFile): DOMNode | undefined;
        forEachChild<T>(cbNode: (node: DOMNode) => T | undefined, cbNodeArray?: (nodes: NodeArray<DOMNode>) => T | undefined): T | undefined;
	}
	export const NodeList: NodeList;
	export const XMLSerializer: XMLSerializer;
}