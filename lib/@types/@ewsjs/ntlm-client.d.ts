declare module "@ewsjs/ntlm-client" {
    import type { IncomingMessage } from "http";
    interface Type2Message {
        flags: number
        encoding: 'ascii' | 'ucs2'
        version: 2 | 1
        challenge: Buffer
        targetName: string
        targetInfo: {
            parsed?: Record<string, string>
            buffer?: Buffer
        }
    }
    export function createType1Message(workstation?: string, target?: string): `NTLM ${string}`
    export function decodeType2Message(str?: string | IncomingMessage): Type2Message
    export function createType3Message(type2Message: Type2Message, username: string, password: string, workstation?: string, target?: string): `NTLM ${string}`
}