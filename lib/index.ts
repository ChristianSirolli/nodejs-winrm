export * as WinRM from './winrm/internal.ts';

import process from "node:process";
import { loggers, transports, format } from 'winston';

if (process.env.WINRM_LOG && process.env.WINRM_LOG != '') {
    try {
        loggers.add('WinRM', {
            transports: [
                new transports.File({ filename: 'winrm-debug.log', level: process.env.WINRM_LOG, format: format.prettyPrint({'colorize': false}) })
            ]
        })
    } catch (e) {
        process.stderr.write(`Invalid WINRM_LOG level is set: ${process.env.WINRM_LOG}`);
        process.stderr.write('');
        process.stderr.write('Please use one of the standard log levels: debug, info, warn, or error');
        process.stderr.write('');
        process.stderr.write('Original error:');
        process.stderr.write('');
        process.stderr.write((e as Error).name);
        process.stderr.write((e as Error).message);
    }
}
