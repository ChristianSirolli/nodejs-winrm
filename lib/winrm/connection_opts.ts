import { randomUUID } from 'node:crypto';
import type { InitialConnectionOptions, InitialCertConnectionOptions, WinRMOptions } from './types.ts';
import { assert } from './helpers.ts';
import type { Buffer } from "node:buffer";

type OptionKey = keyof WinRMOptions;
type OptionValue = URL | Buffer | string | number | boolean | undefined;

export class ConnectionOpts extends Map<OptionKey, WinRMOptions[OptionKey]> {
    static #DEFAULT_OPERATION_TIMEOUT = 60;
    static #DEFAULT_RECEIVE_TIMEOUT = this.#DEFAULT_OPERATION_TIMEOUT + 10;
    static #DEFAULT_MAX_ENV_SIZE = 153600;
    static #DEFAULT_LOCALE = 'en-US';
    static #DEFAULT_RETRY_DELAY = 10;
    static #DEFAULT_RETRY_LIMIT = 3;
    static #DEFAULT_USER_AGENT = 'Node.ts WinRM Client';

    static create_with_defaults: (opts: (InitialConnectionOptions | InitialCertConnectionOptions) & Partial<WinRMOptions>) => ConnectionOpts
    static #ensure_receive_timeout_is_greater_than_operation_timeout: (opts: ConnectionOpts) => ConnectionOpts
    static #default: () => ConnectionOpts;

    static {
        this.create_with_defaults = function (opts) {
            let config: ConnectionOpts = this.#default();
            Object.entries(opts).forEach(v => config.set(v.at(0) as OptionKey, v.at(1) as OptionValue));
            config = this.#ensure_receive_timeout_is_greater_than_operation_timeout(config);
            config.validate();
            return config;
        }

        this.#ensure_receive_timeout_is_greater_than_operation_timeout = function (opts) {
            if (opts.get('receive_timeout')! < opts.get('operation_timeout')!) {
                opts.set('receive_timeout', opts.get('operation_timeout')! + 10);
            }
            return opts;
        }

        this.#default = function () {
            const config = new ConnectionOpts([
                ['session_id', randomUUID().toUpperCase()],
                ['transport', 'negotiate'],
                ['locale', this.#DEFAULT_LOCALE],
                ['max_envelope_size', this.#DEFAULT_MAX_ENV_SIZE],
                ['operation_timeout', this.#DEFAULT_OPERATION_TIMEOUT],
                ['receive_timeout', this.#DEFAULT_RECEIVE_TIMEOUT],
                ['retry_delay', this.#DEFAULT_RETRY_DELAY],
                ['retry_limit', this.#DEFAULT_RETRY_LIMIT],
                ['user_agent', this.#DEFAULT_USER_AGENT],
            ]);
            return config;
        }
    }

    constructor(args: [OptionKey, WinRMOptions[OptionKey]][] | WinRMOptions) {
        super()
        if (!(args instanceof Array)) {
            args = Object.entries(args) as [OptionKey, WinRMOptions[OptionKey]][];
        }
        args?.forEach(v => this.set(v.at(0) as OptionKey, v.at(1)));
    }

    validate() {
        this.#validate_required_fields();
        this.#validate_data_types();
    }

    #validate_required_fields() {
        assert(this.get('endpoint'), 'endpoint is a required option', 'NameError');
        if (this.get('client_cert')) {
            assert(this.get('client_key'), 'path to client key is required', 'NameError');
        } else {
            assert(this.get('user'), 'user is a required option', 'NameError');
            assert(this.get('password'), 'password is a required option', 'NameError');
        }
    }

    #validate_data_types() {
        this.#validate_int('retry_limit');
        this.#validate_int('retry_delay');
        this.#validate_int('max_envelope_size');
        this.#validate_int('operation_timeout');
        this.#validate_int('receive_timeout', this.get('operation_timeout'));
    }

    #validate_int(key: 'retry_limit' | 'retry_delay' | 'max_envelope_size' | 'operation_timeout' | 'receive_timeout', min = 0) {
        const value = this.get(key)!;
        assert(value && Number.isInteger(value), `${key} must be an Integer`, 'ArgumentError');
        assert(value > min, `${key} must be greater than ${min}`, 'ArgumentError');
    }

    override set<K extends OptionKey>(key: K, value: WinRMOptions[K]) {
        return super.set(key, value);
    }

    override get<K extends OptionKey>(key: K) {
        return super.get(key) as WinRMOptions[K];
    }
}