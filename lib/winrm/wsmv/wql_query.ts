import type { ConnectionOpts, Transport } from "../types.ts";
import { WSMV } from "../internal.ts";
import { /*convertToSnakecase,*/ snakecase, xmlEleParser } from "../helpers.ts";
import type { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';
import { /*builder, */ convert } from "xmlbuilder2";
import { XMLSerializer, type Document as SlimDocument } from "slimdom";
// import { Document as DOMDocument } from "@oozcitak/dom/lib/dom/interfaces";

export class WqlQuery extends WSMV.Base {
    session_opts: ConnectionOpts
    wql: string
    items!: Record<string, never[]>
    transport: Transport
    namespace: null | string
    constructor(transport: Transport, session_opts: ConnectionOpts, wql: string, namespace: string | null = null) {
        super();
        this.session_opts = session_opts;
        this.wql = wql;
        this.namespace = namespace;
        this.transport = transport;
    }
    async process_response(response: Promise<SlimDocument | null>) {
        const parser = xmlEleParser({
            convert_tags_to: snakecase,
            strip_namespaces: true
        });
        this.items = {};

        // eslint-disable-next-line @typescript-eslint/no-unsafe-argument
        const doc = new XMLSerializer().serializeToString((await response)!);
        // console.log('WqlQuery.process_response.doc:')
        // console.log(doc);
        const convertedDoc = (/*convertToSnakecase(*/convert({ parser: { element: parser } }, doc, { format: 'object', verbose: true, group: false }/*)*/) as { envelope: { body: { enumerate_response: { items: Record<string, unknown>[], enumeration_context: string[] }[] }[] }[] });
        // console.log('WqlQuery.process_response.convertedDoc:')
        // console.dir(convertedDoc, { depth: 10 });
        let hresp = convertedDoc.envelope[0].body[0].enumerate_response[0];
        this.#process_items(hresp.items);
        // console.log('WqlQuery.process_response.hresp:')
        // console.dir(hresp, { depth: 10 });
        let enumeration_context: string | undefined = hresp?.enumeration_context[0];
        while (enumeration_context !== undefined) {
            const query = new WSMV.WqlPull(this.session_opts, this.namespace, enumeration_context);
            hresp = ((query.process_response(await this.transport.send_request(query.build()))) as { pull_response: { items: Record<string, unknown>[], enumeration_context: string[] }[] }).pull_response[0];
            this.#process_items(hresp.items);
            enumeration_context = hresp?.enumeration_context?.at(0);
        }
        return this.items;
    }
    override create_header(header: XMLBuilder) {
        return header.ele(this.#wql_header())
    }
    override create_body(body: XMLBuilder) {
        return body.ele(`${this.NS_ENUM}:Enumerate`).ele(this.#wql_body());
    }
    #process_items(items: Record<string, unknown>[]) {
        if (items == null) return;
        for (const [k, v] of Object.entries(items)) {
            const v_ary = (v instanceof Array ? v : [v]) as never[];
            if (!this.items[k]) this.items[k] = [];
            this.items[k] = [...this.items[k], ...v_ary];
        }
    }
    #wql_header() {
        return this.merge_headers(this.shared_headers(this.session_opts),
            this.resource_uri_wmi(this.namespace ?? ''),
            this.action_enumerate());
    }
    #wql_body() {
        return {
            [`${this.NS_WSMAN_DMTF}:OptimizeEnumeration`]: null,
            [`${this.NS_WSMAN_DMTF}:MaxElements`]: '32000',
            [`${this.NS_WSMAN_DMTF}:Filter`]: {
                '#': this.wql,
                '@Dialect': 'http://schemas.microsoft.com/wbem/wsman/1/WQL'
            },
            [`${this.NS_WSMAN_MSFT}:SessionId`]: `uuid:${this.session_opts.get('session_id') as string}`,
        }
    }
}