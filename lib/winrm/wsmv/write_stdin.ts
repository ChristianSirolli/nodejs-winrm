import { Buffer } from "node:buffer";
import { WSMV } from "../internal.ts";
import type { ConnectionOpts } from "../types.ts";
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';

interface WriteStdinOPtions {
    shell_id: string
    command_id: string
    stdin: string
    shell_uri?: string
}

export class WriteStdin extends WSMV.Base {
    session_opts!: ConnectionOpts;
    shell_id!: string;
    command_id!: string;
    stdin!: string;
    shell_uri!: string;
    constructor(session_opts: ConnectionOpts, opts: WriteStdinOPtions) {
        super();
        this.#validate_opts(session_opts, opts);
        this.#init_ops(session_opts, opts);
    }
    override create_header(header: XMLBuilder) {
        return header.ele(this.#header())
    }
    override create_body(body: XMLBuilder) {
        return body.ele(this.#body());
    }
    #init_ops(session_opts: ConnectionOpts, opts: WriteStdinOPtions) {
        this.session_opts = session_opts;
        this.shell_id = opts.shell_id;
        this.command_id = opts.command_id;
        this.stdin = opts.stdin;
        this.shell_uri = opts.shell_uri ?? this.RESOURCE_URI_CMD;
    }
    #validate_opts(session_opts: ConnectionOpts, opts: WriteStdinOPtions) {
        if (!session_opts) throw new Error('session_opts is required');
        if (!opts.shell_id) throw new Error('opts.shell_id is required');
        if (!opts.command_id) throw new Error('opts.command_id is required');
        if (!opts.stdin) throw new Error('opts.stdin is required');
    }
    #header() {
        return this.merge_headers(
            this.shared_headers(this.session_opts),
            this.resource_uri_shell(this.shell_uri),
            this.action_send(),
            this.selector_shell_id(this.shell_id)
        )
    }
    #body() {
        return {
            [`${this.NS_WIN_SHELL}:Send`]: {
                [`${this.NS_WIN_SHELL}:Stream`]: {
                    '#': Buffer.from(this.stdin).toString('base64'),
                    '@Name': 'stdin',
                    '@CommandId': this.command_id
                }
            }
        }
    }
}