import type { Transport, Stream } from '../types.ts';
// import { Root, getObjectbyKey, getObjectbyKeyValue } from '../helpers.ts';
import { WSMV, Output, WinRMWSManFault, PSRP } from '../internal.ts';
import { Logger } from 'winston';
import { select, select1 } from 'xpath';
import type { Document as SlimDocument } from 'slimdom';
// import { expect } from 'chai';

export class ReceiveResponseReader extends WSMV.Header {
    transport: Transport
    #logger: Logger
    output_decoder: WSMV.CommandOutputDecoder | PSRP.PowershellOutputDecoder
    constructor(transport: Transport, logger: Logger) {
        super()
        this.transport = transport;
        this.#logger = logger;
        this.output_decoder = new WSMV.CommandOutputDecoder();
    }
    get logger() {
        // console.log(`this.logger is a ${this.#logger.constructor.name}`, this.#logger instanceof Logger, expect(this.#logger).to.have.property('debug'))
        return this.#logger;
    }
    read_output(wsmv_message: WSMV.Base, block?: (handled_out: string[]) => void | Promise<void>) {
        return this.with_output(output => {
            return this.read_response(wsmv_message, true, ([stream, resp_doc]) => {
                const handled_out = this.#handle_stream(stream, output, resp_doc);
                if (handled_out && block) return block(handled_out);
            });
        });
    }
    async read_response(wsmv_message: WSMV.Base, wait_for_done_state: boolean, block: (stream: [ Stream, SlimDocument ]) => void | Promise<void>) {
        let resp_doc: SlimDocument | null | undefined = undefined;
        while (!this.#command_done(resp_doc, wait_for_done_state)) {
            this.logger.debug('[WinRM] Waiting for output...');
            resp_doc = await this.#send_get_output_message(wsmv_message.build());
            this.logger.debug('[WinRM] Processing output');
            if (resp_doc !== null && resp_doc) {
                return this.#read_streams(resp_doc, stream => {
                    // console.log(stream)
                    return block([ stream, resp_doc! ])
                });
            }
        }
    }
    async with_output(block: (output: Output) => Promise<unknown>) {
        const output = new Output();
        await block(output);
        output.exitcode ??= 0;
        return output;
    }
    #handle_stream(stream: { type: 'stdout' | 'stderr', text: string }, output: Output, resp_doc: SlimDocument) {
        const decoded_text = (this.output_decoder as WSMV.CommandOutputDecoder).decode(stream.text)
        if (!decoded_text) return;
        const out = { [stream.type]: decoded_text } as { [x in "stdout" | "stderr"]: string };
        output.push(out);
        const code = select1("//*[local-name() = 'ExitCode']", resp_doc as unknown as Document) as Element;
        if (code) {
            output.exitcode = Number(code.textContent);
        }
        return [out.stdout, out.stderr];
    }
    #send_get_output_message(message: string): Promise<SlimDocument | null> {
        try {
            return this.transport.send_request(message);
        } catch (e) {
            if (e instanceof WinRMWSManFault) {
                // If no output is available before the wsman:OperationTimeout expires,
                // the server MUST return a WSManFault with the Code attribute equal to
                // 2150858793. When the client receives this fault, it SHOULD issue
                // another Receive request.
                // http://msdn.microsoft.com/en-us/library/cc251676.aspx
                if (e.fault_code != '2150858793') {
                    throw e;
                }
                this.logger.debug('[WinRM] retrying receive request after timeout')
                return this.#send_get_output_message(message);
            } else {
                throw e;
            }
        }
    }
    #command_done(resp_doc: SlimDocument | null | undefined, wait_for_done_state: boolean) {
        if (!resp_doc) return false;
        if (!wait_for_done_state) return true;
        return Boolean((select("//*[@State='http://schemas.microsoft.com/wbem/wsman/1/windows/shell/CommandState/Done']", resp_doc as unknown as Document) as Element[]).length);
    }
    async #read_streams(resp_doc: SlimDocument, block: (stream: Stream) => void | Promise<void>) {
        const body_path = "/*[local-name() = 'Envelope']/*[local-name() = 'Body']",
        path = `${body_path}/*[local-name() = 'ReceiveResponse']/*[local-name() = 'Stream']`;
        for (const stream of select(path, resp_doc as unknown as Document) as Element[]) {
            if (stream.textContent === null || stream.textContent === '') continue;
            // console.log(stream.textContent);
            await block({
                type: stream.attributes.getNamedItem('Name')?.value as 'stdout' | 'stderr',
                text: stream.textContent
            });
        }
    }
}