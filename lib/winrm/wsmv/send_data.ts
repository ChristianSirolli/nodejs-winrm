import { WSMV, PSRP } from '../internal.ts'
import type { ConnectionOpts } from '../types.ts'
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';

export class SendData extends WSMV.Base {
    session_opts: ConnectionOpts
    #shell_id!: string
    #command_id!: string
    #fragment!: PSRP.Fragment
    constructor(session_opts: ConnectionOpts, shell_id: string, command_id: string, fragment: PSRP.Fragment) {
        super()
        this.session_opts = session_opts;
        this.shell_id = shell_id;
        this.command_id = command_id;
        this.fragment = fragment;
    }
    get shell_id() {
        return this.#shell_id;
    }
    set shell_id(value: string) {
        this.#shell_id = value;
    }
    get command_id() {
        return this.#command_id;
    }
    set command_id(value: string) {
        this.#command_id = value;
    }
    get fragment() {
        return this.#fragment;
    }
    set fragment(value: PSRP.Fragment) {
        this.#fragment = value;
    }
    override create_header(header: XMLBuilder) {
        return header.ele(this.#command_header())
    }
    override create_body(body: XMLBuilder) {
        return body.ele({
            [`${this.NS_WIN_SHELL}:Send`]: {
                '@CommandId': this.shell_id,
                ...this.#command_body()
            }
        })
    }
    #command_body() {
        return {
            [`${this.NS_WIN_SHELL}:Stream`]: {
                '#': this.encode_bytes(this.fragment.bytes()),
                '@Name': 'stdin',
                '@CommandId': this.command_id
            }
        }
    }
    #command_header() {
        return this.merge_headers(this.shared_headers(this.session_opts),
            this.resource_uri_shell(this.RESOURCE_URI_POWERSHELL),
            this.action_send(),
            this.selector_shell_id(this.shell_id))
    }
}