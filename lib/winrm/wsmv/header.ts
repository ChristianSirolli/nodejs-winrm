import type { ConnectionOpts } from "../types.ts";
import { randomUUID } from "node:crypto";
import { WSMV } from "../internal.ts";
export class Header extends WSMV.SOAP {
    static RESOURCE_URI_CMD = 'http://schemas.microsoft.com/wbem/wsman/1/windows/shell/cmd'
    RESOURCE_URI_CMD = Header.RESOURCE_URI_CMD
    static RESOURCE_URI_POWERSHELL = 'http://schemas.microsoft.com/powershell/Microsoft.PowerShell'
    RESOURCE_URI_POWERSHELL = Header.RESOURCE_URI_POWERSHELL

    merge_headers(...headers: object[]): object {
        const hdr = {};
        headers.forEach(h => Object.assign(hdr, structuredClone(h)));
        return hdr;
    }
    shared_headers(session_opts: ConnectionOpts) {
        return {
            [`${this.NS_ADDRESSING}:To`]: session_opts.get('endpoint'),
            [`${this.NS_ADDRESSING}:ReplyTo`]: {
                [`${this.NS_ADDRESSING}:Address`]: {
                    '#': 'http://schemas.xmlsoap.org/ws/2004/08/addressing/role/anonymous',
                    '@mustUnderstand': true
                }
            },
            [`${this.NS_WSMAN_DMTF}:MaxEnvelopeSize`]: {
                '#': session_opts.get('max_envelope_size'),
                '@mustUnderstand': true
            },
            [`${this.NS_ADDRESSING}:MessageID`]: `uuid:${randomUUID()}`,
            [`${this.NS_WSMAN_MSFT}:SessionId`]: {
                '#': `uuid:${session_opts.get('session_id') as string}`,
                '@mustUnderstand': false
            },
            [`${this.NS_WSMAN_DMTF}:Locale`]: {
                '@mustUnderstand': false,
                '@lang@@xml': session_opts.get('locale')
            },
            [`${this.NS_WSMAN_MSFT}:DataLocale`]: {
                // '#': '',
                '@mustUnderstand': false,
                '@lang@@xml': session_opts.get('locale')
            },
            [`${this.NS_WSMAN_DMTF}:OperationTimeout`]: {
                '#': WSMV.Iso8601Duration.sec_to_dur(Number(session_opts.get('operation_timeout'))),
            }
        };
    }

    resource_uri_shell(shell_uri: string) {
        return {
            [`${this.NS_WSMAN_DMTF}:ResourceURI`]: {
                '#': shell_uri,
                '@mustUnderstand': true
            }
        };
    }

    resource_uri_cmd() {
        return this.resource_uri_shell(this.RESOURCE_URI_CMD);
    }
    resource_uri_wmi(namespace = 'root/cimv2/*') {
        return {
            [`${this.NS_WSMAN_DMTF}:ResourceURI`]: {
                '#': `http://schemas.microsoft.com/wbem/wsman/1/wmi/${namespace}`,
                '@mustUnderstand': true
            }
        };
    }
    action_get() {
        return {
            [`${this.NS_ADDRESSING}:Action`]: {
                '#': `http://schemas.xmlsoap.org/ws/2004/09/transfer/Get`,
                '@mustUnderstand': true
            }
        };
    }
    action_delete() {
        return {
            [`${this.NS_ADDRESSING}:Action`]: {
                '#': `http://schemas.xmlsoap.org/ws/2004/09/transfer/Delete`,
                '@mustUnderstand': true
            }
        };
    }
    action_command() {
        return {
            [`${this.NS_ADDRESSING}:Action`]: {
                '#': `http://schemas.microsoft.com/wbem/wsman/1/windows/shell/Command`,
                '@mustUnderstand': true
            }
        };
    }
    action_receive() {
        return {
            [`${this.NS_ADDRESSING}:Action`]: {
                '#': `http://schemas.microsoft.com/wbem/wsman/1/windows/shell/Receive`,
                '@mustUnderstand': true
            }
        };
    }
    action_send() {
        return {
            [`${this.NS_ADDRESSING}:Action`]: {
                '#': `http://schemas.microsoft.com/wbem/wsman/1/windows/shell/Send`,
                '@mustUnderstand': true
            }
        };
    }
    action_signal() {
        return {
            [`${this.NS_ADDRESSING}:Action`]: {
                '#': `http://schemas.microsoft.com/wbem/wsman/1/windows/shell/Signal`,
                '@mustUnderstand': true
            }
        };
    }
    action_enumerate() {
        return {
            [`${this.NS_ADDRESSING}:Action`]: {
                '#': `http://schemas.xmlsoap.org/ws/2004/09/enumeration/Enumerate`,
                '@mustUnderstand': true
            }
        };
    }
    action_enumerate_pull() {
        return {
            [`${this.NS_ADDRESSING}:Action`]: {
                '#': `http://schemas.xmlsoap.org/ws/2004/09/enumeration/Pull`,
                '@mustUnderstand': true
            }
        };
    }
    action_create() {
        return {
            [`${this.NS_ADDRESSING}:Action`]: {
                '#': `http://schemas.xmlsoap.org/ws/2004/09/transfer/Create`,
                '@mustUnderstand': true
            }
        };
    }
    selector_shell_id(shell_id: string) {
        return {
            [`${this.NS_WSMAN_DMTF}:SelectorSet`]: {
                [`${this.NS_WSMAN_DMTF}:Selector`]: {
                    '#': shell_id,
                    '@Name': 'ShellId'
                }
            }
        };
    }
}