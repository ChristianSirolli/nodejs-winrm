import type { ConnectionOpts } from '../types.ts';
import { WSMV } from '../internal.ts';
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';
export class CleanupCommand extends WSMV.Base {
    session_opts: ConnectionOpts
    shell_id: string
    command_id: string
    shell_uri: string
    constructor(session_opts: ConnectionOpts, opts: { shell_uri?: string, shell_id: string, command_id: string }) {
        super()
        if (!opts.shell_id) throw new Error('opts.shell_id is required');
        if (!opts.command_id) throw new Error('opts.command_id is required');
        this.session_opts = session_opts;
        this.shell_id = opts.shell_id;
        this.command_id = opts.command_id;
        this.shell_uri = opts.shell_uri ?? this.RESOURCE_URI_CMD;
    }
    override create_header(header: XMLBuilder) {
        return header.ele(this.#cleanup_header());
    }
    override create_body(body: XMLBuilder) {
        return body.ele(`${this.NS_WIN_SHELL}:Signal`, { 'CommandId': this.command_id }).ele(this.#cleanup_body());
    }
    #cleanup_header() {
        return this.merge_headers(this.shared_headers(this.session_opts),
            this.resource_uri_shell(this.shell_uri),
            this.action_signal(),
            this.selector_shell_id(this.shell_id))
    }
    #cleanup_body() {
        return {
            [`${this.NS_WIN_SHELL}:Code`]: 'http://schemas.microsoft.com/wbem/wsman/1/windows/shell/signal/terminate'
        }
    }
}