import type { ConnectionOpts } from '../types.ts'
import { WSMV } from '../internal.ts'
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';


export class KeepAlive extends WSMV.Base {
    session_opts: ConnectionOpts
    #shell_id!: string
    constructor(session_opts: ConnectionOpts, shell_id: string) {
        super();
        this.session_opts = session_opts;
        this.shell_id = shell_id;
    }
    get shell_id() {
        return this.#shell_id;
    }
    set shell_id(value: string) {
        this.#shell_id = value;
    }
    override create_header(header: XMLBuilder) {
        return header.ele(this.#header());
    }
    override create_body(body: XMLBuilder) {
        return body.ele({
            [`${this.NS_WIN_SHELL}:Receive`]: this.#body()
        });
    }
    #body() {
        return { [`${this.NS_WIN_SHELL}:DesiredStream`]: 'stdout' };
    }
    #header() {
        return this.merge_headers(this.shared_headers(this.session_opts),
            this.resource_uri_shell(this.RESOURCE_URI_POWERSHELL),
            this.action_receive(),
            this.#header_opts(),
            this.selector_shell_id(this.shell_id));
    }
    #header_opts() {
        return {
            [`${this.NS_WSMAN_DMTF}:OptionSet`]: {
                [`${this.NS_WSMAN_DMTF}:Option`]: {
                    '#': 'TRUE',
                    '@Name': 'WSMAN_CMDSHELL_OPTION_KEEPALIVE'
                }
            }
        };
    }
}