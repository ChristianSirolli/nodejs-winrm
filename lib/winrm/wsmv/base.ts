import { create } from 'xmlbuilder2';
import { NotImplementedError, WSMV } from '../internal.ts';
import { pack } from '../helpers.ts';
import type { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';
import { Buffer } from "node:buffer";

export class Base extends WSMV.Header {
    constructor() {
        super()
    }
    build(): string {
        const builder = create({ encoding: 'UTF-8', namespaceAlias: this.namespaceAliases });
        try {
            let env = builder.ele(`env:Envelope`);
            Object.entries(this.namespaces).forEach(([ns, v]) => {
                env = env.att(ns, v);
            });
            const env_header = env.ele(`env:Header`);
            this.create_header(env_header);
            const env_body = env.ele(`env:Body`);
            this.create_body(env_body);
            return builder.toString({ wellFormed: true, format: 'xml' });
        } catch (e) {
            console.trace((e as Error).message);
            throw e;
            // throw new Error(`Caught error building XML:\n${builder.toString({ wellFormed: false, format: 'xml' })}`, {cause: e})
        }
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    create_header(_header: XMLBuilder): XMLBuilder {
        throw new NotImplementedError('create_header')
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    create_body(_body: XMLBuilder): XMLBuilder | void {
        throw new NotImplementedError('create_header')
    }
    encode_bytes(bytes: (number | bigint | undefined)[]) {
        return Buffer.from(pack(bytes.map(Number), 'C*')).toString('base64');
    }
}