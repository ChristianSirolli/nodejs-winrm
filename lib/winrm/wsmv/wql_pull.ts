import type { ConnectionOpts } from "../types.ts";
import { WSMV } from "../internal.ts";
import { /*builder,*/ convert } from "xmlbuilder2";
import type { XMLBuilder } from "xmlbuilder2/lib/interfaces.ts";
import { XMLSerializer, type Document as SlimDocument } from "slimdom";
import { snakecase, xmlEleParser } from "../helpers.ts";
// import { Document as DOMDocument } from "@oozcitak/dom/lib/dom/interfaces";

export class WqlPull extends WSMV.Base {
    session_opts: ConnectionOpts
    namespace: string | null
    enumeration_context: string
    constructor(session_opts: ConnectionOpts, namespace: string | null, enumeration_context: string) {
        super();
        this.session_opts = session_opts;
        this.namespace = namespace;
        this.enumeration_context = enumeration_context;
    }
    process_response(response: SlimDocument | null): Record<string, unknown> {
        const parser = xmlEleParser({
            convert_tags_to: snakecase,
            strip_namespaces: true
        });
        const convertedDoc = (convert({ parser: { element: parser } }, new XMLSerializer().serializeToString(response!), { format: 'object', verbose: true }) as {envelope: { body: { enumerate_response: Record<string, unknown>[] }[] }[]});
        // console.log('WqlPull.process_response.convertedDoc:')
        // console.dir(convertedDoc, { depth: 10 });
        return convertedDoc.envelope[0].body[0]
    }
    override create_header(header: XMLBuilder) {
        return header.ele(this.#wql_header())
    }
    override create_body(body: XMLBuilder) {
        return body.ele(`${this.NS_ENUM}:Pull`).ele(this.#wql_body());
    }
    #wql_header() {
        return this.merge_headers(this.shared_headers(this.session_opts),
            this.resource_uri_wmi(this.namespace ?? ''),
            this.action_enumerate_pull());
    }
    #wql_body() {
        return {
            [`${this.NS_ENUM}:EnumerationContext`]: this.enumeration_context,
            [`${this.NS_WSMAN_DMTF}:OptimizeEnumeration`]: null,
            [`${this.NS_ENUM}:MaxElements`]: '32000',
            [`${this.NS_WSMAN_MSFT}:SessionId`]: `uuid:${this.session_opts.get('session_id') as string}`,
        }
    }
}