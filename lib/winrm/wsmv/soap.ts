// import type { Constructor } from "../types.ts";
// WSMV SOAP namespaces mixin
// export function SOAP(superclass: Constructor) {
export class SOAP {
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    //    // NS_SOAP_ENV = 'env'   // http://www.w3.org/2003/05/soap-envelope
    NS_ADDRESSING = 'a'   // http://schemas.xmlsoap.org/ws/2004/08/addressing
    NS_CIMBINDING = 'b'   // http://schemas.dmtf.org/wbem/wsman/1/cimbinding.xsd
    NS_ENUM = 'n'   // http://schemas.xmlsoap.org/ws/2004/09/enumeration
    NS_TRANSFER = 'x'   // http://schemas.xmlsoap.org/ws/2004/09/transfer
    NS_WSMAN_DMTF = 'w'   // http://schemas.dmtf.org/wbem/wsman/1/wsman.xsd
    NS_WSMAN_MSFT = 'p'   // http://schemas.microsoft.com/wbem/wsman/1/wsman.xsd
    NS_SCHEMA_INST = 'xsi' // http://www.w3.org/2001/XMLSchema-instance
    NS_WIN_SHELL = 'rsp' // http://schemas.microsoft.com/wbem/wsman/1/windows/shell
    NS_WSMAN_FAULT = 'f'   // http://schemas.microsoft.com/wbem/wsman/1/wsmanfault
    NS_WSMAN_CONF = 'cfg' // http://schemas.microsoft.com/wbem/wsman/1/config
    #namespaces?: {
        [x: `xmlns:${string}`]: string;
        'xmlns:xsd': string;
        'xmlns:xsi': string;
        'xmlns:env': string;
    }
    #namespaceAliases?: {
        [x: `${string}`]: string;
        'xsd': string;
        'xsi': string;
        'env': string;
    }
    get namespaces() {
        return this.#namespaces ??= {
            'xmlns:xsd': 'http://www.w3.org/2001/XMLSchema',
            'xmlns:xsi': 'http://www.w3.org/2001/XMLSchema-instance',
            'xmlns:env': 'http://www.w3.org/2003/05/soap-envelope',
            // [`xmlns:${this.NS_SOAP_ENV}`]: 'http://www.w3.org/2003/05/soap-envelope',
            [`xmlns:${this.NS_ADDRESSING}`]: 'http://schemas.xmlsoap.org/ws/2004/08/addressing',
            [`xmlns:${this.NS_CIMBINDING}`]: 'http://schemas.dmtf.org/wbem/wsman/1/cimbinding.xsd',
            [`xmlns:${this.NS_ENUM}`]: 'http://schemas.xmlsoap.org/ws/2004/09/enumeration',
            [`xmlns:${this.NS_TRANSFER}`]: 'http://schemas.xmlsoap.org/ws/2004/09/transfer',
            [`xmlns:${this.NS_WSMAN_DMTF}`]: 'http://schemas.dmtf.org/wbem/wsman/1/wsman.xsd',
            [`xmlns:${this.NS_WSMAN_MSFT}`]: 'http://schemas.microsoft.com/wbem/wsman/1/wsman.xsd',
            [`xmlns:${this.NS_WIN_SHELL}`]: 'http://schemas.microsoft.com/wbem/wsman/1/windows/shell',
            [`xmlns:${this.NS_WSMAN_CONF}`]: 'http://schemas.microsoft.com/wbem/wsman/1/config'
        };
    }
    get namespaceAliases() {
        return this.#namespaceAliases ??= {
            'xsd': 'http://www.w3.org/2001/XMLSchema',
            'xsi': 'http://www.w3.org/2001/XMLSchema-instance',
            'env': 'http://www.w3.org/2003/05/soap-envelope',
            // [`${this.NS_SOAP_ENV}`]: 'http://www.w3.org/2003/05/soap-envelope',
            [`${this.NS_ADDRESSING}`]: 'http://schemas.xmlsoap.org/ws/2004/08/addressing',
            [`${this.NS_CIMBINDING}`]: 'http://schemas.dmtf.org/wbem/wsman/1/cimbinding.xsd',
            [`${this.NS_ENUM}`]: 'http://schemas.xmlsoap.org/ws/2004/09/enumeration',
            [`${this.NS_TRANSFER}`]: 'http://schemas.xmlsoap.org/ws/2004/09/transfer',
            [`${this.NS_WSMAN_DMTF}`]: 'http://schemas.dmtf.org/wbem/wsman/1/wsman.xsd',
            [`${this.NS_WSMAN_MSFT}`]: 'http://schemas.microsoft.com/wbem/wsman/1/wsman.xsd',
            [`${this.NS_WIN_SHELL}`]: 'http://schemas.microsoft.com/wbem/wsman/1/windows/shell',
            [`${this.NS_WSMAN_CONF}`]: 'http://schemas.microsoft.com/wbem/wsman/1/config'
        };
    }
}
// }