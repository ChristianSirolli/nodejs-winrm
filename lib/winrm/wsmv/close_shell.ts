import { WSMV } from "../internal.ts";
import type { ConnectionOpts, ShellOptions } from "../types.ts";
import type { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';

export class CloseShell extends WSMV.Base {
    session_opts: ConnectionOpts;
    shell_id: string
    shell_uri: string
    constructor(session_opts: ConnectionOpts, opts: ShellOptions) {
        super();
        if (!opts.shell_id) throw new Error('opts.shell_id is required');
        this.session_opts = session_opts;
        this.shell_id = opts.shell_id;
        this.shell_uri = opts.shell_uri ?? this.RESOURCE_URI_CMD;
    }

    override create_header(header: XMLBuilder) {
        return header.ele(this.#header());
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    override create_body(_body: XMLBuilder): void {
        // no body
    }
    #header() {
        return this.merge_headers(this.shared_headers(this.session_opts),
            this.resource_uri_shell(this.shell_uri),
            this.action_delete(),
            this.selector_shell_id(this.shell_id))
    }
}