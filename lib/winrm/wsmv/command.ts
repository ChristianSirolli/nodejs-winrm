import { randomUUID } from "node:crypto";
import type { ConnectionOpts } from "../types.ts";
import { WSMV } from "../internal.ts";
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';

interface CommandOptions {
    shell_id: string
    command: string
    arguments?: string[]
    shell_uri?: string
    console_mode_stdin?: 'TRUE' | 'FALSE'
    skip_cmd_shell?: 'TRUE' | 'FALSE'
}

export class Command extends WSMV.Base {
    #command_id: string;
    session_opts!: ConnectionOpts;
    shell_id!: string;
    command!: string;
    arguments!: string[];
    shell_uri!: string;
    consolemode!: 'TRUE' | 'FALSE'
    skipcmd!: 'TRUE' | 'FALSE'
    constructor(session_opts: ConnectionOpts, opts: CommandOptions) {
        super();
        this.#command_id = randomUUID().toUpperCase();
        this.#validate_opts(session_opts, opts);
        this.#init_ops(session_opts, opts);
    }
    get command_id() {
        return this.#command_id;
    }
    override build() {
        const xml = super.build();
        return this.#issue69_unescape_single_quotes(xml);
    }
    override create_header(header: XMLBuilder) {
        const _header = this.#header();
        // console.dir(_header, { depth: 5 });
        return header.ele(_header)
    }
    override create_body(body: XMLBuilder) {
        const _body = {
            [`${this.NS_WIN_SHELL}:CommandLine`]: {
                '@CommandId': this.command_id,
                ...this.#body()
            }
        };
        // console.dir(_body);
        return body.ele(_body);
    }
    #init_ops(session_opts: ConnectionOpts, opts: CommandOptions) {
        this.session_opts = session_opts;
        this.shell_id = opts.shell_id;
        this.command = opts.command;
        this.arguments = opts.arguments ?? [];
        this.shell_uri = opts.shell_uri ?? this.RESOURCE_URI_CMD;
        this.consolemode = opts.console_mode_stdin ? opts.console_mode_stdin : 'TRUE';
        this.skipcmd = opts.skip_cmd_shell ? opts.skip_cmd_shell : 'FALSE';
    }
    #validate_opts(session_opts: ConnectionOpts, opts: CommandOptions) {
        if (!session_opts) throw new Error('session_opts is required');
        if (!opts.shell_id) throw new Error('opts.shell_id is required')
        if (!opts.command) throw new Error('opts.command is required')
    }
    #issue69_unescape_single_quotes(xml: string) {
        const regex = new RegExp(`<${this.NS_WIN_SHELL}:Command>(.+)</${this.NS_WIN_SHELL}:Command>`, 'm');
        const escaped_cmd = xml.match(regex)?.at(1);
        return xml.replace(regex, `<${this.NS_WIN_SHELL}:Command>${escaped_cmd?.replace(/&#39;/, "'")}</${this.NS_WIN_SHELL}:Command>`);
    }
    #body() {
        return {
            [`${this.NS_WIN_SHELL}:Command`]: `"${this.command}"`,
            [`${this.NS_WIN_SHELL}:Arguments`]: this.arguments || []
        }
    }
    #header() {
        return this.merge_headers(
            this.shared_headers(this.session_opts),
            this.resource_uri_shell(this.shell_uri),
            this.action_command(),
            this.#header_opts(),
            this.selector_shell_id(this.shell_id)
        )
    }
    #header_opts() {
        if (this.shell_uri != this.RESOURCE_URI_CMD) return {};
        return {
            [`${this.NS_WSMAN_DMTF}:OptionSet`]: {
                [`${this.NS_WSMAN_DMTF}:Option`]: [
                    {
                        '#': this.consolemode,
                        '@Name': 'WINRS_CONSOLEMODE_STDIN'
                    },
                    {
                        '#': this.skipcmd,
                        '@Name': 'WINRS_SKIP_CMD_SHELL'
                    }
                ],
            }
        }
    }
}