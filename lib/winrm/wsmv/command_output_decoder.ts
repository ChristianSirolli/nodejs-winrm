import { PSRP } from "../internal.ts";
import { replaceBOM } from "../helpers.ts";
import { Buffer } from "node:buffer";

export class CommandOutputDecoder {
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    decode(raw_output: string | PSRP.Message): string | undefined {
        let decoded_text = this.#decode_raw_output(raw_output as string);
        // decoded_text = this.#handle_invalid_encoding(decoded_text);
        decoded_text = this.#remove_bom(decoded_text); // xml2js handled this
        return decoded_text;
    }
    #decode_raw_output(raw_output: string) {
        return Buffer.from(raw_output, 'base64').toString('utf-8');
    }
    // #handle_invalid_encoding(decoded_text: string) {
        // if (String.(decoded_text)) return decoded_text
    // }
    #remove_bom(decoded_text: string) {
        return replaceBOM(decoded_text);
    }
}