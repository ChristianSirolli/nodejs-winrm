import { WSMV } from '../internal.ts';
import type { ConnectionOpts } from '../types.ts';
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';

export class InitRunspacePool extends WSMV.Base {
    session_opts: ConnectionOpts
    #shell_id!: string
    payload: (number | bigint)[]
    constructor(session_opts: ConnectionOpts, shell_id: string, payload: (number | bigint)[]) {
        super()
        this.session_opts = session_opts;
        this.shell_id = shell_id;
        this.payload = payload;
    }
    get shell_id() {
        return this.#shell_id;
    }
    set shell_id(value: string) {
        this.#shell_id = value;
    }
    override create_header(header: XMLBuilder) {
        return header.ele(this.#header())
    }
    override create_body(body: XMLBuilder) {
        return body.ele({
            [`${this.NS_WIN_SHELL}:Shell`]: {
                '@ShellId': this.shell_id,
                '@Name': 'Runspace',
                ...this.#body()
            }
        })
    }
    #body() {
        return {
            [`${this.NS_WIN_SHELL}:InputStreams`]: 'stdin pr',
            [`${this.NS_WIN_SHELL}:OutputStreams`]: 'stdout',
            [`creationXml@http://schemas.microsoft.com/powershell`]: this.encode_bytes(this.payload),
        }
    }
    #header() {
        return this.merge_headers(this.shared_headers(this.session_opts),
            this.resource_uri_shell(this.RESOURCE_URI_POWERSHELL),
            this.action_create(),
            this.#header_opts())
    }
    #header_opts() {
        return {
            [`${this.NS_WSMAN_DMTF}:OptionSet`]: {
                [`${this.NS_WSMAN_DMTF}:Option`]: {
                    '#': 2.3,
                    '@Name': 'protocolversion',
                    '@MustComply': 'true'
                },
                '@mustUnderstand@@env': 'true'
            }
        }
    }
}