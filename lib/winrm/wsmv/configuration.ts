import type { ConnectionOpts } from '../types.ts'
import { WSMV } from '../internal.ts';
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';

export class Configuration extends WSMV.Base {
    session_opts: ConnectionOpts
    constructor(session_opts: ConnectionOpts) {
        super()
        this.session_opts = session_opts;
    }
    override create_header(header: XMLBuilder) {
        return header.ele(this.#header())
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    override create_body(_body: XMLBuilder): void {
        // no body
    }
    #header() {
        return this.merge_headers(this.shared_headers(this.session_opts),
            this.resource_uri_shell('http://schemas.microsoft.com/wbem/wsman/1/config'),
            this.action_get())
    }
}