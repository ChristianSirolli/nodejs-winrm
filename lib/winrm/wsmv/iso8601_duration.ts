/**
 * Format an ISO8601 Duration
 * 
 * Converts seconds to ISO8601 duration format
*/
export class Iso8601Duration {
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    /**
     * Convert the number of seconds to an ISO8601 duration format
     * @see http://tools.ietf.org/html/rfc2445#section-4.3.6
     * @param {number} seconds The amount of seconds for this duration
     * @returns {string} ISO8601 duration format
     */
    static sec_to_dur(seconds: number): string {
        let iso_str = 'P';
        if (seconds > 604800) { // more than a week
            const weeks = seconds / 604800;
            seconds -= (604800 * weeks);
            iso_str += `${weeks}W`;
        }
        if (seconds > 86_400) { // more than a day
            const days = seconds / 86_400;
            seconds -= (86_400 * days);
            iso_str += `${days}D`;
        }
        if (seconds > 0) {
            iso_str += 'T'
            if (seconds > 3600) { // more than an hour
                const hours = seconds / 3600;
                seconds -= (3600 * hours);
                iso_str += `${hours}H`;
            }
            if (seconds > 60) { // more than a minute
                const minutes = seconds / 60;
                seconds -= (60 * minutes);
                iso_str += `${minutes}M`;
            }
            iso_str += `${seconds}S`;
        }

        return iso_str
    }
}