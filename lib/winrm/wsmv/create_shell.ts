import type { ConnectionOpts, ShellOptions } from '../types.ts'
import { WSMV } from "../internal.ts"
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';

export class CreateShell extends WSMV.Base {
    // utf8 as default codepage
    // https://msdn.microsoft.com/en-us/library/dd317756(VS.85).aspx
    UTF8_CODE_PAGE = 65001
    session_opts: ConnectionOpts
    shell_uri: string
    #i_stream!: string
    #o_stream!: string
    #codepage!: number
    #noprofile!: 'TRUE' | 'FALSE'
    #working_directory!: string
    #idle_timeout!: number
    #env_vars!: object
    constructor(session_opts: ConnectionOpts, opts: ShellOptions = {}) {
        super();
        this.session_opts = session_opts
        this.shell_uri = this.#opt_or_default(opts, 'shell_uri', this.RESOURCE_URI_CMD) as string;
        this.i_stream = this.#opt_or_default(opts, 'i_stream', 'stdin') as string;
        this.o_stream = this.#opt_or_default(opts, 'o_stream', 'stdout stderr') as string;
        this.codepage = this.#opt_or_default(opts, 'codepage', this.UTF8_CODE_PAGE) as number;
        this.noprofile = this.#opt_or_default(opts, 'noprofile', 'FALSE') as 'TRUE' | 'FALSE';
        this.working_directory = this.#opt_or_default(opts, 'working_directory') as string;
        this.idle_timeout = this.#opt_or_default(opts, 'idle_timeout') as number;
        this.env_vars = this.#opt_or_default(opts, 'env_vars') as object;
    }
    get i_stream() {
        return this.#i_stream
    }
    set i_stream(value: string) {
        this.#i_stream = value;
    }
    get o_stream() {
        return this.#o_stream
    }
    set o_stream(value: string) {
        this.#o_stream = value;
    }
    get codepage() {
        return this.#codepage
    }
    set codepage(value: number) {
        this.#codepage = value;
    }
    get noprofile() {
        return this.#noprofile
    }
    set noprofile(value: 'TRUE' | 'FALSE') {
        this.#noprofile = value;
    }
    get working_directory() {
        return this.#working_directory
    }
    set working_directory(value: string) {
        this.#working_directory = value;
    }
    get idle_timeout() {
        return this.#idle_timeout
    }
    set idle_timeout(value: number) {
        this.#idle_timeout = value;
    }
    get env_vars() {
        return this.#env_vars
    }
    set env_vars(value: object) {
        this.#env_vars = value;
    }
    override create_header(header: XMLBuilder) {
        return header.ele(this.#header())
    }
    override create_body(body: XMLBuilder) {
        return body.ele({
            [`${this.NS_WIN_SHELL}:Shell`]: this.#body()
        })
    }
    #opt_or_default(opts: ShellOptions, key: keyof ShellOptions, default_value?: unknown) {
        return opts[key] ? opts[key] : default_value;
    }
    #body() {
        const body: Record<string, string | Record<string, { '#': unknown[]; '@Name': string[]; }>> = {
            [`${this.NS_WIN_SHELL}:InputStreams`]: this.i_stream,
            [`${this.NS_WIN_SHELL}:OutputStreams`]: this.o_stream,
        }
        if (this.working_directory) body[`${this.NS_WIN_SHELL}:WorkingDirectory`] = this.working_directory;
        if (this.idle_timeout) body[`${this.NS_WIN_SHELL}:IdleTimeout`] = this.#format_idle_timeout(this.idle_timeout);
        if (this.env_vars) body[`${this.NS_WIN_SHELL}:Environment`] = this.#environment_vars_body();
        return body;
    }
    #format_idle_timeout(timeout: number | string): string {
        return typeof timeout == 'string' ? timeout : WSMV.Iso8601Duration.sec_to_dur(timeout);
    }
    #environment_vars_body() {
        return {
            [`${this.NS_WIN_SHELL}:Variable`]: {
                '#': Object.values(this.env_vars),
                '@Name': Object.keys(this.env_vars)
            }
        }
    }
    #header() {
        return this.merge_headers(
            this.shared_headers(this.session_opts),
            this.resource_uri_shell(this.shell_uri),
            this.action_create(),
            this.#header_opts()
        )
    }
    #header_opts() {
        return {
            [`${this.NS_WSMAN_DMTF}:OptionSet`]: {
                [`${this.NS_WSMAN_DMTF}:Option`]: [
                    {
                        '#': this.noprofile,
                        '@Name': 'WINRS_NOPROFILE'
                    },
                    {
                        '#': this.codepage,
                        '@Name': 'WINRS_CODEPAGE'
                    }
                ]
            }
        }
    }
}