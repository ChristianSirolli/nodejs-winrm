import type { ConnectionOpts } from "../types.ts";
import { WSMV } from "../internal.ts";
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';

interface CommandOutOptions {
    shell_id: string
    command_id: string
    shell_uri?: string
    out_streams?: string[]
}

export class CommandOutput extends WSMV.Base {
    session_opts: ConnectionOpts
    shell_id: string
    shell_uri: string
    command_id: string
    out_streams: string[]
    constructor(session_opts: ConnectionOpts, opts: CommandOutOptions) {
        super();
        if (!opts.shell_id) throw new Error('opts.shell_id is required');
        if (!opts.command_id) throw new Error('opts.command_id is required');
        this.session_opts = session_opts;
        this.shell_id = opts.shell_id;
        this.command_id = opts.command_id;
        this.shell_uri = opts.shell_uri ?? this.RESOURCE_URI_CMD;
        this.out_streams = opts.out_streams ?? ['stdout', 'stderr']
    }
    override create_header(header: XMLBuilder) {
        return header.ele(this.#header())
    }
    override create_body(body: XMLBuilder) {
        return body.ele(`${this.NS_WIN_SHELL}:Receive`).ele(this.#body())
    }
    #header() {
        return this.merge_headers(
            this.shared_headers(this.session_opts),
            this.resource_uri_shell(this.shell_uri),
            this.action_receive(),
            this.#header_opts(),
            this.selector_shell_id(this.shell_id)
        )
    }
    #header_opts() {
        return {
            [`${this.NS_WSMAN_DMTF}:OptionSet`]: {
                [`${this.NS_WSMAN_DMTF}:Option`]: {
                    '#': 'TRUE',
                    '@Name': 'WSMAN_CMDSHELL_OPTION_KEEPALIVE'
                }
            }
        }
    }
    #body() {
        return {
            [`${this.NS_WIN_SHELL}:DesiredStream`]: {
                '#': this.out_streams.join(' '),
                '@CommandId': this.command_id
            }
        }
    }
}