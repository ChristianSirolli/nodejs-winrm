import type { ConnectionOpts } from '../types.ts'
import { WSMV, PSRP } from '../internal.ts'
import { XMLBuilder } from 'xmlbuilder2/lib/interfaces.ts';

export class CreatePipeline extends WSMV.Base {
    session_opts: ConnectionOpts
    #shell_id!: string
    #command_id!: string
    #fragment?: PSRP.Fragment
    constructor(session_opts: ConnectionOpts, shell_id: string, command_id: string, fragment?: PSRP.Fragment) {
        super()
        this.command_id = command_id;
        this.session_opts = session_opts;
        this.shell_id = shell_id;
        this.fragment = fragment;
    }
    get shell_id() {
        return this.#shell_id;
    }
    set shell_id(value: string) {
        this.#shell_id = value;
    }
    get command_id() {
        return this.#command_id;
    }
    set command_id(value: string) {
        this.#command_id = value;
    }
    get fragment(): PSRP.Fragment | undefined {
        return this.#fragment;
    }
    set fragment(value: PSRP.Fragment | undefined) {
        this.#fragment = value;
    }
    override create_header(header: XMLBuilder) {
        return header.ele(this.#header())
    }
    override create_body(body: XMLBuilder) {
        return body.ele({
            [`${this.NS_WIN_SHELL}:CommandLine`]: {
                '@CommandId': this.command_id,
                ...this.#body()
            }
        })
    }
    #body() {
        return {
            [`${this.NS_WIN_SHELL}:Command`]: 'Invoke-Expression',
            [`${this.NS_WIN_SHELL}:Arguments`]: this.#arguments()
        }
    }
    #header() {
        const data = this.merge_headers(this.shared_headers(this.session_opts),
            this.resource_uri_shell(this.RESOURCE_URI_POWERSHELL),
            this.action_command(),
            this.selector_shell_id(this.shell_id))
        if (!this.shell_id) console.trace(JSON.stringify(data));
        return data;
    }
    #arguments() {
        if (this.fragment) return this.encode_bytes(this.fragment.bytes());
    }
}