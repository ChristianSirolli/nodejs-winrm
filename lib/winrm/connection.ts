import { ConnectionOpts } from './connection_opts.ts';
import type { InitialCertConnectionOptions, InitialConnectionOptions, Transport, WinRMOptions } from './types.ts';
import { type Logger, loggers, transports, format } from 'winston';
import { WSMV, Shells, HTTP } from './internal.ts';

export class Connection {
    connection_opts!: ConnectionOpts;
    #logger!: Logger
    #_shell_factory?: Shells.ShellFactory
    #_transport?: Transport
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    constructor(opts: (InitialConnectionOptions | InitialCertConnectionOptions) & Partial<WinRMOptions>) {
        this.#configure_connection_opts(opts);
        this.#configure_logger();
    }
    get logger() {
        return this.#logger
    }
    set logger(value: Logger) {
        this.#logger = value
    }
    shell(shell_type: string) {
        return this.#shell_factory.create_shell(shell_type);
    }
    run_wql(wql: string, namespace = 'root/cimv2/*') {
        const query = new WSMV.WqlQuery(this.#transport, this.connection_opts, wql, namespace);
        return query.process_response(this.#transport.send_request(query.build()));
    }
    #configure_connection_opts(opts: (InitialConnectionOptions | InitialCertConnectionOptions) & Partial<WinRMOptions>) {
        this.connection_opts = ConnectionOpts.create_with_defaults(opts);
    }
    #configure_logger() {
        this.#logger = loggers.get('WinRM');
        this.#logger.exitOnError = false;
        this.#logger.add(new transports.Console({ level: 'warn', handleExceptions: true, handleRejections: true, format: format.prettyPrint({'colorize': true, 'depth': 10})})); 
    }
    get #shell_factory() {
        return this.#_shell_factory ??= new Shells.ShellFactory(this.connection_opts, this.#transport, this.logger)
    }
    get #transport() {
        return this.#_transport ??= new HTTP.TransportFactory().create_transport(this.connection_opts);
    }
}