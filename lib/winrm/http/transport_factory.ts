import { HTTP, InvalidTransportError } from "../internal.ts";
import type { ConnectionOpts } from "../types.ts";

export class TransportFactory {
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    create_transport(connection_opts: ConnectionOpts) {
        const transport = connection_opts.get('transport')!;
        this.#validate_transport(transport);
        return this[`init_${transport}_transport`](connection_opts);
    }
    private init_negotiate_transport(opts: ConnectionOpts) {
        return new HTTP.HttpNegotiate(opts.get('endpoint') as string, opts.get('user')!, opts.get('password')!, opts);
    }
    private init_kerberos_transport(opts: ConnectionOpts) {
        return new HTTP.HttpGSSAPI(opts.get('endpoint') as string, opts.get('realm') ?? '', opts, opts.get('service'));
    }
    private init_plaintext_transport(opts: ConnectionOpts) {
        return new HTTP.HttpPlainText(opts.get('endpoint') as string, opts.get('user')!, opts.get('password')!, opts);
    }
    private init_ssl_transport(opts: ConnectionOpts) {
        if (opts.get('basic_auth_only')) {
            return new HTTP.BasicAuthSSL(opts.get('endpoint') as string, opts.get('user')!, opts.get('password')!, opts);
        } else if (opts.get('client_cert')) {
            return new HTTP.ClientCertAuthSSL(opts.get('endpoint') as string, opts.get('client_cert')!, opts.get('client_key')!, opts.get('key_pass')!, opts);
        } else {
            return new HTTP.HttpNegotiate(opts.get('endpoint') as string, opts.get('user')!, opts.get('password')!, opts);
        }
    }
    #validate_transport(transport: string) {
        const valid: string[] = Reflect.ownKeys(TransportFactory.prototype)
            .filter(m => m.toString().match(/^init_(.*?)_transport/))
            .map(tm => tm.toString().split('_')[1]);
        if (!valid.includes(transport.toString())) {
            throw new InvalidTransportError(transport, valid);
        }
    }
}