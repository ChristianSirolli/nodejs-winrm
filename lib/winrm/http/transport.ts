import { Agent as HTTPSAgent } from 'node:https';
import type { Document as SlimDocument } from 'slimdom';
import { Socket } from 'node:net';
import { loggers, type Logger } from 'winston';
import { ClientRequest, type OutgoingHttpHeaders } from 'node:http';
import * as tls from 'node:tls';
import kerberos from 'kerberos';
import { existsSync, PathLike, readFileSync, realpathSync } from 'node:fs';
import * as authorization from 'auth-header';
import { ResponseHandler, WinRMHTTPTransportError } from '../internal.ts';
import type { ConnectionOpts, AuthorizationToken } from '../types.ts';
import { HTTPClient, NTLMClient } from '../helpers.ts';
import { create } from 'xmlbuilder2';
import { Buffer } from "node:buffer";
import { URL } from "node:url";

export class HttpTransport {
    #endpoint: URL
    httpcli: HTTPClient
    logger: Logger
    ssl_peer_cert?: tls.PeerCertificate
    ssl_peer_fingerprint = ''
    ssl_peer_fingerprint_verified = false
    headers: OutgoingHttpHeaders = {}
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    get endpoint() {
        return this.#endpoint;
    }
    constructor(endpoint: string | URL, opts: ConnectionOpts) {
        this.#endpoint = typeof endpoint == 'string' ? new URL(endpoint) : endpoint;
        this.httpcli = new HTTPClient();
        this.logger = loggers.get('WinRM');
        this.httpcli.timeout = opts.get('receive_timeout')! * 1000;
        this.httpcli.headers = {
            'User-Agent': opts.get('user_agent') ?? ''
        };
        // , {
        //     headersTimeout: opts.get('receive_timeout')! * 1000,
        //     headers: {
        //         'User-Agent': opts.get('user_agent')
        //     } as HeadersInit,
        //     // httpsAgent: new HTTPSAgent({ keepAlive: false }),
        //     // httpAgent: new HTTPSAgent({ keepAlive: false }),
        //     // responseType: 'text',
        //     withCredentials: true,
        //     validateStatus: status => Boolean(status),
        // });
    }
    async send_request(message: string): Promise<null | SlimDocument> {
        await this.ssl_peer_fingerprint_verification();
        this.log_soap_message(message);
        const hdr: OutgoingHttpHeaders = {
            'Content-Type': 'application/soap+xml;charset=UTF-8',
            'Content-Length': Buffer.byteLength(message).toString(),
        }
        // We need to add this header if using Client Certificate authentication
        if (this.httpcli.httpsAgent.options.clientCertEngine) {
            hdr.Authorization = 'http://schemas.dmtf.org/wbem/wsman/1/wsman/secprofile/https/mutual'
        }

        const resp = await this.httpcli.post<string>(this.endpoint, { body: message, headers: hdr });
        // this.logger.debug([resp.headers.get('www-authenticate'), resp.status, resp.statusText]);
        // const data = (await resp.body?.getReader().read())?.value?.toString() ?? '';
        this.log_soap_message(resp.data);
        this.verify_ssl_fingerprint(await this._form_ssl_connection((resp.request as unknown as ClientRequest).socket).then(conn => conn.getPeerCertificate()))
        const handler = new ResponseHandler(resp.data, resp.status!);
        return handler.parse_to_xml();
    }
    basic_auth_only() {
        this.httpcli.interceptors.request.use(config => {
            const auths: (AuthorizationToken)[] = [];
            if (config.headers.get('authorization') !== null) {
                if (!config.headers.get('authorization')?.includes(',')) {
                    auths.push(authorization.parse(config.headers.get('authorization')));
                } else {
                    config.headers.get('authorization')?.split(',').forEach(val => auths.push(authorization.parse(val)))
                }
                Object.values(auths).forEach(auth => {
                    if (auth.scheme.toLowerCase() != 'basic') {
                        auths.splice(auths.indexOf(auth), 1);
                    }
                });
                auths.forEach(auth => config.headers.append('authorization', authorization.format(auth.scheme, auth.token !== null ? typeof auth.token != 'string' ? auth.token.join(' ') : auth.token : undefined, auth.params)));
            }
            return config;
        });
        // return auths;
    }
    no_sspi_auth() {
        this.httpcli.interceptors.request.use(config => {
            const auths: (AuthorizationToken)[] = [];
            if (config.headers.get('authorization')) {
                if (config.headers.get('authorization')?.includes(',')) {
                    auths.push(authorization.parse(config.headers.get('authorization')));
                } else {
                    config.headers.get('authorization')?.split(',').forEach(val => auths.push(authorization.parse(val)));
                }
                Object.values(auths).forEach(auth => {
                    if (auth.scheme.toLowerCase() == 'ntlm' || auth.scheme.toLowerCase() == 'negotiate') {
                        auths.splice(auths.indexOf(auth), 1);
                    }
                });
                auths.forEach(auth => config.headers.append('authorization', authorization.format(auth.scheme, auth.token !== null ? typeof auth.token != 'string' ? auth.token.join(' ') : auth.token : undefined, auth.params)));
            }
            return config;
        })
    }
    no_ssl_peer_verification() {
        (this.httpcli.defaults.httpsAgent as HTTPSAgent).options.rejectUnauthorized = false;
    }
    async ssl_peer_fingerprint_verification() {
        if (!this.ssl_peer_fingerprint || this.ssl_peer_fingerprint_verified) return;
        const connection = await this.with_untrusted_ssl_connection(),
            connection_cert = connection.getPeerCertificate(),
            err = this.verify_ssl_fingerprint(connection_cert);
        if (err) throw err;
        this.logger.info(`initial ssl fingerprint ${this.ssl_peer_fingerprint} verified`);
        this.ssl_peer_fingerprint_verified = true;
    }
    with_untrusted_ssl_connection(): Promise<tls.TLSSocket> {
        const tcp_connection = new Socket();
        try {
            return this._form_ssl_connection(tcp_connection, {
                rejectUnauthorized: false,
            });
        } finally {
            tcp_connection.end();
        }
    }
    _form_ssl_connection(tcp_connection: Socket | null, tlsSocketOptions?: tls.TLSSocketOptions) {
        let close_tcp_connection = false;
        if (tcp_connection === null) {
            tcp_connection = new Socket();
            close_tcp_connection = true;
        }
        const ssl_connection = new tls.TLSSocket(tcp_connection, tlsSocketOptions).connect(Number(this.endpoint.port), this.endpoint.hostname);
        return new Promise((resolve: (value: tls.TLSSocket) => void, reject) => {
            tcp_connection.addListener('error', reject);
            void new Promise((resolve2: (value: tls.TLSSocket) => void, reject) => {
                resolve2(ssl_connection);
                ssl_connection.addListener('error', reject)
            }).then(resolve).finally(() => {
                ssl_connection.end();
                if (close_tcp_connection) {
                    tcp_connection.end();
                }
            })
        })
    }
    verify_ssl_fingerprint(cert: tls.PeerCertificate) {
        if (!this.ssl_peer_fingerprint) {
            return;
        }
        this.ssl_peer_cert = cert;
        const conn_fingerprint = cert.fingerprint;
        if (this.ssl_peer_fingerprint.localeCompare(conn_fingerprint, undefined, { sensitivity: 'base' }) == 0) return;
        return new Error('ssl fingerprint mismatch!!!!');
    }
    body(message: string, length: number, type = 'application/HTTP-SPNEGO-session-encrypted') {
        return [
            '--Encrypted Boundary',
            `Content-Type: ${type}`,
            `OriginalContent: type=application/soap+xml;charset=UTF-8;Length=${length}`,
            '--Encrypted Boundary',
            'Content-Type: application/octet-stream',
            `${message}--Encrypted Boundary--`
        ].join("\r\n").concat("\r\n");
    }
    log_soap_message(message: string) {
        try {
            if (!this.logger.debug) return;
            const xml_msg = create({}, message);
            this.logger.debug(xml_msg.toString({ prettyPrint: true }));
        } catch (e) {
            this.logger.debug(`Couldn't log SOAP request/response: ${(e as Error).message} - ${message}`);
        }
    }
}
export class HttpPlainText extends HttpTransport {
    constructor(endpoint: string | URL, user: string, pass: string, opts: ConnectionOpts) {
        super(endpoint, opts);
        this.httpcli.defaults.auth = {
            username: user,
            password: pass
        };
        if (opts.get('disable_sspi')) this.no_sspi_auth();
        if (opts.get('basic_auth_only')) this.basic_auth_only();
        if (opts.get('no_ssl_peer_verification')) this.no_ssl_peer_verification();
    }
}
export class HttpNegotiate extends HttpTransport {
    retryable: boolean
    ntlmcli: NTLMClient
    constructor(endpoint: string | URL, user: string, pass: string, opts: ConnectionOpts) {
        super(endpoint, opts);
        this.no_sspi_auth();

        const user_parts = user.split('\\');
        if (user_parts.length > 1) {
            opts.set('domain', user_parts[0]);
            user = user_parts[1];
        }

        this.ntlmcli = new NTLMClient(this.httpcli, user, pass, opts);
        this.retryable = true;
        if (opts.get('no_ssl_peer_verification')) this.no_ssl_peer_verification();
        this.ssl_peer_fingerprint = opts.get('ssl_peer_fingerprint')!;
        if (opts.has('ca_trust_path')) (this.httpcli.defaults.httpsAgent as HTTPSAgent).options.ca = [...tls.rootCertificates, opts.get('ca_trust_path')!];
        // if (opts.has('cert_store')) (this.httpcli.defaults.httpsAgent as HTTPSAgent).options.cert_store = opts.get('cert_store');
    }
    override async send_request(message: string): Promise<SlimDocument> {
        await this.ssl_peer_fingerprint_verification();
        if (!this.ntlmcli.session) await this.#init_auth();
        this.log_soap_message(message);
        const hdr = {
            'Content-Type': 'multipart/encrypted;protocol="application/HTTP-SPNEGO-session-encrypted";boundary="Encrypted Boundary"',
        };
        const resp: AxiosResponse | Error = await this.httpcli.post(this.endpoint.toString(), this.body(this.#seal(message), Buffer.byteLength(message)), {
            headers: hdr
        }).catch((err) => {
            return err as Error;
        });
        if (!(resp instanceof Error)) {
            this.logger.debug([resp.headers.get('www-authenticate'), resp.status, resp.statusText]);
            this.verify_ssl_fingerprint(await this._form_ssl_connection((resp.request as unknown as ClientRequest).socket).then(conn => conn.getPeerCertificate())); // should we throw if it returns an error?
            if (resp.status == 401 && this.retryable) {
                this.retryable = false;
                await this.#init_auth();
                return this.send_request(message)
            } else {
                this.retryable = true;
                const decrypted_body = this.#winrm_decrypt(resp);
                this.log_soap_message(decrypted_body);
                return new ResponseHandler(resp.data as string, resp.status).parse_to_xml();
            }
        } else {
            if (axios.isAxiosError(resp)) {
                this.logger.error((resp as AxiosError).message)
                this.logger.error((resp as AxiosError).response)
            }
            throw resp;
        }
    }
    #seal(message: string): string {
        const emessage = this.ntlmcli.session?.seal_message(message),
            signature = this.ntlmcli.session?.sign_message(message);
        return `\x10\x00\x00\x00${signature}${emessage}`;
    }
    #winrm_decrypt(resp: AxiosResponse): string {
        // OMI server doesn't always respond to encrypted messages with encrypted responses over SSL
        if ((resp.headers.get('Content-Type') ?? '')?.match(/^application\/soap\+xml/i)) return resp.data as string;
        if ((resp.data as string).length == 0) return '';
        let str = Buffer.from(resp.data as string).toString('binary');
        str = str.replace(/^.*Content-Type: application\/octet-stream\r\n(.*)--Encrypted.*$/m, '$1');
        const signature = str.slice(4, 20);
        const message = this.ntlmcli.session?.unseal_message(str.slice(20));
        if (message && this.ntlmcli.session?.verify_signature(signature, message)) return message;
        // if (str) return str;
        throw new WinRMHTTPTransportError('Could not decrypt NTLM message.');
    }
    async #issue_challenge_response(negotiate: string) {
        const auth_header = {
            Authorization: `Negotiate ${negotiate}`,
            'Content-Type': 'application/soap+xml;charset=UTF-8'
        }
        // OMI Server on Linux requires an empty payload with the new auth header to proceed
        // because the config check for max payload size will otherwise break the auth handshake
        // given the OMI server does not support that check
        await this.httpcli.post(this.endpoint.toString(), { headers: auth_header, data: '' });
        return {};
    }
    async #init_auth(): Promise<Record<string, string[] | string>> {
        this.logger.debug(`Initializing Negotiate for ${this.endpoint.toString()}`);
        const auth1 = this.ntlmcli.init_context().match(/NTLM (.+)/)?.at(1) ?? '';
        const hdr = {
            Authorization: `Negotiate ${auth1}`,
            'Content-Type': 'application/soap+xml;charset=UTF-8'
        };
        this.logger.debug('Sending HTTP POST for Negotate Authentication');
        const r = await this.httpcli.post(this.endpoint.toString(), { headers: hdr, data: '' });
        this.verify_ssl_fingerprint(await this._form_ssl_connection((r.request as unknown as ClientRequest).socket).then(conn => conn.getPeerCertificate())) // should we throw if it returns an error?
        const auth_header = r.headers.get('www-authenticate');
        if (!auth_header) {
            const msg = `Unable to parse authorization header. Headers: ${JSON.stringify(r.headers)}\r\nBody: ${r.data}`;
            throw new WinRMHTTPTransportError(msg, r.status);
        }
        const itok = auth_header.split(' ').at(-1);
        const auth3 = this.ntlmcli.init_context(itok/*, this.#channel_binding(r)*/)
        return this.#issue_challenge_response(auth3.match(/NTLM (.+)/)?.at(1) ?? '');
    }
    // #channel_binding(response: AxiosResponse) {
    //     if (!response.request.res.socket.getPeerCertificate()) {
    //         return;
    //     } else {
    //         return new NTLMChannelBinding(response.request.res.socket.getPeerCertificate());
    //     }
    // }
}
export class BasicAuthSSL extends HttpTransport {
    constructor(endpoint: string | URL, user: string, pass: string, opts: ConnectionOpts) {
        super(endpoint, opts);
        this.httpcli.defaults.auth = {
            // endpoint: endpoint.toString()
            username: user,
            password: pass
        };
        this.basic_auth_only();
        if (opts.get('no_ssl_peer_verification')) this.no_ssl_peer_verification();
        this.ssl_peer_fingerprint = opts.get('ssl_peer_fingerprint')!;
        if (opts.has('ca_trust_path')) (this.httpcli.defaults.httpsAgent as HTTPSAgent).options.ca = [...tls.rootCertificates, opts.get('ca_trust_path')!];
        // if (opts.has('cert_store')) (this.httpcli.defaults.httpsAgent as HTTPSAgent).options.cert_store = opts.get('cert_store');
    }
}
export class ClientCertAuthSSL extends HttpTransport {
    constructor(endpoint: string | URL, client_cert: PathLike, client_key: PathLike, key_pass: string, opts: ConnectionOpts) {
        super(endpoint, opts);
        const agent = this.httpcli.defaults.httpsAgent as HTTPSAgent;
        if (client_cert) {
            if (existsSync(client_cert)) {
                agent.options.cert = readFileSync(typeof client_cert == 'string' ? realpathSync(client_cert) : client_cert);
            } else if (client_cert instanceof URL) {
                throw new Error('Client certificate file not found');
            } else {
                agent.options.cert = client_cert as string | Buffer;
            }
        }
        if (client_key) {
            if (existsSync(client_key)) {
                agent.options.cert = readFileSync(typeof client_key == 'string' ? realpathSync(client_key) : client_key);
            } else if (client_key instanceof URL) {
                throw new Error('Client certificate key file not found');
            } else {
                agent.options.cert = client_key as string | Buffer;
            }
        }
        agent.options.passphrase = key_pass;
        this.httpcli.defaults.auth = undefined;
        if (opts.get('no_ssl_peer_verification')) this.no_ssl_peer_verification();
        this.ssl_peer_fingerprint = opts.get('ssl_peer_fingerprint')!;
        if (opts.has('ca_trust_path')) agent.options.ca = [...tls.rootCertificates, opts.get('ca_trust_path')!];
        // if (opts.has('cert_store')) (this.httpcli.defaults.httpsAgent as HTTPSAgent).options.ca = opts.get('cert_store');
    }
}
export class HttpGSSAPI extends HttpTransport {
    service: string
    gsscli!: kerberos.KerberosClient
    opts: ConnectionOpts
    constructor(endpoint: string | URL, realm: string, opts: ConnectionOpts, service = 'HTTP') {
        super(endpoint, opts);
        this.opts = opts;
        this.service = `${service}/${this.endpoint.hostname}@${realm}`;
        if (opts.get('no_ssl_peer_verification')) this.no_ssl_peer_verification();
    }
    override async send_request(message: string) {
        let resp = await this.#send_kerberos_request(message);
        if (!resp) return null;
        if (resp.status == 401) {
            this.logger.debug('Got 401 - reinitializing Kerberos and retrying one more time');
            await this.#init_krb();
            resp = await this.#send_kerberos_request(message);
            if (!resp) return null;
        }
        const handler = new ResponseHandler(await this.#winrm_decrypt(resp.data as string), resp.status);
        return handler.parse_to_xml();
    }
    async #send_kerberos_request(message: string) {
        // try {
            if (!this.gsscli) await this.#init_krb();
            this.log_soap_message(message);
            const original_length = Buffer.byteLength(message);
            const [pad_len, emsg] = await this.#winrm_encrypt(message);
            const req_length = original_length + pad_len;
            const hdr: OutgoingHttpHeaders = {
                'connection': 'keep-alive',
                'content-type': 'multipart/encrypted;protocol="application/HTTP-Kerberos-session-encrypted";boundary="Encrypted Boundary"',
            };
            const resp = await this.httpcli.post(
                this.endpoint.toString(),
                this.body(emsg, req_length, 'application/HTTP-Kerberos-session-encrypted'),
                {
                    headers: hdr as Record<string, string>
                }
            );
            this.log_soap_message(resp.data as string)
            return resp;
        // } catch (e) {
        //     this.logger.error((e as Error).stack);
        // }
    }
    async #init_krb() {
        // try {
            this.logger.debug(`Initializing Kerberos for ${this.service}`);
            const r = await this.httpcli.post(this.endpoint.toString(), '');
            this.logger.debug([r.headers.get('www-authenticate'), r.status]);
            this.gsscli = await kerberos.initializeClient(this.service, { principal: `${this.opts.get('user')}@${this.opts.get('realm')}`, gssFlag: kerberos.GSS_C_DELEG_FLAG|kerberos.GSS_C_MUTUAL_FLAG|kerberos.GSS_C_SEQUENCE_FLAG, mechOID: kerberos.GSS_MECH_OID_KRB5 });
            const token = await this.gsscli.step("");
            const auth = Buffer.from(token).toString('base64');
            const hdr: OutgoingHttpHeaders = {
                'authorization': `Kerberos ${auth}`,
                'connection': 'keep-alive',
                'content-type': 'application/soap+xml;charset=UTF-8',
            }
            this.logger.debug(`Sending HTTP POST for Kerberos Authentication`);
            const kr = await this.httpcli.post(this.endpoint.toString(), '', {
                headers: hdr as Record<string, string>
            });
            this.logger.debug([kr.headers.get('www-authenticate'), kr.status]);
            const itok = (kr.headers.get('www-authenticate') ?? '').split(/\s+/).at(-1);
            return this.gsscli.step(itok ?? '');
        // } catch (e) {
        //     this.logger.error(e);
        // }
    }
    async #winrm_encrypt(str: string): Promise<[number, string]> {
        this.logger.debug(`Encrypting SOAP message:\n${str}`);
        // str = str.replace(/^.*Content-Type: application\/octet-stream\r\n(.*)--Encrypted.*$/, '$1');
        const val = await this.gsscli.wrap(str, { user: this.opts.get('user') });
        return [0, val];
    }
    async #winrm_decrypt(str: string): Promise<string> {
        this.logger.debug(`Decrypting SOAP message:\n${str}`);
        str = str.replace(/^.*Content-Type: application\/octet-stream\r\n(.*)--Encrypted.*$/, '$1');
        const val = await this.gsscli.unwrap(str);
        this.logger.debug(`SOAP message decrypted:\n${val}`);
        return val;
    }
}