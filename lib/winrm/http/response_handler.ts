import { DOMParser, type Document as SlimDocument } from 'slimdom';
import { WinRMHTTPTransportError, WinRMAuthorizationError, WinRMWSManFault, WinRMWMIError, WSMV, WinRMSoapFault } from '../internal.ts';
import { select, select1 } from 'xpath';
// import { parserErrorHandler } from '../helpers.ts';
export class ResponseHandler extends WSMV.SOAP {
    response_body: string
    status_code: number
    #_response_xml!: SlimDocument
    constructor(response_body: string, status_code: number) {
        super();
        this.response_body = response_body;
        this.status_code = status_code;
    }
    parse_to_xml() {
        this.#throw_if_error()
        return this.#response_xml;
    }
    get #response_xml() {
        try {
            return this.#_response_xml ??= new DOMParser().parseFromString(this.response_body, "application/xml");
        } catch (err) {
            throw new WinRMHTTPTransportError(`Unable to parse WinRM response: ${(err as Error).message}`, this.status_code);
        }
    }
    #throw_if_error() {
        if (this.status_code == 200) return;
        this.#throw_if_auth_error();
        this.#throw_if_wsman_fault();
        this.#throw_if_wmi_error();
        this.#throw_if_soap_fault();
        this.#throw_transport_error();
    }
    #throw_if_auth_error() {
        if (this.status_code == 401) {
            // console.log(process.env);
            throw new WinRMAuthorizationError();
        }
    }
    #throw_if_wsman_fault() {
        const soap_errors = select("//*[local-name() = 'Envelope']/*[local-name() = 'Body']/*[local-name() = 'Fault']/*", this.#response_xml as unknown as Document) as Element[] | null;
        if (soap_errors === null) return;
        const fault = select1("//*[local-name() = 'WSManFault']", soap_errors[0]) as Element;
        if (fault != null && fault != undefined) {
            throw new WinRMWSManFault(fault.textContent ?? '', fault.attributes.getNamedItem('Code')?.value ?? '');
        }
    }
    #throw_if_wmi_error() {
        const soap_errors = select("//*[local-name() = 'Envelope']/*[local-name() = 'Body']/*[local-name() = 'Fault']/*", this.#response_xml as unknown as Document) as Element[] | null;
        if (soap_errors === null) return;
        const error = select1("//*[local-name() = 'MSFT_WmiError']", soap_errors[0]) as Element;
        if (error == null) return;
        const error_code = (select1("//*[local-name() = 'error_Code']", error) as Element)?.textContent;
        throw new WinRMWMIError(error.textContent ?? '', error_code ?? '');
    }
    #throw_if_soap_fault() {
        const soap_errors = select("//*[local-name() = 'Envelope']/*[local-name() = 'Body']/*[local-name() = 'Fault']/*", this.#response_xml as unknown as Document) as Element[] | null;
        if (soap_errors === null) return;
        const code = (select1("//*[local-name() = 'Code']/*[local-name() = 'Value']/text()", soap_errors[0]) as Text)?.textContent,
            subcode = (select1("//*[local-name() = 'Subcode']/*[local-name() = 'Value']/text()", soap_errors[0]) as Text)?.textContent,
            reason = (select1("//*[local-name() = 'Reason']/*[local-name() = 'Text']/text()", soap_errors[0]) as Text)?.textContent;
        if (!(code === null && subcode === null && reason === null)) throw new WinRMSoapFault(code, subcode, reason);
    }
    #throw_transport_error() {
        throw new WinRMHTTPTransportError(
            `Bad HTTP response returned from server. Body(if present): ${this.response_body}`,
            this.status_code
        );
    }
}