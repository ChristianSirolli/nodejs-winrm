import { InvalidExitCode } from "./internal.ts";

export class Output extends Array {
    #data: {
        stdout?: string
        stderr?: string
    }[]
    #exitcode = 0
    constructor() {
        super();
        this.#data = [];
    }
    get exitcode() {
        return this.#exitcode;
    }
    set exitcode(code: number) {
        if (!Number.isInteger(code)) throw new InvalidExitCode();
        this.#exitcode = code;
    }
    output() {
        return this.#data.flatMap(line => [line.stdout, line.stderr]).filter(v => v).join('');
    }
    stdout() {
        return this.#data.map(line => line.stdout).filter(v => v).join('');
    }
    stderr() {
        return this.#data.map(line => line.stderr).filter(v => v).join('');
    }
    override push(...items: {
        stdout?: string
        stderr?: string
    }[]): number {
        return this.#data.push(...items);
    }
    override pop() {
        return this.#data.pop();
    }
    // static data(data)
}