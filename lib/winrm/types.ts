import type { PathLike } from 'node:fs';
import type * as HTTP from './http/transport.ts';
export type { ConnectionOpts } from './connection_opts.ts';

export interface WinRMOptions {
    transport?: TransportString
    receive_timeout?: number
    operation_timeout?: number
    session_id?: Uppercase<`${string}-${string}-${string}-${string}-${string}`>
    locale?: string
    max_envelope_size?: number
    retry_delay?: number
    retry_limit?: number
    user_agent?: string
    endpoint?: URL | string
    user?: string
    password?: string
    basic_auth_only?: boolean
    client_cert?: PathLike
    client_key?: PathLike
    key_pass?: string
    realm?: string
    service?: string
    disable_sspi?: boolean
    no_ssl_peer_verification?: boolean
    domain?: string
    ssl_peer_fingerprint?: string
    ca_trust_path?: string
    workstation?: string
    max_commands?: number
    cert_store?: unknown // not implemented
}

export interface InitialConnectionOptions {
    endpoint: string
    user: string
    password: string
}
export interface InitialCertConnectionOptions {
    endpoint: string
    client_cert: PathLike
    client_key: PathLike
}

export interface ShellOptions {
    shell_id?: string
    shell_uri?: string
    i_stream?: string
    o_stream?: string
    codepage?: number
    noprofile?: 'TRUE' | 'FALSE'
    working_directory?: string
    idle_timeout?: number
    env_vars?: object
}

export type TransportString = 'negotiate' | 'kerberos' | 'plaintext' | 'ssl';

export interface Stream {
    type: "stdout" | "stderr";
    text: string;
}

export type Transport = HTTP.HttpNegotiate | HTTP.HttpGSSAPI | HTTP.HttpPlainText | HTTP.BasicAuthSSL | HTTP.ClientCertAuthSSL

export type MessageType = 0x00010002 | 0x00010004 | 0x00010005 | 0x00010006 | 0x00010007 | 0x00010008 | 0x0002100b | 0x0002100c | 0x00021002 | 0x00021003 | 0x00021004 | 0x00021005 | 0x00021006 | 0x00021007 | 0x00021008 | 0x00021009 | 0x0002100a | 0x00021100 | 0x00021101 | 0x00041002 | 0x00041003 | 0x00041004 | 0x00041005 | 0x00041006 | 0x00041007 | 0x00041008 | 0x00041009 | 0x00041010 | 0x00041011 | 0x00041100 | 0x00041101

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export declare type Constructor<T = any> = new (...args: any[]) => T;

export declare type Mixin = (superclass: Constructor) => Constructor

export interface AuthorizationToken {
    scheme: string;
    params: Record<string, string | string[]>;
    token: null | string | string[];
}