import type { Transport, ConnectionOpts } from "../types.ts";
import type { Logger } from "winston";
import { Shells, InvalidShellError } from '../internal.ts';
export class ShellFactory {
    opts: ConnectionOpts
    transport: Transport
    logger: Logger
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    constructor(opts: ConnectionOpts, transport: Transport, logger: Logger) {
        this.opts = opts;
        this.transport = transport;
        this.logger = logger;
    }
    create_shell(shell_type: string) {
        const type = shell_type.toLowerCase().split('').map((v, i) => i == 0 ? v.toUpperCase() : v).join('');
        if (Object.keys(Shells).includes(type)) {
            return new Shells[type as 'Cmd' | 'Powershell'](this.opts, this.transport, this.logger);
        } else {
            throw new InvalidShellError(`${type} is not a valid WinRM shell type.\nExpected either Cmd or Powershell.`);
        }
    }
}