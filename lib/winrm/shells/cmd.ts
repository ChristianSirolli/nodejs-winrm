import type { Logger } from "winston";
import type { Transport, ConnectionOpts } from "../types.ts";
import { WSMV, Shells } from '../internal.ts';
import { select1 } from "xpath";

export class Cmd extends Shells.Base {
    // Finalizer is handled by Base
    #response_reader?: WSMV.ReceiveResponseReader
    static {
        this.close_shell = function (connection_opts, transport, shell_id) {
            const msg = new WSMV.CloseShell(connection_opts, { shell_id });
            return transport.send_request(msg.build());
        }
    }

    constructor(opts: ConnectionOpts, transport: Transport, logger: Logger) {
        super(opts, transport, logger)
    }

    override async send_command(_command: string, _args: string[]): Promise<string> {
        const cmd_msg = new WSMV.Command(
            this.connection_opts, {
            shell_id: this.shell_id!,
            command: _command,
            arguments: _args
        });
        const resp_doc = await this.transport.send_request(cmd_msg.build());
        const command_id = (select1("//*[local-name() = 'CommandId']", resp_doc! as unknown as Document) as Element).textContent!;
        this.logger.debug(`[WinRM] Command created for ${_command} with ${command_id}`);
        return command_id;
    }
    override get response_reader(): WSMV.ReceiveResponseReader {
        return this.#response_reader ??= new WSMV.ReceiveResponseReader(this.transport, this.logger);
    }
    override async open_shell() {
        const msg = new WSMV.CreateShell(this.connection_opts, this.shell_opts);
        const resp_doc = await this.transport.send_request(msg.build());
        return (select1("//*[@Name='ShellId']", resp_doc! as unknown as Document) as Element).textContent!;
    }
    override out_streams(): string[] {
        return ['stdout', 'stderr'];
    }
}