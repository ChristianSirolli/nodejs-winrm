import { AxiosError } from 'feaxios';
import { WinRMWSManFault, WinRMHTTPTransportError, WinRMAuthorizationError } from '../internal.ts';
export class Retryable {
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    #RETRYABLE_EXCEPTIONS: () => string[] = () => [
        'EACCES', 'EADDRINUSE', 'ECONNREFUSED', 'ETIMEDOUT',
        'ECONNRESET', 'ENETUNREACH', 'EHOSTUNREACH', WinRMWSManFault.prototype.name,
        WinRMHTTPTransportError.prototype.name, WinRMAuthorizationError.prototype.name,
        AxiosError.ECONNABORTED, AxiosError.ETIMEDOUT
    ]
    async retryable(retries: number, delay: number, block: () => Promise<void>) {
        try {
            return await block();
        } catch (e) {
            if (((e as Error).name in this.#RETRYABLE_EXCEPTIONS() || (e as AxiosError).code! in this.#RETRYABLE_EXCEPTIONS()) && retries-- > 0) {
                return new Promise(resolve => setTimeout(resolve, delay));
            } else {
                throw e;
            }
        }
    }
}