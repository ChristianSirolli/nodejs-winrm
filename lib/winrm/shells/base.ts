import type { Logger } from 'winston';
import type { Transport, ConnectionOpts, ShellOptions } from '../types.ts';
import { WSMV, NotImplementedError, WinRMWSManFault, WinRMHTTPTransportError, Shells, PSRP } from '../internal.ts';
import { Document as SlimDocument } from 'slimdom';

export class Base extends Shells.Retryable {
  static close_shell: (connection_opts: ConnectionOpts, transport: Transport, shell_id: string) => Promise<SlimDocument | null>
  static TOO_MANY_COMMANDS = '2150859174'
  static ERROR_OPERATION_ABORTED = '995'
  static SHELL_NOT_FOUND = '2150858843'

  #TOO_MANY_COMMANDS = Base.TOO_MANY_COMMANDS
  #ERROR_OPERATION_ABORTED = Base.ERROR_OPERATION_ABORTED
  #SHELL_NOT_FOUND = Base.SHELL_NOT_FOUND

  #FAULTS_FOR_RESET = [
    this.#SHELL_NOT_FOUND, // Shell has been closed
    '2147943418', // Error reading registry key
    this.#TOO_MANY_COMMANDS, // Maximum commands per user exceeded
  ]
  #shell_id?: string
  #shell_uri?: string
  #connection_opts: ConnectionOpts
  #transport: Transport
  #logger: Logger
  #shell_opts: ShellOptions
  tries?: number
  #ObjectSpace!: FinalizationRegistry<{ connection_opts: ConnectionOpts, transport: Transport, shell_id: string }>
  get shell_id() {
    return this.#shell_id;
  }
  get shell_uri() {
    return this.#shell_uri;
  }
  set shell_uri(value: string | undefined) {
    this.#shell_uri = value;
  }
  get connection_opts() {
    return this.#connection_opts;
  }
  get transport() {
    return this.#transport;
  }
  get logger() {
    return this.#logger;
  }
  get shell_opts() {
    return this.#shell_opts;
  }
  static finalize(heldValue: { connection_opts: ConnectionOpts, transport: Transport, shell_id: string }) {
    return () => {
      void this.close_shell(heldValue.connection_opts, heldValue.transport, heldValue.shell_id);
    }
  }
  // static close_shell(connection_opts: ConnectionOpts, transport: Transport, shell_id: string) {
  //   const msg = new WSMV.CloseShell(connection_opts, { shell_id });
  //   return transport.send_request(msg.build());
  // }
  constructor(opts: ConnectionOpts, transport: Transport, logger: Logger, shell_opts: ShellOptions = {}) {
    super()
    this.#connection_opts = opts;
    this.#transport = transport;
    this.#logger = logger;
    this.#shell_opts = shell_opts;
    // eslint-disable-next-line @typescript-eslint/unbound-method
    this.#ObjectSpace = new FinalizationRegistry((this.constructor as typeof Base).finalize);
  }
  run(command: string, args: string[], block?: (values: string[]) => Promise<void> | void) {
    return this.with_command_shell(command, args, ([shell, cmd]) => {
      return this.response_reader.read_output(this.command_output_message(shell, cmd), block);
    });
    // for await (const values of this.with_command_shell(command, args)) {
    //   block(values);
    // }
  }
  close() {
    if (!this.shell_id) return;
    try {
      return (this.constructor as typeof Base).close_shell(this.connection_opts, this.transport, this.shell_id);
    } catch (e) {
      if (e instanceof WinRMWSManFault) {
        if (![this.#ERROR_OPERATION_ABORTED, this.#SHELL_NOT_FOUND].includes(e.fault_code)) throw e;
      } else {
        throw e;
      }
    } finally {
      this.#remove_finalizer();
      this.#shell_id = undefined;
    }
  }
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  send_command(_command: string, _args: string[]): Promise<string> {
    throw new NotImplementedError('send_command')
  }
  get response_reader(): WSMV.ReceiveResponseReader | PSRP.ReceiveResponseReader {
    throw new NotImplementedError('response_reader')
  }
  // eslint-disable-next-line @typescript-eslint/require-await
  open_shell(): Promise<string> {
    throw new NotImplementedError('open_shell')
  }
  out_streams(): string[] {
    throw new NotImplementedError('out_streams')
  }
  command_output_message(shell_id: string, command_id: string) {
    const cmd_out_opts = {
      shell_id: shell_id,
      command_id: command_id,
      shell_uri: this.shell_uri,
      out_streams: this.out_streams()
    };
    return new WSMV.CommandOutput(this.connection_opts, cmd_out_opts);
  }
  async with_command_shell(command: string, args: string[] = [], block: (values: string[]) => void | Promise<unknown>) {
    this.tries ??= 2;
    this.logger.debug(`[WinRM] Checking for shell_id (${this.shell_id})`);
    if (!this.shell_id) await this.#open();
    this.logger.debug(`[WinRM] Shell ID should be set: ${this.shell_id}`);
    let command_id;
    try {
      command_id = await this.send_command(command, args);
      this.logger.debug(`[WinRM] creating command_id: ${command_id} on shell_id ${this.shell_id}`);
      await block([this.shell_id!, command_id]);
    } catch (e) {
      if (e instanceof WinRMWSManFault) {
        if (this.#FAULTS_FOR_RESET.includes(e.fault_code) && (this.tries -= 1) > 0) {
          await this.#reset_on_error(e);
          await this.with_command_shell(command, args, block);
        } else {
          throw e;
        }
      } else {
        throw e;
      }
    } finally {
      if (command_id) await this.#cleanup_command(command_id)
    }
  }
  async #reset_on_error(error: WinRMWSManFault) {
    if (error.fault_code == this.#TOO_MANY_COMMANDS) await this.close();
    this.logger.debug('[WinRM] opening new shell since the current one was deleted');
    this.#shell_id = undefined;
  }
  #cleanup_command(command_id: string) {
    try {
      this.logger.debug(`[WinRM] cleaning up command_id: ${command_id} on shell_id ${this.shell_id}`);
      const cleanup_msg = new WSMV.CleanupCommand(
        this.connection_opts, {
        shell_uri: this.shell_uri,
        shell_id: this.shell_id!,
        command_id
      });
      return this.transport.send_request(cleanup_msg.build());
    } catch (e) {
      if (e instanceof WinRMWSManFault) {
        if (![this.#ERROR_OPERATION_ABORTED, this.#SHELL_NOT_FOUND].includes(e.fault_code)) throw e;
      } else if (e instanceof WinRMHTTPTransportError) {
        this.logger.info(`[WinRM] ${e.status_code} returned in cleanup with error: ${e.message}`);
      } else {
        throw e;
      }
    }
  }
  async #open() {
    this.logger.debug('[WinRM] Closing previously opened shell if any');
    await this.close();
    this.logger.debug('[WinRM] Trying to open a retryable shell');
    await this.retryable(this.connection_opts.get('retry_limit')!, this.connection_opts.get('retry_delay')!, async () => {
      this.logger.debug(`[WinRM] opening remote shell on ${this.connection_opts.get('endpoint') as string}`)
      this.#shell_id = await this.open_shell();
    });
    this.logger.debug(`[WinRM] remote shell created with shell_id: ${this.shell_id}`);
    this.#add_finalizer();
  }
  #add_finalizer() {
    this.#ObjectSpace.register(
      this,
      { connection_opts: this.connection_opts, transport: this.transport, shell_id: this.shell_id! },
      this
    )
  }
  #remove_finalizer() {
    this.#ObjectSpace.unregister(this);
  }
}