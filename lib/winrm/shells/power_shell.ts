import type { Logger } from "winston";
import type { Transport, ConnectionOpts } from "../types.ts";
import { WSMV, PSRP, WinRMWSManFault, WinRMSoapFault, Shells } from '../internal.ts';
import { randomUUID } from "node:crypto";
import { select1 } from "xpath";

export class Powershell extends Shells.Base {
    runspace_id?: string
    #max_fragment_blob_size?: Promise<number>
    #_fragmenter?: Promise<PSRP.MessageFragmenter>
    #_max_envelope_size_kb?: Promise<number>
    #response_reader?: PSRP.ReceiveResponseReader
    // Finalizer is handled by Base
    static {
        this.close_shell = function (connection_opts, transport, shell_id) {
            const msg = new WSMV.CloseShell(
                connection_opts, {
                shell_id,
                shell_uri: WSMV.Header.RESOURCE_URI_POWERSHELL
            });
            return transport.send_request(msg.build());
        }
    }

    constructor(opts: ConnectionOpts, transport: Transport, logger: Logger) {
        super(opts, transport, logger)
        this.shell_uri = WSMV.Header.RESOURCE_URI_POWERSHELL;
    }
    send_pipeline_command(command: string, block: (message: PSRP.Message) => void) {
        return this.with_command_shell(command, [], ([shell, cmd]) => {
            return this.response_reader.read_message(this.command_output_message(shell, cmd), true, block);
        });
    }
    get max_fragment_blob_size() {
        return this.#max_fragment_blob_size ??= (async () => {
            try {
                const fragment_header_length = 21;
                const max_fragment_bytes = (await this.#max_envelope_size_kb * 1024) - this.#empty_pipeline_envelope().length;
                return this.#base64_deflated(max_fragment_bytes) - fragment_header_length;
            } catch (e) {
                if (e instanceof WinRMWSManFault) {
                    if (e.fault_code != '5') throw e;
                    return PSRP.MessageFragmenter.DEFAULT_BLOB_LENGTH;
                }
                if (e instanceof WinRMSoapFault) {
                    return PSRP.MessageFragmenter.DEFAULT_BLOB_LENGTH;
                }
                throw e;
            }
        })()
    }
    override get response_reader() {
        return this.#response_reader ??= new PSRP.ReceiveResponseReader(this.transport, this.logger);
    }
    override async send_command(command: string): Promise<string> {
        let command_id = randomUUID().toUpperCase();
        command += `\r\nif (!$?) { if($LASTEXITCODE) { exit $LASTEXITCODE } else { exit 1 } }`;
        const message = PSRP.MessageFactory.create_pipeline_message(this.runspace_id!, command_id, command);
        await (await this.#fragmenter).fragment(message, async fragment => {
            const command_args: [ConnectionOpts, string, string, PSRP.Fragment] = [this.connection_opts, this.shell_id!, command_id, fragment];
            if (fragment.start_fragment) {
                const resp_doc = await this.transport.send_request(new WSMV.CreatePipeline(...command_args).build());
                command_id = (select1("//*[local-name() = 'CommandId']", resp_doc! as unknown as Document) as Element).textContent!;
            } else {
                await this.transport.send_request(new WSMV.SendData(...command_args).build())
            }
        });
        this.logger.debug(`[WinRM] Command created for ${command} with id: ${command_id}`);
        return command_id;
    }
    override async open_shell(): Promise<string> {
        this.runspace_id = randomUUID().toUpperCase();
        const runspace_msg = new WSMV.InitRunspacePool(
            this.connection_opts,
            this.runspace_id,
            (await this.#open_shell_payload(this.runspace_id)).flat()
        );
        this.logger.debug(`[WinRM] Sending InitRunspacePool cmd @ ${new Date().toLocaleString()}`);
        const resp_doc = await this.transport.send_request(runspace_msg.build());
        this.logger.debug(`[WinRM] InitRunspacePool cmd response received @ ${new Date().toLocaleString()}`);
        const shell_id = (select1("//*[@Name='ShellId']", resp_doc! as unknown as Document) as Element).textContent!;
        await this.#wait_for_running(shell_id);
        return shell_id;
    }
    override out_streams(): string[] {
        return ['stdout'];
    }
    #base64_deflated(inflated_length: number) {
        return inflated_length / 4 * 3;
    }
    #empty_pipeline_envelope() {
        return new WSMV.CreatePipeline(
            this.connection_opts,
            '00000000-0000-0000-0000-000000000000',
            '00000000-0000-0000-0000-000000000000'
        ).build();
    }
    get #max_envelope_size_kb() {
        return this.#_max_envelope_size_kb ??= (async () => {
            const config_msg = new WSMV.Configuration(this.connection_opts);
            const msg = config_msg.build();
            const resp_doc = await this.transport.send_request(msg);
            return Number((select1("//*[local-name() = 'MaxEnvelopeSizekb']", resp_doc! as unknown as Document) as Element).textContent);
        })();
    }
    #open_shell_payload(shell_id: string): Promise<(number | bigint)[][]> {
        return Promise.all([
            PSRP.MessageFactory.session_capability_message(shell_id),
            PSRP.MessageFactory.init_runspace_pool_message(shell_id)
        ].map(async message => (await (await this.#fragmenter).fragment(message)).bytes()).flat());
    }
    async #wait_for_running(shell_id: string) {
        let state = PSRP.MessageData.RunspacepoolState.OPENING;
        const keepalive_msg = new WSMV.KeepAlive(this.connection_opts, shell_id);
        while (state != PSRP.MessageData.RunspacepoolState.OPENED) {
            await this.response_reader.read_message(keepalive_msg, async message => {
                this.logger.debug(`[WinRM] polling for pipeline state. message: ${JSON.stringify(message)}`)
                const parsed = message.parsed_data;
                if (parsed instanceof PSRP.MessageData.RunspacepoolState) {
                    state = parsed.runspace_state();
                } else if (parsed instanceof PSRP.MessageData.SessionCapability) {
                    if ((await this.#fragmenter).max_blob_length == PSRP.MessageFragmenter.DEFAULT_BLOB_LENGTH) {
                        (await this.#fragmenter).max_blob_length = this.#default_protocol_envelope_size(parsed.protocol_version() || '')
                    }
                }
            });
        }
    }
    #default_protocol_envelope_size(protocol_version: string) {
        return Number(protocol_version) > 2.1 ? 512000 : 153600;
    }
    get #fragmenter() {
        return this.#_fragmenter ??= (async () => new PSRP.MessageFragmenter(await this.max_fragment_blob_size))();
    }
}