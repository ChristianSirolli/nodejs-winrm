// WinRM base class for errors
export class WinRMError extends Error {
    constructor(message: string) {
        super(message);
    }
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    override get name() {
        return this[Symbol.toStringTag]
    }
}
// Authorization Error
export class WinRMAuthorizationError extends WinRMError {
    constructor() {
        super('');
    }
}
// Shell creation error
export class InvalidShellError extends WinRMError {
    constructor(msg: string) {
        super(msg);
    }
}
// Exitcode error
export class InvalidExitCode extends WinRMError {
    constructor() {
        super('');
    }
}
// Shell creation error
export class InvalidTransportError extends WinRMError {
    #invalid_transport: string
    constructor(invalid_transport: string, valid_transports: string[]) {
        super(`Invalid transport ${invalid_transport} specified,\nexpected: ${valid_transports.join(', ')}.`);
        this.#invalid_transport = invalid_transport;
    }
    get invalid_transport() {
        return this.#invalid_transport;
    }
}
// A Fault returned in the SOAP response. The XML node is a WSManFault
export class WinRMWSManFault extends WinRMError {
    #fault_code: string
    #fault_description: string
    constructor(fault_description: string, fault_code: string) {
        super(`[WSMAN ERROR CODE: ${fault_code}]: ${fault_description}`);
        this.#fault_code = fault_code;
        this.#fault_description = fault_description;
    }
    get fault_code() {
        return this.#fault_code;
    }
    get fault_description() {
        return this.#fault_description;
    }
}
// A Fault returned in the SOAP response. The XML node is a MSFT_WmiError
export class WinRMSoapFault extends WinRMError {
    #code: string | number
    #subcode: string | number
    #reason: string
    constructor(code: string | number | null, subcode: string | number | null, reason: string | null) {
        super(`[SOAP ERROR CODE: ${code} (${subcode})]: ${reason}`);
        this.#code = code ?? '';
        this.#subcode = subcode ?? '';
        this.#reason = reason ?? '';
    }
    get code() {
        return this.#code;
    }
    get subcode() {
        return this.#subcode;
    }
    get reason() {
        return this.#reason;
    }
}
// A Fault returned in the SOAP response. The XML node is a MSFT_WmiError
export class WinRMWMIError extends WinRMError {
    #error_code: string | number
    #error: string
    constructor(error: string, error_code: string | number) {
        super(`[WMI ERROR CODE: ${error_code}]: ${error}`);
        this.#error_code = error_code;
        this.#error = error;
    }
    get error_code() {
        return this.#error_code;
    }
    get error() {
        return this.#error;
    }
}
// non-200 response without a SOAP fault
export class WinRMHTTPTransportError extends WinRMError {
    #status_code: number | undefined
    constructor(msg: string, status_code?: number) {
        super(`${msg} (${status_code}).`);
        this.#status_code = status_code;
    }
    get status_code() {
        return this.#status_code
    }
}

export class NotImplementedError extends WinRMError {
    constructor(msg: string) {
        super(`The method ${msg} isn't implemented.`);
    }
}