import type { MessageType } from "../types.ts";
import { PSRP } from "../internal.ts";
import { Buffer } from "node:buffer";

type MessageTypeKeys =
    'session_capability' |
    'init_runspacepool' |
    'public_key' |
    'encrypted_session_key' |
    'public_key_request' |
    'connect_runspacepool' |
    'runspace_init_data' |
    'reset_runspace_state' |
    'set_max_runspaces' |
    'set_min_runspaces' |
    'runspace_availability' |
    'runspacepool_state' |
    'create_pipeline' |
    'get_available_runspaces' |
    'user_event' |
    'application_private_data' |
    'get_command_metadata' |
    'runspacepool_host_call' |
    'runspacepool_host_response' |
    'pipeline_input' |
    'end_of_pipeline_input' |
    'pipeline_output' |
    'error_record' |
    'pipeline_state' |
    'debug_record' |
    'verbose_record' |
    'warning_record' |
    'progress_record' |
    'information_record' |
    'pipeline_host_call' |
    'pipeline_host_response';
export class Message extends PSRP.UUID {
    static CLIENT_DESTINATION = 1
    static SERVER_DESTINATION = 2
    static MESSAGE_TYPES: Record<MessageTypeKeys, MessageType> = {
        session_capability: 0x00010002,
        init_runspacepool: 0x00010004,
        public_key: 0x00010005,
        encrypted_session_key: 0x00010006,
        public_key_request: 0x00010007,
        connect_runspacepool: 0x00010008,
        runspace_init_data: 0x0002100b,
        reset_runspace_state: 0x0002100c,
        set_max_runspaces: 0x00021002,
        set_min_runspaces: 0x00021003,
        runspace_availability: 0x00021004,
        runspacepool_state: 0x00021005,
        create_pipeline: 0x00021006,
        get_available_runspaces: 0x00021007,
        user_event: 0x00021008,
        application_private_data: 0x00021009,
        get_command_metadata: 0x0002100a,
        runspacepool_host_call: 0x00021100,
        runspacepool_host_response: 0x00021101,
        pipeline_input: 0x00041002,
        end_of_pipeline_input: 0x00041003,
        pipeline_output: 0x00041004,
        error_record: 0x00041005,
        pipeline_state: 0x00041006,
        debug_record: 0x00041007,
        verbose_record: 0x00041008,
        warning_record: 0x00041009,
        progress_record: 0x00041010,
        information_record: 0x00041011,
        pipeline_host_call: 0x00041100,
        pipeline_host_response: 0x00041101
    }
    #destination: number
    #type: MessageType
    #runspace_pool_id: string
    #pipeline_id?: string
    #data: string
    #parsed_data?: PSRP.MessageData.Base
    #_data_bytes?: Buffer
    constructor(runspace_pool_id: string, type: MessageType, data: string, pipeline_id?: string, destination = Message.SERVER_DESTINATION) {
        super();
        if (!(Object.values(Message.MESSAGE_TYPES).includes(type))) throw new Error(`invalid message type (${type})`);
        this.#data = data;
        this.#destination = destination;
        this.#type = type;
        this.#pipeline_id = pipeline_id;
        this.#runspace_pool_id = runspace_pool_id;
    }
    get destination() {
        return this.#destination;
    }
    get type() {
        return this.#type;
    }
    get runspace_pool_id() {
        return this.#runspace_pool_id;
    }
    get pipeline_id() {
        return this.#pipeline_id;
    }
    get data() {
        return this.#data;
    }
    bytes() {
        return [
            ...this.#int16le(this.destination),
            ...this.#int16le(this.type),
            ...this.uuid_to_windows_guid_bytes(this.runspace_pool_id),
            ...this.uuid_to_windows_guid_bytes(this.pipeline_id ?? ''),
            ...this.#byte_order_mark(),
            ...this.#data_bytes.toJSON().data
        ]
    }
    get parsed_data() {
        return this.#parsed_data ??= PSRP.MessageData.parse(this);
    }
    #byte_order_mark() {
        return [239, 187, 191];
    }
    get #data_bytes() {
        return this.#_data_bytes ??= Buffer.from(this.data, 'utf-8');
    }
    // int16 to UInt32BE to UInt8(4), reversed
    #int16le(int16: number) {
        const int = Buffer.alloc(4);
        int.writeUint32BE(int16);
        return new Uint8Array(int.toJSON().data).reverse();
    }
}