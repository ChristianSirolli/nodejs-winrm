import { PSRP } from '../internal.ts';

type classes = 'Base' |
    'PipelineOutput' |
    'PipelineHostCall' |
    'RunspacepoolHostCall' |
    'RunspacepoolState' |
    'SessionCapability';

export function parse(message: PSRP.Message) {
    const type_key = Object.entries(PSRP.Message.MESSAGE_TYPES).filter(v => v[1] == message.type)[0][0]
    const type = camelize(type_key.toString());
    // console.log(type);
    if (Object.keys(PSRP.MessageData).includes(type)) return new PSRP.MessageData[type](message.data);
}
export function camelize(underscore: string) {
    return underscore.split('_').map(v => v.toLowerCase().split('').map((v, i) => i == 0 ? v.toUpperCase() : v).join('')).join('') as classes;
}