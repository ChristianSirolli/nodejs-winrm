import { Buffer } from "node:buffer";
import { pack, unpack } from "../helpers.ts";

export class Fragment {
    #object_id: number | bigint
    #fragment_id: number | bigint
    #end_fragment: boolean
    #start_fragment: boolean
    #blob: number[]
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    constructor(object_id: number | bigint, blob: number[], fragment_id: number | bigint = 0, start_fragment = true, end_fragment = true) {
        this.#object_id = object_id;
        this.#blob = blob;
        this.#fragment_id = fragment_id;
        this.#start_fragment = start_fragment;
        this.#end_fragment = end_fragment;
    }
    get object_id() {
        return this.#object_id;
    }
    get fragment_id() {
        return this.#fragment_id;
    }
    get end_fragment() {
        return this.#end_fragment;
    }
    get start_fragment() {
        return this.#start_fragment;
    }
    get blob() {
        return this.#blob;
    }
    bytes() {
        return [
            this.#int64be(this.object_id),
            this.#int64be(this.fragment_id),
            this.#end_start_fragment(),
            ...this.#int16be(this.blob.length),
            ...this.blob
        ].flat();
    }
    #end_start_fragment() {
        let end_start = 0;
        if (this.end_fragment) end_start += 0b10;
        if (this.start_fragment) end_start += 0b1;
        return [end_start];
    }
    // [int64 >> 32, int64 & 4294967295] to UInt32BE(2) to UInt8(4)
    #int64be(int64: number | bigint) {
        return unpack(pack([Number(BigInt(int64) >> 32n), Number(BigInt(int64) & BigInt(0x00000000ffffffff))], 'N2'), 'C8');
    }
    // int16 to UInt32BE to UInt8(4)
    #int16be(int16: number) {
        // const int = new ArrayBuffer(4);
        const int = Buffer.alloc(4);
        int.writeUint32BE(int16);
        return new Uint8Array(int.toJSON().data);
    }
}