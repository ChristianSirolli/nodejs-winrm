export default () => `<obj RefId="0">
  <ms>
    <i32 N="MinRunspaces">1</i32>
    <i32 N="MaxRunspaces">1</i32>
    <obj N="PSThreadOptions" RefId="1">
      <tn RefId="0">
        <t>System.Management.Automation.Runspaces.PSThreadOptions</t>
        <t>System.Enum</t>
        <t>System.ValueType</t>
        <t>System.Object</t>
      </tn>
      <tostring>Default</tostring>
      <i32>0</i32>
    </obj>
    <obj N="ApartmentState" RefId="2">
      <tn RefId="1">
        <t>System.Threading.ApartmentState</t>
        <t>System.Enum</t>
        <t>System.ValueType</t>
        <t>System.Object</t>
      </tn>
      <tostring>Unknown</tostring>
      <i32>2</i32>
    </obj>
    <obj N="ApplicationArguments" RefId="3">
      <tn RefId="2">
        <t>System.Management.Automation.PSPrimitiveDictionary</t>
        <t>System.Collections.Hashtable</t>
        <t>System.Object</t>
      </tn>
      <dct>
        <en>
          <s N="Key">PSVersionTable</s>
          <obj N="Value" RefId="4">
            <tnref RefId="2" />
            <dct>
              <en>
                <s N="Key">PSVersion</s>
                <version N="Value">5.0.11082.1000</version>
              </en>
              <en>
                <s N="Key">PSCompatibleVersions</s>
                <obj N="Value" RefId="5">
                  <tn RefId="3">
                    <t>System.Version[]</t>
                    <t>System.Array</t>
                    <t>System.Object</t>
                  </tn>
                  <lst>
                    <version>1.0</version>
                    <version>2.0</version>
                    <version>3.0</version>
                    <version>4.0</version>
                    <version>5.0.11082.1000</version>
                  </lst>
                </obj>
              </en>
              <en>
                <s N="Key">CLRVersion</s>
                <version N="Value">4.0.30319.42000</version>
              </en>
              <en>
                <s N="Key">BuildVersion</s>
                <version N="Value">10.0.11082.1000</version>
              </en>
              <en>
                <s N="Key">WSManStackVersion</s>
                <version N="Value">3.0</version>
              </en>
              <en>
                <s N="Key">PSRemotingProtocolVersion</s>
                <version N="Value">2.3</version>
              </en>
              <en>
                <s N="Key">SerializationVersion</s>
                <version N="Value">1.1.0.1</version>
              </en>
            </dct>
          </obj>
        </en>
      </dct>
    </obj>
    <obj N="HostInfo" RefId="6">
      <ms>
        <obj N="_hostDefaultData" RefId="7">
          <ms>
            <obj N="data" RefId="8">
              <tn RefId="4">
                <t>System.Collections.Hashtable</t>
                <t>System.Object</t>
              </tn>
              <dct>
                <en>
                  <i32 N="Key">9</i32>
                  <obj N="Value" RefId="9">
                    <ms>
                      <s N="T">System.String</s>
                      <s N="V">C:\\dev\\kitchen-vagrant</s>
                    </ms>
                  </obj>
                </en>
                <en>
                  <i32 N="Key">8</i32>
                  <obj N="Value" RefId="10">
                    <ms>
                      <s N="T">System.Management.Automation.Host.Size</s>
                      <obj N="V" RefId="11">
                        <ms>
                          <i32 N="width">199</i32>
                          <i32 N="height">52</i32>
                        </ms>
                      </obj>
                    </ms>
                  </obj>
                </en>
                <en>
                  <i32 N="Key">7</i32>
                  <obj N="Value" RefId="12">
                    <ms>
                      <s N="T">System.Management.Automation.Host.Size</s>
                      <obj N="V" RefId="13">
                        <ms>
                          <i32 N="width">80</i32>
                          <i32 N="height">52</i32>
                        </ms>
                      </obj>
                    </ms>
                  </obj>
                </en>
                <en>
                  <i32 N="Key">6</i32>
                  <obj N="Value" RefId="14">
                    <ms>
                      <s N="T">System.Management.Automation.Host.Size</s>
                      <obj N="V" RefId="15">
                        <ms>
                          <i32 N="width">80</i32>
                          <i32 N="height">25</i32>
                        </ms>
                      </obj>
                    </ms>
                  </obj>
                </en>
                <en>
                  <i32 N="Key">5</i32>
                  <obj N="Value" RefId="16">
                    <ms>
                      <s N="T">System.Management.Automation.Host.Size</s>
                      <obj N="V" RefId="17">
                        <ms>
                          <i32 N="width">80</i32>
                          <i32 N="height">9999</i32>
                        </ms>
                      </obj>
                    </ms>
                  </obj>
                </en>
                <en>
                  <i32 N="Key">4</i32>
                  <obj N="Value" RefId="18">
                    <ms>
                      <s N="T">System.Int32</s>
                      <i32 N="V">25</i32>
                    </ms>
                  </obj>
                </en>
                <en>
                  <i32 N="Key">3</i32>
                  <obj N="Value" RefId="19">
                    <ms>
                      <s N="T">System.Management.Automation.Host.Coordinates</s>
                      <obj N="V" RefId="20">
                        <ms>
                          <i32 N="x">0</i32>
                          <i32 N="y">9974</i32>
                        </ms>
                      </obj>
                    </ms>
                  </obj>
                </en>
                <en>
                  <i32 N="Key">2</i32>
                  <obj N="Value" RefId="21">
                    <ms>
                      <s N="T">System.Management.Automation.Host.Coordinates</s>
                      <obj N="V" RefId="22">
                        <ms>
                          <i32 N="x">0</i32>
                          <i32 N="y">9998</i32>
                        </ms>
                      </obj>
                    </ms>
                  </obj>
                </en>
                <en>
                  <i32 N="Key">1</i32>
                  <obj N="Value" RefId="23">
                    <ms>
                      <s N="T">System.ConsoleColor</s>
                      <i32 N="V">0</i32>
                    </ms>
                  </obj>
                </en>
                <en>
                  <i32 N="Key">0</i32>
                  <obj N="Value" RefId="24">
                    <ms>
                      <s N="T">System.ConsoleColor</s>
                      <i32 N="V">7</i32>
                    </ms>
                  </obj>
                </en>
              </dct>
            </obj>
          </ms>
        </obj>
        <b N="_isHostNull">false</b>
        <b N="_isHostUINull">false</b>
        <b N="_isHostRawUINull">false</b>
        <b N="_useRunspaceHost">false</b>
      </ms>
    </obj>
  </ms>
</obj>`