import type { Logger } from "winston";
import type { Transport } from "../types.ts";
import { PSRP, WSMV } from "../internal.ts";
export class ReceiveResponseReader extends WSMV.ReceiveResponseReader {
    override output_decoder: PSRP.PowershellOutputDecoder | WSMV.CommandOutputDecoder
    constructor(transport: Transport, logger: Logger) {
        super(transport, logger);
        this.output_decoder = new PSRP.PowershellOutputDecoder();
    }
    async read_message(wsmv_message: WSMV.Base): Promise<PSRP.Message[]>
    async read_message(wsmv_message: WSMV.Base, wait_for_done_state_or_block: boolean): Promise<PSRP.Message[]>
    async read_message(wsmv_message: WSMV.Base, wait_for_done_state_or_block: (message: PSRP.Message) => void | Promise<void>): Promise<undefined>
    async read_message(wsmv_message: WSMV.Base, wait_for_done_state_or_block: boolean, block?: (message: PSRP.Message) => void | Promise<void>): Promise<undefined>
    async read_message(wsmv_message: WSMV.Base, wait_for_done_state_or_block: boolean | ((message: PSRP.Message) => void | Promise<void>) = false, block?: (message: PSRP.Message) => void | Promise<void>): Promise<PSRP.Message[] | undefined> {
        let wait_for_done_state = false;
        if (typeof wait_for_done_state_or_block == 'boolean') {
            wait_for_done_state = wait_for_done_state_or_block;
        } else {
            block = wait_for_done_state_or_block;
        }
        const messages: PSRP.Message[] = [],
            defragmenter = new PSRP.MessageDefragmenter();
        await new Promise(resolve => setTimeout(resolve, 1000));
        await this.read_response(wsmv_message, wait_for_done_state, async ([ stream ]) => {
            const message = await defragmenter.defragment(stream.text);
            if (!message) {
                return;
            }
            if (block) {
                return block(message);
            } else {
                messages.push(message);
            }
        });
        if (!block) return messages;
    }
    override read_output(wsmv_message: WSMV.Base, block?: (out: string[]) => void) {
        return this.with_output(output =>
            this.read_message(wsmv_message, true, message => {
                const exit_code = this.#find_exit_code(message);
                if (exit_code) {
                    output.exitcode = exit_code;
                }
                const decoded_text = (this.output_decoder as PSRP.PowershellOutputDecoder).decode(message);
                const out = { [this.#stream_type(message)]: decoded_text } as { [x in "stdout" | "stderr"]: string };
                output.push(out);
                if (block) block([out.stdout, out.stderr]);
            })
        );
    }
    #stream_type(message: PSRP.Message) {
        let type: 'stdout' | 'stderr' = 'stdout';
        switch (message.type) {
            case PSRP.Message.MESSAGE_TYPES.error_record:
                type = 'stderr'; break;
            case PSRP.Message.MESSAGE_TYPES.pipeline_host_call:
                if (message.data.includes('WriteError')) type = 'stderr';
                break;
        }
        return type;
    }
    #find_exit_code(message: PSRP.Message): number | undefined {
        const parsed = message.parsed_data;
        if (!(parsed instanceof PSRP.MessageData.PipelineHostCall)) return undefined;
        if (parsed.method_identifier() == 'SetShouldExit') return Number(parsed.method_parameters()[0].i32[0]);
    }
}