import { bit, pack, unpack } from "../helpers.ts";
import type { MessageType } from "../types.ts";
import { PSRP } from '../internal.ts';
import { Buffer } from "node:buffer";

export class MessageDefragmenter {
    messages: Record<number | string, PSRP.Fragment[]>
    get [Symbol.toStringTag]() {
        return this.constructor.name;
    }
    constructor() {
        this.messages = {};
    }
    async defragment(base64_bytes: string | Promise<never>) {
        // eslint-disable-next-line @typescript-eslint/await-thenable
        console.log('defragment:', JSON.stringify(base64_bytes), base64_bytes instanceof Promise);
        const fragment = this.fragment_from(Buffer.from(await base64_bytes, 'base64').toString('ascii'));
        this.messages[fragment.object_id.toString()] ??= [];
        this.messages[fragment.object_id.toString()].push(fragment);
        // console.log(fragment.object_id, fragment.end_fragment, fragment.fragment_id, fragment.start_fragment);
        if (fragment.end_fragment) {
            const blob: number[] = [];
            this.messages[fragment.object_id.toString()].forEach((frag: PSRP.Fragment) => blob.push(...frag.blob));
            delete this.messages[fragment.object_id.toString()];
            return this.message_from(pack(blob, 'C*'));
        }
    }
    fragment_from(byte_string: string) {
        console.log('fragment_from:', JSON.stringify(byte_string));
        return new PSRP.Fragment(
            unpack(byte_string.slice(0, 8).split('').reverse().join(''), 'Q').at(0)!, // 64-bit unsigned, native endian (uint64_t)
            Buffer.from(byte_string.slice(21), 'ascii').toJSON().data, // differs from Ruby
            unpack(byte_string.slice(8, 16).split('').reverse().join(''), 'Q').at(0), // 64-bit unsigned, native endian (uint64_t)
            bit(unpack(byte_string[16], 'C').at(0)!, 0) == 1,
            bit(unpack(byte_string[16], 'C').at(0)!, 1) == 1
        );
    }
    message_from(byte_string: string) {
        // console.log(byte_string);
        return new PSRP.Message(
            '00000000-0000-0000-0000-000000000000',
            Number(unpack(byte_string.slice(4, 8), 'V').at(0)) as MessageType,
            byte_string.slice(40),
            '00000000-0000-0000-0000-000000000000',
            Number(unpack(byte_string.slice(0, 4), 'V').at(0)),
        );
    }
}