import create_pipeline from './create_pipeline.xml.ts';
import init_runspace_pool from './init_runspace_pool.xml.ts';
import session_capability from './session_capability.xml.ts';
import { PSRP } from '../internal.ts';

const files = {
    create_pipeline,
    init_runspace_pool,
    session_capability,
}

export class MessageFactory {
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    
    static session_capability_message: (runspace_pool_id: string) => PSRP.Message;
    static init_runspace_pool_message: (runspace_pool_id: string) => PSRP.Message;
    static create_pipeline_message: (runspace_pool_id: string, pipeline_id: string, command: string) => PSRP.Message;
    static #render: (template_name: keyof typeof files, context?: string) => string

    static {
        this.session_capability_message = function (runspace_pool_id) {
            return new PSRP.Message(
                runspace_pool_id,
                PSRP.Message.MESSAGE_TYPES.session_capability,
                this.#render('session_capability')
            );
        };
        this.init_runspace_pool_message = function (runspace_pool_id) {
            return new PSRP.Message(
                runspace_pool_id,
                PSRP.Message.MESSAGE_TYPES.init_runspacepool,
                this.#render('init_runspace_pool')
            );
        };
        this.create_pipeline_message = function (runspace_pool_id, pipeline_id, command) {
            return new PSRP.Message(
                runspace_pool_id,
                PSRP.Message.MESSAGE_TYPES.create_pipeline,
                this.#render('create_pipeline', encodeURIComponent(command)),
                pipeline_id
            )
        }
        this.#render = function (template_name, context) {
            const template = files[template_name];
            return template(context);
        }
    }
}