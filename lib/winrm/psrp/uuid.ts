export class UUID {
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    /**
     * Format a UUID into a GUID compatible byte array for Windows
     *
     * https://msdn.microsoft.com/en-us/library/windows/desktop/aa373931(v=vs.85).aspx
     * typedef struct _GUID {
     *   DWORD Data1;
     *   WORD  Data2;
     *   WORD  Data3;
     *   BYTE  Data4[8];
     * } GUID;
     *
     * @param uuid [String] Canonical hex format with hypens.
     * @return [Array<byte>] UUID in a Windows GUID compatible byte array layout.
     */
    uuid_to_windows_guid_bytes(uuid?: string) {
        if (!uuid) return [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        const b = (uuid.match(/[0-9a-fA-F]{2}/g) as string[]).map(x => Number.parseInt(x, 16));
        return ([] as number[]).concat(b.slice(0, 4).reverse(), b?.slice(4, 6).reverse(), b?.slice(6, 8).reverse(), b?.slice(8))
    }
}