import { create } from 'xmlbuilder2';
import { snakecase, xmlEleParser } from '../../helpers.ts';

export class Base {
    #raw: string
    #clixml?: Record<string, unknown>
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    constructor(data: string) {
        this.#raw = data;
    }
    get raw() {
        return this.#raw;
    }
    get clixml() {
        return this.#clixml ??= (() => {
            const parser = xmlEleParser({
                strip_namespaces: true,
                convert_tags_to: snakecase
            });
            return (create({ parser: { element: parser } }, this.raw).toObject({ verbose: true, group: false }) as { obj: { ms: Record<string, unknown[]>[] }[] }).obj[0].ms[0];
        })();
    }
}