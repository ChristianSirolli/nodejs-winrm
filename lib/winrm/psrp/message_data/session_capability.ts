import { PSRP } from "../../internal.ts";
import { replaceBOM } from "../../helpers.ts";

export class SessionCapability extends PSRP.MessageData.Base {
    constructor(data: string) {
        super(replaceBOM(data));
    }
    protocol_version() {
        return (this.clixml.version as { '@N': string, '#': string }[]).filter(v => v['@N'] == 'protocolversion')[0]['#'];
    }
    ps_version() {
        return (this.clixml.version as { '@N': string, '#': string }[]).filter(v => v['@N'] == 'PSVersion')[0]['#'];
    }
    serialization_version() {
        return (this.clixml.version as { '@N': string, '#': string }[]).filter(v => v['@N'] == 'SerializationVersion')[0]['#'];
    }
}