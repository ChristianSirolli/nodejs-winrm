import { PSRP } from "../../internal.ts";
import { DOMParser, type Document as SlimDocument } from 'slimdom';
import { replaceBOM } from "../../helpers.ts";
import { select1 } from "xpath";

type ExceptionType = Record<keyof { message: string, position_message: string }, string>;
type InvocationInfoType = Record<keyof {
    message: string,
    my_command: string,
    bound_parameters: string,
    unbound_arguments: string,
    script_line_number: string,
    offset_in_line: string,
    history_id: string,
    script_name: string,
    line: string,
    position_message: string,
    ps_script_root: string,
    ps_command_path: string,
    invocation_name: string,
    pipeline_length: string,
    pipeline_position: string,
    expecting_input: string,
    command_origin: string,
    display_script_position: string
}, string>;

export class ErrorRecord extends PSRP.MessageData.Base {
    #exception!: ExceptionType
    #fully_qualified_error_id?: string
    #invocation_info!: InvocationInfoType
    #error_category_message?: string
    #error_details_script_stack_trace?: string
    #doc?: SlimDocument
    constructor(data: string) {
        super(replaceBOM(data));
    }
    get exception() {
        return this.#exception ??= this.property_hash('Exception') as ExceptionType;
    }
    get fully_qualified_error_id() {
        return this.#fully_qualified_error_id ??= this.string_prop('FullyQualifiedErrorId');
    }
    get invocation_info() {
        return this.#invocation_info ??= this.property_hash('InvocationInfo') as InvocationInfoType;
    }
    get error_category_message() {
        return this.#error_category_message ??= this.string_prop('ErrorCategory_Message');
    }
    get error_details_script_stack_trace() {
        return this.#error_details_script_stack_trace ??= this.string_prop('ErrorDetails_ScriptStackTrace');
    }
    get doc() {
        return this.#doc ??= new DOMParser().parseFromString(this.raw, 'application/xml');
    }
    string_prop(prop_name: string): string | undefined {
        const prop = select1(`//*[@N='${prop_name}']`, this.doc as unknown as Document) as Element;
        if (prop) return prop.textContent!;
    }
    property_hash(prop_name: string) {
        const prop_nodes = select1(`//*[@N='${prop_name}']/Props`, this.doc as unknown as Document) as Element;
        if (prop_nodes == null) return {};
        const props: Record<string, string> = {};
        Object.values(prop_nodes.childNodes).filter(node => typeof node != 'string').forEach(node => {
            if (node.nodeType == node.ELEMENT_NODE && (node as Element).attributes) {
                const name = Object.values((node as Element).attributes).filter(n => typeof n != 'string').filter(n => n?.name == 'N')[0]?.value;
                // console.log(this.underscore(name), JSON.stringify(node?.textContent));
                if (node?.textContent && name) props[this.underscore(name)] = node?.textContent;
            }
        });
        return props;
    }
    underscore(camel: string) {
        return camel.replace(/::/g, '/')
            .replace(/([A-Z]+)([A-Z][a-z])/g, '$1_$2')
            .replace(/([a-z\d])([A-Z])/g, '$1_$2')
            .replace(/-/g, '_').toLowerCase();
    }
}