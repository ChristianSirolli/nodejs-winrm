import { replaceBOM } from "../../helpers.ts";
import { PSRP } from "../../internal.ts";
import { DOMParser } from 'slimdom';
import { Buffer } from "node:buffer";

function hex(_match: string, p1: string) {
    return Buffer.from([parseInt(p1, 16)]).toString('utf-8');
}

export class PipelineOutput extends PSRP.MessageData.Base {
    constructor(data: string) {
        super(data);
    }
    output() {
        return this.extract_out_string(this.remove_bom(Buffer.from(this.raw).toString('utf-8')))
    }
    extract_out_string(text: string): string {
        const doc = new DOMParser().parseFromString(text, 'application/xml');
        return Object.values(doc.getElementsByTagName('S')).filter(n => typeof n != 'string').map((node) => {
            let text = '';
            if (node.textContent?.replace(/(\r\n|\r|\n)/g, '')) {
                // console.log(JSON.stringify(node.textContent), arr.length, String(node.textContent), String(node.textContent).replace(/_x([0-9a-fA-F]{4})_/g, hex));
                // text += String(node.textContent).replace(/(_x[0-9a-fA-F]{4}_)+/g, match => {
                //     return Buffer.from(pack(match.match(/_x([0-9a-fA-F]{4})_/g)?.map(v => parseInt(v, 16)) ?? [], 'S*'), 'utf-16le').toString('utf-8');
                // });
                text += String(node.textContent).replace(/_x([0-9a-fA-F]{4})_/g, hex).replace(/(\r\n|\r|\n)$/g, '');
                text += '\r\n';
            }
            return text;
        }).join('');
    }
    remove_bom(text: string) {
        return replaceBOM(text)
    }
}