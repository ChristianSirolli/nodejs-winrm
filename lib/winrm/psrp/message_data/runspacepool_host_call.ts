import { PSRP } from "../../internal.ts";
import { replaceBOM } from "../../helpers.ts";

export class RunspacepoolHostCall extends PSRP.MessageData.Base {
    constructor(data: string) {
        super(replaceBOM(data));
    }
    method_identifier(): string {
        return (this.clixml.obj as { to_string: string[] }[])[0].to_string[0] ?? '';
    }
    method_parameters() {
        return (this.clixml.obj as { lst: { s: { '#': string | null }[], i32: { '#': string | null }[] }[] }[])[1].lst;
    }
}