import { PSRP } from "../../internal.ts";
import { ErrorRecord } from "./error_record.ts";
import { replaceBOM } from "../../helpers.ts";

export class PipelineState extends PSRP.MessageData.Base {
    static NOT_STARTED = 0
    static RUNNING = 1
    static STOPPING = 2
    static STOPPED = 3
    static COMPLETED = 4
    static FAILED = 5
    static DISCONNECTED = 6
    #exception_as_error_record?: PSRP.MessageData.ErrorRecord
    constructor(data: string) {
        // console.log('PipelineState:', JSON.stringify(data), data.match(Buffer.from("\xEF\xBB\xBF", 'ascii').toString('ascii')));
        super(replaceBOM(data));
    }
    pipeline_state() {
        return parseInt((this.clixml.i32 as { '#': string }[])[0]['#']);
    }
    get exception_as_error_record() {
        if (this.pipeline_state() == PipelineState.FAILED) return this.#exception_as_error_record ??= new ErrorRecord(this.raw);
        return undefined;
    }

}