import { PSRP } from "../../internal.ts";
import { replaceBOM } from "../../helpers.ts";

export class RunspacepoolState extends PSRP.MessageData.Base {
    static BEFORE_OPEN = 0
    static OPENING = 1
    static OPENED = 2
    static CLOSED = 3
    static CLOSING = 4
    static BROKEN = 5
    static NEGOTIATION_SENT = 6
    static NEGOTIATION_SUCCEEDED = 7
    static CONNECTING = 8
    static DISCONNECTED = 9
    constructor(data: string) {
        super(replaceBOM(data));
    }
    runspace_state() {
        return parseInt((this.clixml.i32 as { '#': string }[])[0]['#']);
    }
}