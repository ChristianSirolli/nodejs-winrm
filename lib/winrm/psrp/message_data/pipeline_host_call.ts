import { replaceBOM } from "../../helpers.ts";
import { PSRP } from "../../internal.ts";

export class PipelineHostCall extends PSRP.MessageData.Base {
    constructor(data: string) {
        super(replaceBOM(data));
    }
    method_identifier(): string {
        return (this.clixml.obj as { to_string: string[] }[])[0].to_string[0] ?? '';
    }
    method_parameters() {
        return (this.clixml.obj as { lst: { s: string[], i32: string[] }[] }[])[1].lst;
    }
}