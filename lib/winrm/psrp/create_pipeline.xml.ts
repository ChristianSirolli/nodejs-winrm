export default (command?: string) => `<obj RefId="0">
  <ms>
    <obj N="PowerShell" RefId="1">
      <ms>
        <obj N="Cmds" RefId="2">
          <tn RefId="0">
            <t>System.Collections.Generic.List\`1[[System.Management.Automation.PSObject, System.Management.Automation, Version=3.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35]]</t>
            <t>System.Object</t>
          </tn>
          <lst>
            <obj RefId="3">
              <ms>
                <s N="Cmd">Invoke-expression</s>
                <b N="IsScript">false</b>
                <nil N="UseLocalScope" />
                <obj N="MergeMyResult" RefId="4">
                  <tn RefId="1">
                    <t>System.Management.Automation.Runspaces.PipelineResultTypes</t>
                    <t>System.Enum</t>
                    <t>System.ValueType</t>
                    <t>System.Object</t>
                  </tn>
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="MergeToResult" RefId="5">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="MergePreviousResults" RefId="6">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="MergeError" RefId="7">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="MergeWarning" RefId="8">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="MergeVerbose" RefId="9">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="MergeDebug" RefId="10">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="Args" RefId="11">
                  <tnref RefId="0" />
                  <lst>
                    <obj RefId="12">
                      <ms>
                        <s N="N">-Command</s>
                        <nil N="V" />
                      </ms>
                    </obj>
                    <obj RefId="13">
                      <ms>
                        <nil N="N" />
                        <s N="V">${command}</s>
                      </ms>
                    </obj>
                  </lst>
                </obj>
              </ms>
            </obj>
            <obj RefId="14">
              <ms>
                <s N="Cmd">Out-string</s>
                <b N="IsScript">false</b>
                <nil N="UseLocalScope" />
                <obj N="MergeMyResult" RefId="15">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="MergeToResult" RefId="16">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="MergePreviousResults" RefId="17">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="MergeError" RefId="18">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="MergeWarning" RefId="19">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="MergeVerbose" RefId="20">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="MergeDebug" RefId="21">
                  <tnref RefId="1" />
                  <tostring>None</tostring>
                  <i32>0</i32>
                </obj>
                <obj N="Args" RefId="22">
                  <tnref RefId="0" />
                  <lst>
                    <obj RefId="22">
                      <ms>
                        <s N="N">-Stream</s>
                        <nil N="V" />
                      </ms>
                    </obj>
                  </lst>
                </obj>
              </ms>
            </obj>
          </lst>
        </obj>
        <b N="IsNested">false</b>
        <nil N="History" />
        <b N="RedirectShellErrorOutputPipe">true</b>
      </ms>
    </obj>
    <b N="NoInput">true</b>
    <obj N="ApartmentState" RefId="23">
      <tn RefId="2">
        <t>System.Threading.ApartmentState</t>
        <t>System.Enum</t>
        <t>System.ValueType</t>
        <t>System.Object</t>
      </tn>
      <tostring>Unknown</tostring>
      <i32>2</i32>
    </obj>
    <obj N="RemoteStreamOptions" RefId="24">
      <tn RefId="3">
        <t>System.Management.Automation.RemoteStreamOptions</t>
        <t>System.Enum</t>
        <t>System.ValueType</t>
        <t>System.Object</t>
      </tn>
      <tostring>0</tostring>
      <i32>0</i32>
    </obj>
    <b N="AddToHistory">true</b>
    <obj N="HostInfo" RefId="25">
      <ms>
        <b N="_isHostNull">true</b>
        <b N="_isHostUINull">true</b>
        <b N="_isHostRawUINull">true</b>
        <b N="_useRunspaceHost">true</b>
      </ms>
    </obj>
    <b N="IsNested">false</b>
  </ms>
</obj>`;