import { PSRP } from "../internal.ts";

export class MessageFragmenter {
    static DEFAULT_BLOB_LENGTH = 32768;
    #object_id!: number;
    #max_blob_length!: number;
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }

    constructor(max_blob_length = MessageFragmenter.DEFAULT_BLOB_LENGTH) {
        this.#object_id = 0;
        this.max_blob_length = max_blob_length;
    }

    get object_id() {
        return this.#object_id;
    }

    get max_blob_length() {
        return this.#max_blob_length
    }

    set max_blob_length(value: number) {
        this.#max_blob_length = value;
    }

    async fragment(message: PSRP.Message, block?: (fragment: PSRP.Fragment) => Promise<void>) {
        this.#object_id += 1;
        const message_bytes = message.bytes();
        let bytes_fragmented = 0,
            fragment_id = 0,
            fragment: PSRP.Fragment;
        while (bytes_fragmented < message_bytes.length) {
            let last_byte = bytes_fragmented + this.max_blob_length;
            if (last_byte > message_bytes.length) last_byte = message_bytes.length;
            fragment = new PSRP.Fragment(
                this.object_id,
                message_bytes.slice(bytes_fragmented, last_byte),
                fragment_id,
                bytes_fragmented == 0,
                last_byte == message_bytes.length
            )
            fragment_id += 1;
            bytes_fragmented = last_byte;
            if (block) await block(fragment);
        }
        return fragment!;
        // return fragment;
    }
}