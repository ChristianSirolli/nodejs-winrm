import { Buffer } from "node:buffer";
import { PSRP, WSMV } from '../internal.ts';

export class PowershellOutputDecoder extends WSMV.CommandOutputDecoder {
    constructor() {
        super();
    }
    override decode(message: PSRP.Message) {
        // console.log(message.type, Object.keys(PSRP.Message.MESSAGE_TYPES).at(Object.values(PSRP.Message.MESSAGE_TYPES).indexOf(message.type)))
        switch (message.type) {
            case PSRP.Message.MESSAGE_TYPES.pipeline_output:
                return this.decode_pipeline_output(message);
            case PSRP.Message.MESSAGE_TYPES.runspacepool_host_call:
                return this.decode_host_call(message);
            case PSRP.Message.MESSAGE_TYPES.pipeline_host_call:
                return this.decode_host_call(message);
            case PSRP.Message.MESSAGE_TYPES.error_record:
                return this.decode_error_record(message);
            case PSRP.Message.MESSAGE_TYPES.pipeline_state:
                if ((message.parsed_data as PSRP.MessageData.PipelineState)?.pipeline_state && (message.parsed_data as PSRP.MessageData.PipelineState)?.pipeline_state() == PSRP.MessageData.PipelineState.FAILED) {
                    return this.decode_error_record(message);
                }
                return;
            default:
                return;
        }
    }
    decode_pipeline_output(message: PSRP.Message) {
        return (message.parsed_data as PSRP.MessageData.PipelineOutput).output();
    }
    decode_host_call(message: PSRP.Message) {
        const text = (() => {
            const str = (message.parsed_data as PSRP.MessageData.PipelineHostCall).method_identifier();
            // console.log(JSON.stringify(str));
            switch (true) {
                case /WriteLine/.test(str):
                case str == 'WriteErrorLine':
                    // console.log(JSON.stringify((message.parsed_data as PSRP.MessageData.PipelineHostCall).method_parameters()));
                    return `${(message.parsed_data as PSRP.MessageData.PipelineHostCall).method_parameters()[0].s[0] ?? ''}\r\n`;
                case str == 'WriteDebugLine':
                    return `Debug: ${(message.parsed_data as PSRP.MessageData.PipelineHostCall).method_parameters()[0].s[0] ?? ''}\r\n`;
                case str == 'WriteWarningLine':
                    return `Warning: ${(message.parsed_data as PSRP.MessageData.PipelineHostCall).method_parameters()[0].s[0] ?? ''}\r\n`;
                case str == 'WriteVerboseLine':
                    return `Verbose: ${(message.parsed_data as PSRP.MessageData.PipelineHostCall).method_parameters()[0].s[0] ?? ''}\r\n`;
                case /Write[12]/.test(str):
                case str == 'Write2':
                    return (message.parsed_data as PSRP.MessageData.PipelineHostCall).method_parameters()[0].s[0] ?? '';
                default: return '';
            }
        })();
        // console.log('decode_host_call:', JSON.stringify(this.#hex_decode(text)));
        return this.#hex_decode(text);
    }
    decode_error_record(message: PSRP.Message) {
        const parsed = message.parsed_data!;
        const text = (() => {
            if (message.type == PSRP.Message.MESSAGE_TYPES.pipeline_state) {
                return this.render_exception_as_error_record((parsed as PSRP.MessageData.PipelineState).exception_as_error_record!)
            }
            switch ((parsed as PSRP.MessageData.ErrorRecord).fully_qualified_error_id) {
                case 'Microsoft.PowerShell.Commands.WriteErrorException':
                    return this.render_write_error_exception((parsed as PSRP.MessageData.ErrorRecord));
                case 'NativeCommandError':
                    return this.render_native_command_error((parsed as PSRP.MessageData.ErrorRecord));
                case 'NativeCommandErrorMessage':
                    return (parsed as PSRP.MessageData.ErrorRecord).exception.message;
                default:
                    return this.render_exception((parsed as PSRP.MessageData.ErrorRecord));
            }
        })();
        return this.#hex_decode(text);
    }
    render_write_error_exception(parsed: PSRP.MessageData.ErrorRecord) {
        return `${parsed.invocation_info.line} : ${parsed.exception.message}
        + CategoryInfo          : ${parsed.error_category_message}
        + FullyQualifiedErrorID : ${parsed.fully_qualified_error_id}`;
    }
    render_exception(parsed: PSRP.MessageData.ErrorRecord) {
        return `${parsed.invocation_info.message}
${parsed.exception.position_message}
        + CategoryInfo          : ${parsed.error_category_message}
        + FullyQualifiedErrorID : ${parsed.fully_qualified_error_id}`;
    }
    render_native_command_error(parsed: PSRP.MessageData.ErrorRecord) {
        return `${parsed.invocation_info.my_command} : ${parsed.exception.message}
        + CategoryInfo          : ${parsed.error_category_message}
        + FullyQualifiedErrorID : ${parsed.fully_qualified_error_id}`;
    }
    render_exception_as_error_record(parsed: PSRP.MessageData.ErrorRecord) {
        return `${parsed.exception.message}
        + CatgeryInfo           : ${parsed.error_category_message}
        + FullyQualifiedErrorID : ${parsed.fully_qualified_error_id}`
    }
    #hex_decode(text: string) {
        if (!text) return;
        return text.replace(/_x([0-9a-fA-F]{4})_/g, (_match, $1: string) => Buffer.from([parseInt($1, 16)]).toString('ascii'));
    }
}