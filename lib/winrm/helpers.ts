import { endianness } from 'node:os';
import { type Decipher, createHmac, randomBytes, createDecipheriv, createHash, createCipheriv, type Cipher, CipherKey } from 'node:crypto';
import ntlm, { type Type2Message } from '@ewsjs/ntlm-client';
import flags from '@ewsjs/ntlm-client/lib/flags.js';
import type { ConnectionOpts, /*Constructor, Mixin*/ } from './types.ts';
// import type { AxiosInstance } from 'feaxios';
import type { XMLBuilder, XMLSerializedAsObject, XMLSerializedAsObjectArray } from 'xmlbuilder2/lib/interfaces.ts';
// import type { DOMParserOptions } from "@xmldom/xmldom";
import { Buffer } from "node:buffer";
import { URL } from "node:url";
import { Agent as HTTPSAgent, type RequestOptions, request as HTTPSRequest } from "node:https";
import { type OutgoingHttpHeaders, type OutgoingHttpHeader, request as HTTPRequest, Agent as HTTPAgent, type IncomingMessage, ClientRequest } from "node:http";
// import type { C } from "../../node_modules/feaxios/dist/client-DGpL0cYy.d.ts";
import type { IncomingHttpHeaders } from "node:http";

export function replaceBOM(data: string) {
    return data
        .replace("\xEF\xBB\xBF", '')
        .replace(Buffer.from("\xEF\xBB\xBF", 'ascii').toString('ascii'), '');
}
export function pack(value: number[], formatStr: string) {
    const nativeEndian = endianness();
    let result = '';
    const formats = formatStr.replace(/ /g, '').split(/([A-JL-NPQSUVXZa-jl-npqsuvwx@](?:_|!?(?:>|<)?)?(?:\d+|\*)?)/).filter(v => v);
    let start = 0;
    formats.forEach((format, i) => {
        if ((format.includes('<') || format.includes('>')) && !'sSiIlLqQjJ'.includes(format[0])) {
            throw new Error(`ArgumentError: '${format.match(/(<|>)/)?.at(0)}' allowed only after types sSiIlLqQjJ in '${format}'`);
        }
        let endian = nativeEndian;
        if (format.includes('<')) endian = 'LE';
        if (format.includes('>')) endian = 'BE';
        const numMatch = format.match(/(\d+|\*)$/)?.at(1);
        let valLen = Number(numMatch)
        if (numMatch == undefined) valLen = 1;
        if (numMatch == '*') valLen = value.length - i;
        switch (format[0]) {
            case 'C': {
                const byteLen = 1;
                const valBuf = Buffer.alloc(byteLen * valLen);
                const work = value.slice(start, valLen);
                if (work.map((v => typeof v)).includes('string')) throw new Error(`TypeError: no implicit conversion of String into Number`);
                work.forEach((v, i) => start = valBuf.writeUInt8(v, i * byteLen));
                result += valBuf.toString('ascii');
                break;
            }
            case 'S': {
                const byteLen = 4;
                const valBuf = Buffer.alloc(byteLen * valLen);
                const work = value.slice(start, valLen);
                if (work.map((v => typeof v)).includes('string')) throw new Error(`TypeError: no implicit conversion of String into Number`);
                work.forEach((v, i) => start = valBuf.writeUInt8(v, i * byteLen));
                result += valBuf.toString('ascii');
                break;
            }
            case 'I': {
                const byteLen = 4;
                const valBuf = Buffer.alloc(byteLen * valLen);
                const work = value.slice(start, valLen);
                if (work.map((v => typeof v)).includes('string')) throw new Error(`TypeError: no implicit conversion of String into Number`);
                work.forEach((v, i) => start = valBuf[`writeUInt${endian}`](v, i * byteLen, byteLen) - byteLen + 1);
                result += valBuf.toString('ascii');
                break;
            }
            case 'N': {
                const byteLen = 4;
                const valBuf = Buffer.alloc(byteLen * valLen);
                const work = value.slice(start, valLen);
                if (work.map((v => typeof v)).includes('string')) throw new Error(`TypeError: no implicit conversion of String into Number`);
                work.forEach((v, i) => start = valBuf.writeUInt32BE(v, i * byteLen) - byteLen + 1);
                result += valBuf.toString('ascii');
                break;
            }
            case 'V': {
                const byteLen = 4;
                const valBuf = Buffer.alloc(byteLen * valLen);
                const work = value.slice(start, valLen);
                if (work.map((v => typeof v)).includes('string')) throw new Error(`TypeError: no implicit conversion of String into Number`);
                work.forEach((v, i) => start = valBuf.writeUInt32LE(v, i * byteLen) - byteLen + 1);
                result += valBuf.toString('ascii');
                break;
            }
            default: throw new Error(`ArgumentError: unknown directive '${format[0]}' in '${format}'`);
        }
    });
    return result;
}
export function unpack(value: string, formatStr: 'Q'): bigint[]
export function unpack(value: string, formatStr: 'C8' | 'C' | 'V'): number[]
export function unpack(value: string, formatStr: 'C8' | 'Q' | 'C' | 'V' /*| 'L' | `La${number}a*`*/): (number | bigint)[] {
    let result: (number | bigint)[] = [];
    switch (formatStr) {
        case 'C8': {
            const valArr = Buffer.alloc(8);
            valArr.write(value, 'ascii');
            result = [...valArr.toJSON().data];
            break;
        }
        case 'Q': {
            // console.log('unpack Q', JSON.stringify(value), typeof value);
            const valArr = Buffer.alloc(8);
            valArr.write(value, 'ascii');
            result = [valArr[`readBigUInt64${endianness()}`]()];
            break;
        }
        case 'C': {
            // console.log('unpack C', JSON.stringify(value), typeof value);
            const valArr = Buffer.alloc(1);
            valArr.write(value, 'ascii');
            result = [...valArr.toJSON().data];
            break;
        }
        case 'V': {
            const valArr = Buffer.alloc(8);
            valArr.write(value, 'ascii');
            result = [valArr[`readUInt32${endianness()}`]()];
            break;
        }
        // case 'L': {
        //     let valArr = Buffer.alloc(4);
        //     valArr.write(value, 'binary');
        //     result = [valArr[`readUInt32${endianness()}`]()];
        //     break;
        // }
        // default: {
        //     if (formatStr.match(/La\d+a\*/)) {
        //         let Lval = Buffer.alloc(4);
        //         Lval.write(value, 'binary');
        //         let ad = Number(formatStr.match(/\d+/)?.at(0));
        //         let valBuf = Buffer.from(value, 'binary');
        //         result = [Lval[`readUInt32${endianness()}`](), valBuf.subarray(4, ad + 4).toString(), valBuf.subarray(ad + 4).toString()];
        //     } else {
        //         throw 'Format string not supported'
        //     }
        // }
    }
    return result;
}

export function bit(val: string | number, i: number) {
    return Number(Number(val).toString(2).padStart(2, '0').split('').reverse()[i]) as 0 | 1;
}

export const snakecase = (str: string) => str.replace(/([a-z])([A-Z])/g, '$1_$2').toLowerCase();

export function convertToSnakecase(obj: Record<string, unknown> | XMLSerializedAsObject | XMLSerializedAsObjectArray) {
    const newObj: Record<string, unknown> = {};
    Object.keys(obj).forEach(k => {
        const value = obj as Record<string, unknown>,
            meta = Object.getOwnPropertyNames(value);
        newObj[snakecase(k)] = (meta.length == 0 || meta.includes('length')) ? value : convertToSnakecase(value);
    });
    return newObj;
}

export function xmlEleParser(options?: {
    strip_namespaces?: boolean
    convert_tags_to?: (tag: string) => string
}) {
    const opts = {
        strip_namespaces: false,
        convert_tags_to: (tag: string) => tag,
        ...options
    }
    return (parent: XMLBuilder, namespace: string | null | undefined, name: string) => parent.ele((!opts.strip_namespaces && namespace) ? namespace : null, opts.convert_tags_to(opts.strip_namespaces ? name.split(':')[1] || name : name))
}

// export function parserErrorHandler(): DOMParserOptions {
//     return {
//         errorHandler: (level: 'error' | 'fatalError' | 'warning', msg: string) => {
//             if (level == 'error' || level == 'fatalError') throw new Error(msg)
//         }
//     }
// }

// export const xmlEleParser = (parent: XMLBuilder, _namespace: string | null | undefined, name: string) => parent.ele(null, snakecase(name));

// https://github.com/WinRb/rubyntlm/blob/master/lib/net/ntlm/client/session.rb
class NTLMSession {
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    client: NTLMClient
    challenge_message: Buffer
    // channel_binding?: NTLMChannelBinding
    VERSION_MAGIC = "\x01\x00\x00\x00"
    // TIME_OFFSET = 11644473600
    // MAX64 = 0xffffffffffffffffn
    CLIENT_TO_SERVER_SIGNING = "session key to client-to-server signing key magic constant\0"
    SERVER_TO_CLIENT_SIGNING = "session key to server-to-client signing key magic constant\0"
    CLIENT_TO_SERVER_SEALING = "session key to client-to-server sealing key magic constant\0"
    SERVER_TO_CLIENT_SEALING = "session key to server-to-client sealing key magic constant\0"
    server_cipher?: Decipher
    client_cipher?: Cipher
    // client_challenge?: unknown
    // server_challenge?: unknown
    // timestamp?: number
    type2message?: Type2Message
    // user_session_key?: string
    client_sign_key?: string
    server_sign_key?: string
    client_seal_key?: string
    server_seal_key?: string
    raw_sequence?: number
    // ntlmv2_hash?: unknown
    // nt_proof_str?: string
    // target_info?: unknown
    // blob?: unknown
    constructor(client: NTLMClient, challenge_message: Buffer) {//, channel_binding?: NTLMChannelBinding) {
        this.client = client
        this.challenge_message = challenge_message

        // this.channel_binding = channel_binding
    }

    authenticate() {
        // this.#calculate_user_session_key();
        // let type3_opts = {
        //     lm_response: this.lmv2_resp,
        //     ntlm_response: this.ntlm_resp,
        //     domain: this.client.domain,
        //     user: this.client.username,
        //     workstation: this.client.workstation,
        // }
        this.type2message = ntlm.decodeType2Message(this.challenge_message.toString());
        return ntlm.createType3Message(this.type2message, this.#username(), this.#password(), this.#workstation(), this.#domain())
    }
    exported_session_key() {
        if (this.#negotiate_key_exchange()) {
            return createDecipheriv('rc4', this.#client_seal_key(), randomBytes(16)).final('utf8').toString()
        }
    }
    sign_message(message: string): string {
        const seq = this.#sequence();
        let sig = createHmac('md5', this.#client_sign_key()).update(`${seq}${message}`).digest().subarray(0, 8);
        if (this.#negotiate_key_exchange()) {
            sig = this.#server_cipher().update(sig);
            sig = Buffer.concat([new Uint8Array(sig.toJSON().data), new Uint8Array(this.#server_cipher().final().toJSON().data)]);
        }
        return `${this.VERSION_MAGIC}${sig.toString()}${seq}`;
    }
    verify_signature(signature: string, message?: string): boolean {
        const seq = this.#sequence();
        let sig = createHmac('md5', this.#server_sign_key()).update(`${seq}${message}`).digest().subarray(0, 8);
        if (this.#negotiate_key_exchange()) {
            sig = this.#server_cipher().update(sig);
            sig = Buffer.concat([new Uint8Array(sig.toJSON().data), new Uint8Array(this.#server_cipher().final().toJSON().data)]);
        }
        return `${this.VERSION_MAGIC}${sig.toString()}${seq}` == signature;
    }
    seal_message(message: string): string {
        const emessage = this.#client_cipher().update(message, 'binary', 'utf8');
        return emessage + this.#client_cipher().final('utf8').toString();
    }
    unseal_message(emessage: string): string {
        const message = this.#server_cipher().update(emessage, 'binary', 'utf8');
        return message + this.#server_cipher().final('utf8').toString();
    }
    // #user_session_key() {
    //     return this.user_session_key ??= undefined;
    // }
    #sequence(): string {
        return pack([this.#raw_sequence()], 'V*')
    }
    #raw_sequence() {
        if (this.raw_sequence !== undefined) {
            this.raw_sequence += 1;
        } else {
            this.raw_sequence = 0;
        }
        return this.raw_sequence;
    }
    #client_sign_key() {
        return this.client_sign_key ??= createHash('md5').update(`${this.exported_session_key()}${this.CLIENT_TO_SERVER_SIGNING}`).digest('hex');
    }
    #server_sign_key() {
        return this.server_sign_key ??= createHash('md5').update(`${this.exported_session_key()}${this.SERVER_TO_CLIENT_SIGNING}`).digest('hex');
    }
    #client_seal_key(): CipherKey {
        return this.client_seal_key ??= createHash('md5').update(`${this.exported_session_key()}${this.CLIENT_TO_SERVER_SEALING}`).digest('hex');
    }
    #server_seal_key() {
        return this.server_seal_key ??= createHash('md5').update(`${this.exported_session_key()}${this.SERVER_TO_CLIENT_SEALING}`).digest('hex');
    }
    #client_cipher() {
        // https://stackoverflow.com/a/13060315/5530509
        return this.client_cipher ??= createCipheriv('rc4', this.#client_seal_key(), randomBytes(16));
    }
    #server_cipher() {
        // https://stackoverflow.com/a/13060315/5530509
        return this.server_cipher ??= createDecipheriv('rc4', this.#server_seal_key(), randomBytes(16));
    }
    // #client_challenge() {
    //     return this.client_challenge ??= ;
    // }
    // #server_challenge() {
    //     return this.server_challenge ??= ;
    // }
    // #timestamp() {
    //     this.timestamp ??= 10000000 * (new Date().valueOf() / 1000 + this.TIME_OFFSET)
    // }
    #use_oem_strings(): boolean {
        return this.type2message?.encoding == 'ascii';
    }
    #negotiate_key_exchange(): number {
        // eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
        return (this.type2message?.flags ?? 0) & flags.NTLMFLAG_NEGOTIATE_KEY_EXCHANGE;
    }
    #username() {
        return this.#oem_or_unicode_str(this.client.username);
    }
    #password() {
        return this.#oem_or_unicode_str(this.client.password);
    }
    #workstation() {
        if (this.client.workstation) return this.#oem_or_unicode_str(this.client.workstation);
        return '';
    }
    #domain() {
        if (this.client.domain) return this.#oem_or_unicode_str(this.client.domain);
        return '';
    }
    #oem_or_unicode_str(str: string) {
        if (this.#use_oem_strings()) {
            return Buffer.from(str, 'ucs2').toString('ascii')
        } else {
            return Buffer.from(str, 'ascii').toString('ucs2')
        }
    }
    // #ntlmv2_hash() {
    //     if (!this.ntlmv2_hash) {}
    //     return this.ntlmv2_hash;
    // }
    // #calculate_user_session_key() {
    //     this.user_session_key = createHmac('md5', this.#ntlmv2_hash() as string | Buffer).update(this.#nt_proof_str() as string).digest().subarray(0, 8).toString() + this.#client_challenge();
    // }
    // #lmv2_resp() {
    //     return this.#nt_proof_str() + this.#blob();
    // }
    // #nt_proof_str() {
    //     if (!this.nt_proof_str) {}
    //     return this.nt_proof_str;
    // }
    // #blob() {
    //     if (!this.blob) {
    //         let b = new Buffer({
    //             timestamp: this.#timestamp(),
    //             challenge: this.#client_challenge(),
    //             target_info: this.#target_info(),
    //         });
    //     }
    //     return this.blob;
    // }
    // #target_info() {
    //     if (!this.target_info) {
    //         if (this.channel_binding) {}
    //     }
    //     return this.target_info;
    // }
}

// https://github.com/WinRb/rubyntlm/blob/master/lib/net/ntlm/channel_binding.rb
// export class NTLMChannelBinding {
////     channel: PeerCertificate
//     unique_prefix: string
//     initiator_addtype: number
//     initiator_address_length: number
//     acceptor_addrtype: number
//     acceptor_address_length: number
//     constructor(outer_channel: PeerCertificate) {
//         this.channel = outer_channel;
//         this.unique_prefix = 'tls-server-end-point';
//         this.initiator_addtype = 0
//         this.initiator_address_length = 0
//         this.acceptor_addrtype = 0
//         this.acceptor_address_length = 0
//     }
//     channel_binding_token() { }
//     gss_channel_bindings_struct() {
//         let token = pack([this.initiator_addtype], 'I');
//         token += pack([this.initiator_address_length], 'I');
//         token += pack([this.acceptor_addrtype], 'I');
//         token += pack([this.acceptor_address_length], 'I');
//         token += pack([this.application_data()], 'I');
//         token += this.application_data().length;
//         return token;
//     }
//     channel_hash() {
//         return this.channel.raw
//     }
//     application_data() {
//         let data = this.unique_prefix;
//         data += ':';
//         data += this.channel_hash().toString('binary');
//         return data;
//     }
// }

// https://github.com/WinRb/rubyntlm/blob/master/lib/net/ntlm/client.rb
export class NTLMClient {
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    client: HTTPClient
    username: string
    password: string
    session?: NTLMSession
    domain?: string
    workstation?: string
    opts: ConnectionOpts
    constructor(client: HTTPClient, username: string, password: string, opts: ConnectionOpts) {
        this.client = client;
        this.username = username;
        this.password = password;
        this.domain = opts.get('domain')!;
        this.opts = opts;
        this.workstation = opts.get('workstation')!;
    }
    init_context(resp?: string/*, channel_binding?: any*/): `NTLM ${string}` {
        if (!resp) {
            this.session = undefined;
            return this.#type1_message();
        } else {
            this.session = new NTLMSession(this, Buffer.from(resp, 'base64'));//, channel_binding);
            return this.session.authenticate();
        }
    }
    session_key() {
        if (this.session) return this.session.exported_session_key()
    }
    #type1_message() {
        return ntlm.createType1Message(this.workstation, this.domain)
    }
}

// const creator = (prev: Constructor, curr: Mixin) => curr(prev);
// export const extender = (...parts: Mixin[]) => parts.reduce(creator, Root);

export function assert(condition: unknown, message?: string, name = 'AssertionError') {
    if (!condition) {
        const AssertionError = Error;
        AssertionError.prototype.name = name;
        const e = new AssertionError(message ?? 'Assertion failed');
        // e.name = name;
        throw e;
    }
}

type HTTPOptions = Omit<RequestOptions, 'headers'> & {
    headers?: HTTPHeaders | OutgoingHttpHeaders,
    body?: string | Buffer | Uint8Array | object
}

class HTTPHeaders {
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }

    *[Symbol.iterator]() {
        for (const v of Object.entries(this.headers)) yield v;
    }

    headers: OutgoingHttpHeaders = {};

    constructor(init?: OutgoingHttpHeaders) {
        this.headers = init ?? this.headers;
    }

    append(key: keyof OutgoingHttpHeaders, value: OutgoingHttpHeader) {
        let tempVal: string[];
        if (value instanceof Array) {
            tempVal = value
        } else if (typeof value == 'string') {
            tempVal = [value];
        } else {
            tempVal = [value.toString()];
        }
        if (this.headers[key] instanceof Array) {
            (this.headers[key] as string[]).push(...tempVal);
        } else if (typeof this.headers[key] == 'string') {
            const modVal: string[] = (this.headers[key] as string).split(', ');
            modVal.push(...tempVal);
            this.headers[key] = modVal.flat().join(',');
        } else {
            this.headers[key] = [this.headers[key]?.toString(), ...tempVal].filter(v => v != undefined);
        }
    }

    get(key: keyof OutgoingHttpHeaders) {
        return this.headers[key];
    }

    set(key: keyof OutgoingHttpHeaders, value: OutgoingHttpHeader) {
        this.headers[key] = value;
    }
}

class HTTPResponse<T = string | object | Buffer | Uint8Array> {
    #message: IncomingMessage
    #request: ClientRequest
    // headers: IncomingHttpHeaders
    data!: T
    // status: number
    // statusText: string
    // ok: boolean
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    constructor(message: IncomingMessage, request: ClientRequest, _options: HTTPOptions) {
        this.#message = message;
        this.#request = request;
        // this.headers = incoming.headers;
        // this.status = incoming.statusCode!;
        // this.statusText = incoming.statusMessage ?? '';
        // this.ok = incoming.statusCode! >= 200 && incoming.statusCode! <= 299;
        // this.url = incoming.url
    }
    get message() {
        return this.#message;
    }
    get request() {
        return this.#request;
    }
    get status() {
        return this.#message.statusCode;
    }
    get statusText() {
        return this.#message.statusMessage;
    }
    get ok() {
        return this.#message.statusCode! >= 200 && this.#message.statusCode! <= 299;
    }
    get url() {
        return this.#message.url;
    }
    get headers() {
        return this.#message.headers;
    }
}

class WWWAuth {
    basicAuth = undefined
    digestAuth = undefined
    negotiateAuth = undefined
    sspiNegotatiate = undefined
    constructor() {}
}

export class HTTPClient {
    // headers?: HTTPHeaders
    #defaults: Omit<HTTPOptions, 'headers'> & { headers: HTTPHeaders }
    httpsAgent = new HTTPSAgent()
    httpAgent = new HTTPAgent()
    auth = new WWWAuth()
    get [Symbol.toStringTag]() {
        return this.constructor.name
    }
    constructor(options?: HTTPOptions) {
        this.#defaults = {
            timeout: 70000,
            headers: new HTTPHeaders(),
            ...options
        };
    }
    set timeout(val: number) {
        this.#defaults.timeout = val;
    }
    get timeout() {
        return this.#defaults.timeout!;
    }
    set headers(value: Record<string, string | string[] | number>) {
        Object.entries(value).forEach(([k, v]) => this.#defaults.headers.set(k, v));
    }
    request<T>(url: string | URL, options: HTTPOptions) {
        return new Promise<HTTPResponse<T>>((resolve, reject) => {
            try {
                const requestOptions: Omit<HTTPOptions, 'headers'> & { headers?: string[] | HTTPHeaders | OutgoingHttpHeaders } = {
                    ...this.#defaults,
                    ...(typeof url == 'string' ? new URL(url) : url),
                    ...options,
                };
                let headers: (string|OutgoingHttpHeader|undefined)[][];
                if (options.headers) {
                    headers = [...this.#defaults.headers, ...Object.entries(options.headers as OutgoingHttpHeaders)];
                } else {
                    headers = [...this.#defaults.headers];
                }
                delete requestOptions.headers;
                (requestOptions as unknown as Omit<HTTPOptions, 'headers'> & { headers: (string|OutgoingHttpHeader|undefined)[][] }).headers = headers;
                let req: ClientRequest;
                if (requestOptions.protocol?.toLowerCase() == 'https') {
                    req = HTTPSRequest(requestOptions as RequestOptions, (incoming) => resolve(new HTTPResponse(incoming, req, options)));
                } else {
                    req = HTTPRequest(requestOptions as RequestOptions, (incoming) => resolve(new HTTPResponse(incoming, req, options)));
                }
                req.on('error', (err) => {
                    // Check if retry is needed
                    if (req.reusedSocket && (err as NodeJS.ErrnoException).code === 'ECONNRESET') {
                        this.request<T>(url, options).then(resolve).catch(reject);
                    } else {
                        reject(err);
                    }
                });
                if (options.body) {
                    if (typeof options.body == 'string' || options.body instanceof Buffer || options.body instanceof Uint8Array) {
                        req.end(options.body)
                    } else {
                        req.end(JSON.stringify(options.body));
                    }
                } else {
                    req.end();
                }
            } catch (err) {
                reject(err);
            }
        })
    }
    get<T>(url: string | URL, options: HTTPOptions) {
        return this.request<T>(url, { method: 'GET', ...options });
    }
    post<T>(url: string | URL, options: HTTPOptions) {
        return this.request<T>(url, { method: 'POST', ...options });
    }
}

// const HttpMethods = [
//     'ACL', 'BIND', 'CHECKOUT',
//     'CONNECT', 'COPY', 'DELETE',
//     'GET', 'HEAD', 'LINK',
//     'LOCK', 'MERGE',
//     'MKACTIVITY', 'MKCALENDAR', 'MKCOL',
//     'MOVE', 'NOTIFY', 'OPTIONS',
//     'PATCH', 'POST', 'PROPFIND',
//     'PROPPATCH', 'PURGE', 'PUT',
//     'QUERY', 'REBIND', 'REPORT',
//     'SEARCH', 'SOURCE', 'SUBSCRIBE',
//     'TRACE', 'UNBIND', 'UNLINK',
//     'UNLOCK', 'UNSUBSCRIBE'
// ] as const;

// HttpMethods.forEach(method => HTTPClient.prototype[method.toLowerCase()] = function (url: string | URL, options: RequestOptions) {
//     return this.request(url, { method, ...options });
// });